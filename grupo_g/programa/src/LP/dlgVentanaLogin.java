package LP;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.border.EmptyBorder;

import LN.clsGestorLN;
import LN.enRol;

import javax.swing.*;
import java.awt.event.*;
import java.awt.Font;

/**
 * Esta clase es la ventana en la cual puedes iniciar sesion, ir a otra ventana 
 * para registrarte o entrar como invitado en la aplicacion
 */
public class dlgVentanaLogin extends JDialog implements ActionListener {
	

	//****************** INICIO COMPONENTES ***************************
	/**
 	 * Panel principal de contenido de la ventana.
 	 * Se utiliza para contener los elementos de la interfaz de usuario principales.
 	 */
	private JPanel contentPanel = new JPanel();
	/**
	 * Panel de botones de la ventana.
	 * Se utiliza para contener los botones de acción como "Aceptar", "Cancelar", "Registrar", y "Invitado".
	 */
	private JPanel buttonPane = new JPanel();

	/**
	 * Este boton sirve para que te mande a la ventana donde podras registrarte
	 */
	private JButton btnRegistrar = new JButton("Registrarse");
	/**
	 * Este boton sirve para entrar como invitado en la aplicación
	 */
	private JButton btnInvitado = new JButton("Invitado");
	/**
	 * Este es el boton para aceptar e iniciar sesion
	 */
	private JButton btnAceptar = new JButton("Aceptar");
	/**
	 * Este es el boton para salir definitivamente del programa
	 */
	private JButton btnCancelar = new JButton("SALIR");
	/**
	 * Este es el campo donde hay que meter el correo
	 */
	private JLabel lblUsuario = new JLabel("CORREO:");
	/**
	 * Este es el campo donde hay que meter la clave
	 */
	private JLabel lblPassword = new JLabel("CLAVE:");

	/**
	 * Aqui se guarda el correo del usuario
	 */
	private JTextField txtUsuario;
	/**
	 * Aqui se mete la clave del usuario
	 */
	private JPasswordField pwdPassword;
	//****************** FIN COMPONENTES ***************************//

	//*********************** GESTOR LN **********************
	/**
	 * El gestorLN del sistema
	 */
	private clsGestorLN obj_gestorLN;
	//*********************** GESTOR LN **********************//
	
	//***********************CONSTRUCTOR DE LA VENTANA*****************

	/**
	 * Este es el constructor de la clase
	 */
	public dlgVentanaLogin() {

		obj_gestorLN = new clsGestorLN(); //<--------- !!!

		getContentPane().setLayout(new BorderLayout());
		contentPanel.
		setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblUsuario.setBounds(379, 36, 104, 14);
		contentPanel.add(lblUsuario);
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPassword.setBounds(391, 80, 92, 14);
		contentPanel.add(lblPassword);
		txtUsuario = new JTextField();
		txtUsuario.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txtUsuario.setBounds(484, 34, 155, 20);
		contentPanel.add(txtUsuario);
		txtUsuario.setColumns(10);
		pwdPassword = new JPasswordField();
		pwdPassword.setFont(new Font("Tahoma", Font.PLAIN, 12));
		pwdPassword.setBounds(484, 78, 155, 20);
		contentPanel.add(pwdPassword);
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		buttonPane.add(btnRegistrar);
		buttonPane.add(btnInvitado);
		buttonPane.add(btnAceptar);
		getRootPane().setDefaultButton(btnAceptar);
		buttonPane.add(btnCancelar);
		

		JLabel lblFoto_deustoking = new JLabel("");
		lblFoto_deustoking.setBounds(10, 10, 92, 75);
		contentPanel.add(lblFoto_deustoking);

		ImageIcon icon2 = 
		new ImageIcon(getClass().getResource("/img/deustoking_fondo.png"));
		lblFoto_deustoking.setIcon(icon2);


		JLabel lblFoto_playa = new JLabel("");
		lblFoto_playa.setBounds(0, 0, 678, 337);
		contentPanel.add(lblFoto_playa);

		ImageIcon icon1 =
		new ImageIcon(getClass().getResource("/img/playa.png"));
		lblFoto_playa.setIcon( icon1 );

		//--------------------------------------------------------------
		
		btnAceptar.setActionCommand("Aceptar");
		btnInvitado.setActionCommand("Invitado");
		btnCancelar.setActionCommand("Cancelar");
		btnRegistrar.setActionCommand("Registrar");

		btnRegistrar.addActionListener(this);
		btnAceptar.addActionListener(this);
		btnCancelar.addActionListener(this);
		btnInvitado.addActionListener(this);

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(false);
		setTitle("GestionEstudiantes");
		setBounds(100, 100, 690, 405);
		setLocationRelativeTo(null);
		setVisible(true);


		//--------------------------------------------------------------
	}

	//*********************** FIN CONSTRUCTOR ****************************//

	/**
     * Maneja los eventos de accion en los botones de la ventana.
     * 
     * @param e El evento de accion.
     */
	public void actionPerformed(ActionEvent e) 
	{

		switch (e.getActionCommand()) 
		{
			case "Aceptar":

				System.out.println("DEBUG: aceptar");
				String str_correo = txtUsuario.getText();
				String str_clave = new String( pwdPassword.getPassword() );

				Boolean bln_resultado = 
				obj_gestorLN.bln_comprobarCredenciales(str_correo , str_clave );

				if (bln_resultado) 
				{
						//usuario registrado
						if (this.obj_gestorLN.obtenerRol(str_correo, str_clave) 
														    == enRol.REGISTRADO)
						{
						    JOptionPane.showMessageDialog(null, 
						   							"BIENBENIDO " + str_correo);
							setVisible(false);						  
							new frmVentanaUsuarioRegistrado();
						}
						//usuario administrador
						else if (this.obj_gestorLN.obtenerRol(str_correo, 
											  str_clave) == enRol.ADMINISTRADOR)
						{
							JOptionPane.showMessageDialog(null, 
						   							"BIENBENIDO " + str_correo);
							setVisible(false);
							new frmVentanaAdministrador();
						}
				}		
				else
				{
					JOptionPane.showMessageDialog
					(null, "Ese usuario no existe!!");
					txtUsuario.setText("");
					pwdPassword.setText("");
					txtUsuario.requestFocus();
				}
				//1 ventana 3 paneles si eres admin acceso/visible a los 3
				//insertar / borrar / consultar
				break;

			case "Cancelar":
				System.out.println("Cancelar!");
				System.exit(0);
				break;
			
			case "Registrar":
				System.out.println("DEBUG: Registrar!");

				new dlgVentanaRegister();

				break;

			case "Invitado":
				System.out.println("DEBUG: Invitado!");
				JOptionPane.showMessageDialog(null,
				"BIENVENIDO A DEUSTOKING");
				setVisible(false);
				new frmVentanaInvitado();
				//llamar a la ventana de registro enviando su gln
				//new dlgVentanaRegistrarse( obj_gestorLN ); 
				break;
		}
	}
}