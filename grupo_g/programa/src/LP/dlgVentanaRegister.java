package LP;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import EXCEPCIONES.clsDniFormatoIncorrecto;
import EXCEPCIONES.clsUsuarioRepetido;
import LN.clsGestorLN;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;

/**
 * Esta clase sirve para que un usuario se registre y obtendra el rol de 
 * registrado
 */
public class dlgVentanaRegister extends JDialog implements ActionListener 
{

	/**
     * Versión de la clase para la serialización.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Panel principal de contenido de la ventana. Se utiliza para contener los 
	 * elementos de la interfaz de usuario principales.
     */
    private final JPanel contentPanel = new JPanel();

    /**
     * Campo de texto para introducir el nombre del usuario.
     */
    private JTextField textNombre;

    /**
     * Campo de texto para introducir el DNI del usuario.
     */
    private JTextField textDni;

    /**
     * Campo de texto para introducir el correo electrónico del usuario.
     */
    private JTextField textCorreo;

    /**
     * Campo de texto para introducir el apellido del usuario.
     */
    private JTextField textApellido;

    /**
     * Campo de texto para introducir la clave del usuario.
     */
    private JPasswordField textClave;

    /**
     * Etiqueta para el campo de texto del correo.
     */
    private JLabel lblCorreo;

    /**
     * Etiqueta para el campo de texto de la clave.
     */
    private JLabel lblClave;

    /**
     * Etiqueta para el campo de texto del DNI.
     */
    private JLabel lblDni;

    /**
     * Etiqueta para el campo de texto del apellido.
     */
    private JLabel lblApellido;

    /**
     * Etiqueta para el campo de texto del nombre.
     */
    private JLabel lblNombre;

    /**
     * Etiqueta para el menú de registro.
     */
    private JLabel lblMenuDeRegistro;

    /**
     * Objeto gestor de lógica de negocio que maneja las operaciones del sistema.
     */
    private clsGestorLN obj_gestorLN;

    /**
	 *  Etiqueta en la ventana
	 */
	JLabel lblFoto = new JLabel("");
	//****************** FIN COMPONENTES ***************************//

	/**
     * Crea la ventana de registro.
     */
	public dlgVentanaRegister() 
	{
		obj_gestorLN = new clsGestorLN();

		ImageIcon icon = new ImageIcon(getClass().
							    getResource("/img/ImagenRegistrarse.png"));
		lblFoto.setIcon( icon );

		setResizable(false);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.
		setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
	
			textNombre = new JTextField();
			textNombre.setBounds(240, 87, 96, 19);
			contentPanel.add(textNombre);
			textNombre.setColumns(10);


			textDni = new JTextField();
			textDni.setColumns(10);
			textDni.setBounds(240, 145, 96, 19);
			contentPanel.add(textDni);
			textCorreo = new JTextField();
			textCorreo.setColumns(10);
			textCorreo.setBounds(240, 174, 96, 19);
			contentPanel.add(textCorreo);
			textApellido = new JTextField();
			textApellido.setColumns(10);
			textApellido.setBounds(240, 116, 96, 19);
			contentPanel.add(textApellido);
			textClave = new JPasswordField();
			textClave.setBounds(240, 203, 96, 19);
			contentPanel.add(textClave);
			lblNombre = new JLabel("NOMBRE:");
			lblNombre.setFont(new Font("Tahoma", Font.BOLD, 10));
			lblNombre.setBounds(160, 82, 70, 28);
			contentPanel.add(lblNombre);
			lblApellido = new JLabel("APELLIDO:");
			lblApellido.setFont(new Font("Tahoma", Font.BOLD, 10));
			lblApellido.setBounds(160, 111, 70, 28);
			contentPanel.add(lblApellido);
			lblDni = new JLabel("DNI:");
			lblDni.setFont(new Font("Tahoma", Font.BOLD, 10));
			lblDni.setBounds(186, 140, 30, 28);
			contentPanel.add(lblDni);
			lblCorreo = new JLabel("CORREO:");
			lblCorreo.setFont(new Font("Tahoma", Font.BOLD, 10));
			lblCorreo.setBounds(165, 169, 51, 28);
			contentPanel.add(lblCorreo);
			lblClave = new JLabel("CLAVE:");
			lblClave.setFont(new Font("Tahoma", Font.BOLD, 10));
			lblClave.setBounds(175, 198, 70, 28);
			contentPanel.add(lblClave);
			lblMenuDeRegistro = new JLabel("MENU DE REGISTRO");
			lblMenuDeRegistro.
			setFont(new Font("Tahoma", Font.BOLD, 17));
			lblMenuDeRegistro.setBounds(142, 10, 194, 43);
			contentPanel.add(lblMenuDeRegistro);
			lblFoto.setBounds(10, 63, 130, 130);
			contentPanel.add(lblFoto);
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			JButton okButton = new JButton("OK");
			okButton.setActionCommand("OK");
			buttonPane.add(okButton);
			getRootPane().setDefaultButton(okButton);
			JButton cancelButton = new JButton("Cancel");
			cancelButton.setActionCommand("Cancel");
			buttonPane.add(cancelButton);


		cancelButton.setActionCommand("cancelar");
		okButton.setActionCommand("ok");

		cancelButton.addActionListener(this);
		okButton.addActionListener(this);

		setVisible( true );
	}

	/**
     * Maneja los eventos de los botones en la ventana.
     * 
     * @param e El evento de accion.
     */
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		switch (e.getActionCommand()) 
		{
			case "ok":

				String str_nombre = lblNombre.getText();
				String str_apellido = lblApellido.getText();
				String str_dni = lblDni.getText();
				String str_correo = lblCorreo.getText();
				String str_clave = lblClave.getText();
				boolean bln_resultado;

				try{
				bln_resultado = obj_gestorLN.bln_insertarUsuario(str_nombre, 
				str_apellido, str_dni, str_correo, str_clave);

					if (bln_resultado == true) 
					{
						JOptionPane.showMessageDialog(null, 
										  "Excelente, usuario añadido");
						setVisible(false);
						new dlgVentanaLogin();
					}  

				}catch(clsUsuarioRepetido o)
				{
					JOptionPane.showMessageDialog(null, 
												"Usuario exisitente!!");

					textNombre.setText("");
					textApellido.setText("");
					textDni.setText("");
					textCorreo.setText("");
					textClave.setText("");	
				}
				catch (clsDniFormatoIncorrecto o) 
				{
					JOptionPane.showMessageDialog(null, 
								  "El formato del dni es incorrecto!!");
					
					textNombre.setText("");
					textApellido.setText("");
					textDni.setText("");
					textCorreo.setText("");
					textClave.setText("");
				}
				
				break;
		
			case "cancelar":

				setVisible(false);
				new dlgVentanaLogin();

				break;
		}
	}
}