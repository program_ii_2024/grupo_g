package LP;

/**
 * NO MODIFICA
 */

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import COMUN.itfProperty;
import EXCEPCIONES.clsDniFormatoIncorrecto;
import EXCEPCIONES.clsUsuarioRepetido;
import LN.clsGestorLN;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JEditorPane;
import java.awt.Font;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JTextField;

/**
 * Esta clase contiene la ventana para los usuarios administradores
 */
public class frmVentanaAdministrador extends JFrame implements ActionListener {

	 /**
     * Versión de la clase para la serialización.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Panel adicional de la ventana.
     */
    private JPanel panel_1;

    /**
     * Campo de texto para introducir el nombre del usuario.
     */
    private JTextField textNombre;

    /**
     * Campo de texto para introducir el apellido del usuario.
     */
    private JTextField textApellido;

    /**
     * Campo de texto para introducir el correo electrónico del usuario.
     */
    private JTextField textCorreo;

    /**
     * Campo de texto para introducir la clave del usuario.
     */
    private JTextField textClave;

    /**
     * Campo de texto para introducir el nombre al añadir un usuario.
     */
    private JTextField textNombreAñadir;

    /**
     * Campo de texto para introducir el apellido al añadir un usuario.
     */
    private JTextField textApellidoAñadir;

    /**
     * Campo de texto para introducir el DNI al añadir un usuario.
     */
    private JTextField textDniAñadir;

    /**
     * Campo de texto para introducir el correo electrónico al añadir un usuario.
     */
    private JTextField textCorreoAñadir;

    /**
     * Campo de texto para introducir la clave al añadir un usuario.
     */
    private JTextField textClaveAñadir;

    /**
     * Objeto gestor de lógica de negocio que maneja las operaciones del sistema.
     */
    private clsGestorLN obj_GestorLN;

    /**
     * Modelo del comboBox para los DNIs.
     */
    private DefaultComboBoxModel<String> dcbmDNIs;

    /**
     * Modelo del comboBox para los roles.
     */
    private DefaultComboBoxModel<String> dcbmRoles;

    /**
     * ComboBox para seleccionar el DNI de un usuario.
     */
    private JComboBox<String> comboBox_dni;

    /**
     * ComboBox para seleccionar el rol de un usuario.
     */
    private JComboBox<String> comboBox_rol;

    /**
     * Modelo de lista para los usuarios.
     */
    private DefaultListModel<itfProperty> dlm;

    /**
     * Lista de usuarios.
     */
    private ArrayList<itfProperty> arrl_usuarios;

    /**
     * Componente de lista que muestra los usuarios.
     */
    private JList<itfProperty> list_usuarios;

	/**
     * Crea la ventana del administrador.
     */
	public frmVentanaAdministrador() 
	{
        obj_GestorLN = new clsGestorLN();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1358, 892);
		panel_1 = new JPanel();
		panel_1.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(panel_1);
		panel_1.setLayout(null);
		
		JEditorPane dtrpnUsuariosRegistrados = new JEditorPane();
		dtrpnUsuariosRegistrados.setText("LISTA DE USUARIOS");
		dtrpnUsuariosRegistrados.
		setFont(new Font("Tahoma", Font.BOLD, 18));
		dtrpnUsuariosRegistrados.setBounds(1071, 71, 194, 28);
		panel_1.add(dtrpnUsuariosRegistrados);
		
		JButton btnOrdenarDni = new JButton("Ordenar por DNI");
		btnOrdenarDni.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnOrdenarDni.setBounds(989, 561, 165, 36);
		panel_1.add(btnOrdenarDni);
		
		JButton btnOrdenarNombre = new JButton("Ordenar por Nombre");
		btnOrdenarNombre.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnOrdenarNombre.setBounds(1170, 561, 155, 36);
		panel_1.add(btnOrdenarNombre);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(989, 109, 329, 410);
		panel_1.add(scrollPane);
		
		list_usuarios = new JList<>();
		scrollPane.setViewportView(list_usuarios);
		
		JLabel lblFoto1 = new JLabel("");
		lblFoto1.setBounds(0, 607, 674, 238);
		panel_1.add(lblFoto1);

        ImageIcon icon1 = new ImageIcon(getClass().
                                    getResource("/img/foto_maldivas.jpg"));
		lblFoto1.setIcon(icon1);
		
		JLabel lblFoto2 = new JLabel("");
		lblFoto2.setBounds(670, 607, 674, 238);
		panel_1.add(lblFoto2);

        ImageIcon icon2 = new ImageIcon(getClass().
                                  getResource("/img/deustoking_made.jpg"));
		lblFoto2.setIcon(icon2);
		
		JEditorPane dtrpnUsuariosRegistrados_1 = new JEditorPane();
		dtrpnUsuariosRegistrados_1.setText("PANTALLA ADMINISTRADOR");
		dtrpnUsuariosRegistrados_1.
		setFont(new Font("Tahoma", Font.BOLD, 18));
		dtrpnUsuariosRegistrados_1.
		setBounds(588, 10, 290, 28);
		panel_1.add(dtrpnUsuariosRegistrados_1);
		
		JButton btnBorrar = new JButton("BORRAR");
		btnBorrar.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnBorrar.setBounds(552, 559, 165, 36);
		panel_1.add(btnBorrar);
		
		JButton btnModificar = new JButton("MODIFICAR");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnModificar.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnModificar.setBounds(744, 560, 165, 36);
		panel_1.add(btnModificar);
		
		dcbmDNIs = new DefaultComboBoxModel<>();
        comboBox_dni = new JComboBox<String>();
		comboBox_dni.setModel(dcbmDNIs);
		comboBox_dni.setBounds(588, 165, 290, 21);
		panel_1.add(comboBox_dni);
		
		textNombre = new JTextField();
		textNombre.setBounds(730, 211, 148, 19);
		panel_1.add(textNombre);
		textNombre.setColumns(10);
		
		textApellido = new JTextField();
		textApellido.setBounds(730, 313, 148, 19);
		panel_1.add(textApellido);
		textApellido.setColumns(10);
		
		textCorreo = new JTextField();
		textCorreo.setBounds(730, 414, 148, 19);
		panel_1.add(textCorreo);
		textCorreo.setColumns(10);
		
		textClave = new JTextField();
		textClave.setBounds(730, 511, 148, 19);
		panel_1.add(textClave);
		textClave.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("NOMBRE:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(588, 196, 100, 45);
		panel_1.add(lblNewLabel);
		
		JLabel lblApellido = new JLabel("APELLIDO:");
		lblApellido.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblApellido.setBounds(588, 298, 100, 45);
		panel_1.add(lblApellido);
		
		JLabel lblCorreo = new JLabel("CORREO:");
		lblCorreo.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCorreo.setBounds(588, 399, 100, 45);
		panel_1.add(lblCorreo);
		
		JLabel lblClave = new JLabel("CLAVE:");
		lblClave.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblClave.setBounds(600, 496, 100, 45);
		panel_1.add(lblClave);
		
		JLabel lblNombreAñadir = new JLabel("NOMBRE:");
		lblNombreAñadir.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNombreAñadir.setBounds(189, 109, 100, 45);
		panel_1.add(lblNombreAñadir);
		
		textNombreAñadir = new JTextField();
		textNombreAñadir.setColumns(10);
		textNombreAñadir.setBounds(312, 124, 148, 19);
		panel_1.add(textNombreAñadir);
		
		JLabel lblApellidoAñadir = new JLabel("APELLIDO:");
		lblApellidoAñadir.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblApellidoAñadir.setBounds(178, 181, 100, 45);
		panel_1.add(lblApellidoAñadir);
		
		textApellidoAñadir = new JTextField();
		textApellidoAñadir.setColumns(10);
		textApellidoAñadir.setBounds(312, 196, 148, 19);
		panel_1.add(textApellidoAñadir);
		
		JLabel lblDniAñadir = new JLabel("DNI:");
		lblDniAñadir.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDniAñadir.setBounds(189, 265, 100, 45);
		panel_1.add(lblDniAñadir);
		
		textDniAñadir = new JTextField();
		textDniAñadir.setColumns(10);
		textDniAñadir.setBounds(312, 280, 148, 19);
		panel_1.add(textDniAñadir);
		
		JLabel lblCorreoAñadir = new JLabel("CORREO:");
		lblCorreoAñadir.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCorreoAñadir.setBounds(178, 340, 100, 45);
		panel_1.add(lblCorreoAñadir);
		
		textCorreoAñadir = new JTextField();
		textCorreoAñadir.setColumns(10);
		textCorreoAñadir.setBounds(312, 355, 148, 19);
		panel_1.add(textCorreoAñadir);
		
		JEditorPane dtrpnAadirUsuarios = new JEditorPane();
		dtrpnAadirUsuarios.setText("AÑADIR USUARIOS");
		dtrpnAadirUsuarios.setFont(new Font("Tahoma", Font.BOLD, 18));
		dtrpnAadirUsuarios.setBounds(229, 71, 194, 28);
		panel_1.add(dtrpnAadirUsuarios);
		
		JLabel lblClaveAñadir = new JLabel("CLAVE:");
		lblClaveAñadir.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblClaveAñadir.setBounds(189, 414, 100, 45);
		panel_1.add(lblClaveAñadir);	
		
		textClaveAñadir = new JTextField();
		textClaveAñadir.setColumns(10);
		textClaveAñadir.setBounds(312, 429, 148, 19);
		panel_1.add(textClaveAñadir);
		
		JButton btnAñadir = new JButton("AÑADIR");
		btnAñadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAñadir.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnAñadir.setBounds(245, 559, 165, 36);
		panel_1.add(btnAñadir);

        comboBox_dni.addActionListener(this);

		
		JEditorPane dtrpnAadirBorrar = new JEditorPane();
		dtrpnAadirBorrar.setText("MODIFICAR / BORRAR USUARIOS");
		dtrpnAadirBorrar.setFont(new Font("Tahoma", Font.BOLD, 18));
		dtrpnAadirBorrar.setBounds(588, 71, 321, 28);
		panel_1.add(dtrpnAadirBorrar);
		
		JButton btnSalir = new JButton("SALIR");
		btnSalir.setBounds(1180, 10, 85, 21);
		panel_1.add(btnSalir);

		dcbmRoles = new DefaultComboBoxModel<>();
		comboBox_rol = new JComboBox<String>();
		comboBox_rol.setModel(dcbmRoles);
		comboBox_rol.setBounds(312, 510, 148, 21);
		panel_1.add(comboBox_rol);
		
		JLabel lblRol = new JLabel("ROL:");
		lblRol.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblRol.setBounds(189, 496, 100, 45);
		panel_1.add(lblRol);

		btnModificar.addActionListener(this);
		btnModificar.setActionCommand("Modificar");

		btnAñadir.addActionListener(this);
		btnAñadir.setActionCommand("añadir");

		btnBorrar.addActionListener(this);
		btnBorrar.setActionCommand("borrar");

		btnSalir.addActionListener(this);
		btnSalir.setActionCommand("salir");

		btnOrdenarDni.addActionListener(this);
		btnOrdenarDni.setActionCommand("ordenar dni");

		btnOrdenarNombre.addActionListener(this);
		btnOrdenarNombre.setActionCommand("ordenar nombre");
	

        //Muestra los DNIs en la combobox para borrar.

		ArrayList<String> dnis = obj_GestorLN.getDniUsuarios();
		dcbmDNIs.addAll(dnis);

		ArrayList<String> roles = obj_GestorLN.getRoles();
		dcbmRoles.addAll(roles);

		System.out.println( dnis );
		System.out.println("DEBUG : " + dnis.size() ) ;

		//Muestra los usuarios en la lista.

		dlm = new DefaultListModel<>();
		list_usuarios.setModel(dlm);

		arrl_usuarios = obj_GestorLN.getUsuarios();
		dlm.addAll(arrl_usuarios);

		setLocationRelativeTo(null);
        setVisible(true);
	}

	/**
     * Maneja los eventos de accion en la interfaz de usuario.
     * 
     * @param e El evento de accion.
     */
    public void actionPerformed(ActionEvent e) 
    {
        switch (e.getActionCommand()) 
		{
            case "Modificar":
				System.out.println("DEBUG: MODIFICAR");
				vo_procesoModificar();
                break;
        
            case "comboBoxChanged":
				vo_procesoCargarDatosFormulario();
				break;
			
			case "salir":
				setVisible(false);
				new dlgVentanaLogin();
				break;

			case "añadir":
				vo_procesoAñadir();
				break;

			case "borrar":
				vo_borrarUsuario();
				break;

			case "ordenar dni":
				vo_ordenarUsuariosDni();
				break;

			case "ordenar nombre":
				vo_ordenarUsuariosNombre();
				break;
        }
    }

	/**
     * Proceso para añadir un nuevo usuario.
     * Obtiene los datos del formulario y valida que todos los campos esten 
	 * llenos y luego intenta anadir el usuario utilizando el gestorLN.
     * Actualiza la lista de DNIs y la lista de usuarios mostrados en la 
	 * interfaz de usuario.
     * Si hay algun error, como un formato incorrecto de DNI o un usuario 
	 * ya existente, muestra un mensaje de error correspondiente.
     */
    private void vo_procesoAñadir() 
	{
		String str_nombre = textNombreAñadir.getText();
		String str_apellido = textApellidoAñadir.getText();
		String str_dni = textDniAñadir.getText();
		String str_correo = textCorreoAñadir.getText();
		String str_clave = textClaveAñadir.getText();
		String str_rol = (String) comboBox_rol.getSelectedItem();

		if (textNombreAñadir.getText().length() == 0 ||
				textApellidoAñadir.getText().length() == 0 ||
				textDniAñadir.getText().length() == 0 ||
				textCorreoAñadir.getText().length() == 0 ||
				textClaveAñadir.getText().length() == 0)
		{
			JOptionPane.showMessageDialog
			(null, "Debes rellenar todos los datos!");
		}
		else
		{
			try
			{
				obj_GestorLN.vo_añadirUsuario(str_nombre, str_apellido, str_dni, 
				str_correo, str_clave, str_rol);

				JOptionPane.showMessageDialog(null, 
													 "USUARIO AÑADIDO");

				dcbmDNIs.removeAllElements();
				ArrayList <String> dnis = obj_GestorLN.getDniUsuarios();
				dcbmDNIs.addAll(dnis);

				dlm.clear();
				arrl_usuarios = obj_GestorLN.getUsuarios();
				dlm.addAll(arrl_usuarios);

			}
			catch(clsDniFormatoIncorrecto e)
			{
				JOptionPane.showMessageDialog
				   (null, "DNI con formato incorrecto");
			}
			catch (clsUsuarioRepetido o)
			{
				JOptionPane.showMessageDialog
						 (null, "USUARIO YA EXISTENTE");
			}
		}

		textNombreAñadir.setText("");
		textApellidoAñadir.setText("");
		textDniAñadir.setText("");
		textCorreoAñadir.setText("");
		textClaveAñadir.setText("");

	}

	/**
     * Proceso para cargar los datos de un usuario seleccionado en el formulario
     * Obtiene el DNI seleccionado en el combo box, y utiliza el gestorLN
     * para obtener el nombre, apellido, correo y clave de ese DNI.
     * Establece estos valores en los campos correspondientes del formulario
     * para permitir la modificacion de los datos del usuario.
     */
	private void vo_procesoCargarDatosFormulario() 
	{
		String str_dni = (String) comboBox_dni.getSelectedItem();

		String str_nombre = obj_GestorLN.getNombreUsuario(str_dni);
		String str_apellido = obj_GestorLN.getApellidoUsuario(str_dni);
		String str_correo = obj_GestorLN.getCorreoUsuario(str_dni);
		String str_clave = obj_GestorLN.getClaveUsuario(str_dni);

		textNombre.setText(str_nombre);
		textApellido.setText(str_apellido);
		textCorreo.setText(str_correo);
		textClave.setText(str_clave);
	}

	/**
     * Proceso para modificar los datos de un usuario.
     * Obtiene los nuevos valores de nombre, apellido, correo y clave
     * desde los campos del formulario. 
	 * Oobtiene el DNI del usuario en el combo box y llama al metodo 
	 * correspondiente del gestorLN para realizar la modificacion.
     * Despues de la modificacion, actualiza la lista de DNIs en el combo box
     * y la lista de usuarios en la interfaz grafica.
     */
	private void vo_procesoModificar() 
	{	
		String str_nombre = textNombre.getText();
		String str_apellido = textApellido.getText();
		String str_correo = textCorreo.getText();
		String str_clave = textClave.getText();
		

		// el dni que has seleccionado de la combobox
		String str_dni = (String) comboBox_dni.getSelectedItem();

		// llamar a modificar
		obj_GestorLN.modificarEstudiante(str_dni, str_nombre, str_apellido, 
		str_correo, str_clave);

		dcbmDNIs.removeAllElements();
		ArrayList<String> dnis = obj_GestorLN.getDniUsuarios();
		dcbmDNIs.addAll(dnis);

		dlm.clear();
		arrl_usuarios = obj_GestorLN.getUsuarios();
		dlm.addAll(arrl_usuarios);
	}

	/**
	 * Este metodo borra un usuario.
	 * Obtiene el DNI del usuario seleccionado en el JComboBox.
 	 * Muestra un mensaje de confirmacion de borrado con el DNI del usuario.
	 * Llama al metodo vo_borrarUsuario del gestorLN para eliminar el usuario.
	 * Actualiza la lista de DNIs en el JComboBox y la lista de usuarios en la 
	 * interfaz grafica despues del borrado.
	 */
	private void vo_borrarUsuario()
	{
		String str_dni = (String) comboBox_dni.getSelectedItem();

		JOptionPane.showMessageDialog(null,"BORRADO el dni :" + 
																	   str_dni);
		obj_GestorLN.vo_borrarUsuario(str_dni);

		dcbmDNIs.removeAllElements();
		ArrayList <String> dnis = obj_GestorLN.getDniUsuarios();
		dcbmDNIs.addAll(dnis);

		dlm.clear();
		arrl_usuarios = obj_GestorLN.getUsuarios();
		dlm.addAll(arrl_usuarios);
	}

	/**
	 * Este metodo ordena la lista de usuarios por DNI en orden ascendente.
	 * Obtiene la lista de usuarios ordenada por DNI del gestorLN.
 	 * Limpia la lista de usuarios en la interfaz grafica.
	 * Anade los usuarios ordenados por DNI a la lista en la interfaz grafica.
	 */
	private void vo_ordenarUsuariosDni()
	{
		List<itfProperty> r = obj_GestorLN.getUsuariosOrdenadosDni();

		dlm.clear();
		dlm.addAll(r);
	}

	/**
	 * Este metodo ordena la lista de usuarios por nombre en orden ascendente.
	 * Obtiene la lista de usuarios ordenada por nombre del gestorLN.
	 * Limpia la lista de usuarios en la interfaz grafica.
 	 * Anade los usuarios ordenados por nombre a la lista en la interfaz.
	 */
	private void vo_ordenarUsuariosNombre()
	{
		List<itfProperty> r = obj_GestorLN.getUsuariosOrdenadosNombre();

		dlm.clear();
		dlm.addAll(r);
	}
}