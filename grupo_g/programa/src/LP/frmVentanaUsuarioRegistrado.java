package LP;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * Esta es la ventana para los usuarios que hayan iniciado sesion y sean 
 * usuarios registrados
 */
public class frmVentanaUsuarioRegistrado extends JFrame implements ActionListener {

	/**
 * Identificador único de la versión de serialización de esta clase.
 * Este valor se utiliza durante la deserialización para garantizar la compatibilidad
 * entre diferentes versiones de la clase.
 */
private static final long serialVersionUID = 1L;

	/**
	 * Panel principal de contenido de la ventana.
	 */
	private JPanel contentPane;

	/**
	 * Este es el constructor de la clase
	 */
	public frmVentanaUsuarioRegistrado() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 841, 624);
		contentPane = new JPanel();
		contentPane.
		setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("    LA VENTANA PARA LOS USUARIOS " 
								+ "REGISTRADOS SE ENCUENTRA FUERA DE SERVICIO");
								
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblNewLabel.setBounds(10, 140, 807, 95);
		contentPane.add(lblNewLabel);
		
		JLabel lblIntentaloEnOtro = 
		new JLabel("       INTENTALO EN OTRO MOMENTO");
		lblIntentaloEnOtro.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblIntentaloEnOtro.setBounds(220, 245, 357, 78);
		contentPane.add(lblIntentaloEnOtro);
		
		JButton btnSalir = new JButton("SALIR");
		btnSalir.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnSalir.setBounds(325, 414, 153, 47);
		contentPane.add(btnSalir);
		
		JLabel lblFoto_1 = new JLabel("");
		lblFoto_1.setBounds(10, 388, 153, 189);
		contentPane.add(lblFoto_1);

        ImageIcon icon1 = 
		new ImageIcon(getClass().
		getResource("/img/fuera_de_servicio.jpg"));
		lblFoto_1.setIcon( icon1 );

		
		JLabel lblFoto_2 = new JLabel("");
		lblFoto_2.setBounds(664, 388, 153, 189);
		contentPane.add(lblFoto_2);

        ImageIcon icon2 = 
		new ImageIcon(getClass().
		getResource("/img/fuera_de_servicio.jpg"));
		lblFoto_2.setIcon( icon2 );

        setVisible(true);

        btnSalir.setActionCommand("salir");
        btnSalir.addActionListener(this);

	}

	 /**
     * Metodo de accion para manejar eventos de boton.
     * Este metodo se invoca cuando se hace clic en el boton SALIR.
     * Cierra la ventana y muestra la ventana de inicio de sesion.
     * 
     * @param e El evento de accion que desencadena este metodo.
     */
    @Override
    public void actionPerformed(ActionEvent e) 
    {
        switch (e.getActionCommand()) 
        {
            case "salir":
                setVisible(false);
                new dlgVentanaLogin();
                break;
        
        }
    }
}