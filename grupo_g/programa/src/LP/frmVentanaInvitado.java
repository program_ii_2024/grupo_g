package LP;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import COMUN.itfProperty;
import LN.clsGestorLN;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import java.awt.Font;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.Timer;

/**
 * Esta es la clase de los usuarios que entran como invitado
 */
public class frmVentanaInvitado extends JFrame implements ActionListener
{

	/**
     * Versión de la clase para la serialización.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Panel principal de contenido de la ventana.
     */
    private JPanel contentPane;

    /**
     * Objeto gestor de lógica de negocio que maneja las operaciones del sistema.
     */
    private clsGestorLN obj_GestorLN;

    /**
     * Lista de interfaz de usuario para mostrar los vuelos.
     */
    private JList<itfProperty> lst_Vue;

    /**
     * Lista de interfaz de usuario para mostrar los alojamientos.
     */
    private JList<itfProperty> lst_Aloj;

    /**
     * Lista de objetos de tipo vuelo.
     */
    private ArrayList<itfProperty> obj_vuelos;

    /**
     * Lista de objetos de tipo alojamiento.
     */
    private ArrayList<itfProperty> obj_alojamientos;

    /**
     * Modelo de lista para gestionar los datos de los vuelos en la interfaz de usuario.
     */
    private DefaultListModel<itfProperty> dlm_1;

    /**
     * Modelo de lista para gestionar los datos de los alojamientos en la interfaz de usuario.
     */
    private DefaultListModel<itfProperty> dlm_2;

	/**
 	 * Hilo de ejecución asociado a esta instancia.
 	 */
	private Thread hilo = new Thread() {

		public void run() {

			while (true) {
				try {
					Thread.sleep(1000 * 20); 
					// Ajusta la velocidad del desplazamiento

					// ****************************************************

					String message = "¿Estás conectado?";
					String title = "Confirmación";
					int optionType = JOptionPane.YES_NO_OPTION;
					int messageType = JOptionPane.QUESTION_MESSAGE;

					// Crear un JOptionPane personalizado
					Object[] options = { "Sí", "No (5)" };
					JOptionPane optionPane = new JOptionPane(message, 
									messageType, optionType, null, options,
							options[0]);

					// Crear un JDialog a partir del JOptionPane
					JDialog dialog = optionPane.createDialog(title);

					/**  Crear un Timer para actualizar el texto del botón "No" 
					 * y cerrar el dialogo
					 */
					// después de 5 segundos
					Timer timer = new Timer(1000, new ActionListener() {
						int count = 10; // Contador de 10 segundos

						@Override
						public void actionPerformed(ActionEvent e) {
							if (count > 1) {
								count--;
								optionPane.setOptions(new Object[] 
												{ "Sí", "No (" + count + ")" });
							} else {
								/**
								 * Detener el timer y cerrar el diálogo si el 
								 * tiempo ha terminado
								*/ 
								((Timer) e.getSource()).stop();
								dialog.dispose();
								System.exit(0); // Terminar el programa
							}
						}
					});

					// Iniciar el timer
					timer.setInitialDelay(0);
					timer.start();

					// Mostrar el diálogo
					dialog.setVisible(true);

					// Obtener la respuesta del usuario
					Object selectedValue = optionPane.getValue();

					// Cancelar el timer si el usuario ha respondido
					timer.stop();

					// Comprobar la respuesta del usuario
					if (selectedValue.equals("Sí")) {
						System.out.println("Usuario conectado.");
					} else {
						System.out.println("Usuario no conectado.");
						System.exit(0); 
						// Terminar el programa si la respuesta es "No"
					}

					// ***************************************************

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	};

	/**
     * Crea la ventana de la aplicacion.
     */
	public frmVentanaInvitado() 
	{
		obj_GestorLN = new clsGestorLN();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 889, 710);
		contentPane = new JPanel();
		contentPane.
		setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.	setLayout(null);
		
		JButton btnOrdenPrecioVuelo = new JButton("ORDENAR POR PRECIO");
		btnOrdenPrecioVuelo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnOrdenPrecioVuelo.setBounds(168, 42, 185, 21);
		contentPane.add(btnOrdenPrecioVuelo);
		
		JButton btnOrdenIdAlojamiento = new JButton("ORDENAR POR DESTINO");
		btnOrdenIdAlojamiento.setBounds(132, 327, 191, 21);
		contentPane.add(btnOrdenIdAlojamiento);
		
		JButton btnOrdenIdVuelo = new JButton("ORDENAR POR ID");
		btnOrdenIdVuelo.setBounds(517, 42, 175, 21);
		contentPane.add(btnOrdenIdVuelo);
		
		JButton btnOrdenPrecioAlojamiento = 
		new JButton("ORDENAR POR PRECIO");
		btnOrdenPrecioAlojamiento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnOrdenPrecioAlojamiento.setBounds(544, 327, 185, 21);
		contentPane.add(btnOrdenPrecioAlojamiento);
		
		JEditorPane dtrpnVuelos = new JEditorPane();
		dtrpnVuelos.setFont(new Font("Tahoma", Font.PLAIN, 18));
		dtrpnVuelos.setText("VUELOS");
		dtrpnVuelos.setBounds(390, 35, 75, 28);
		contentPane.add(dtrpnVuelos);
		
		JEditorPane dtrpnAlojamientos = new JEditorPane();
		dtrpnAlojamientos.setText("ALOJAMIENTOS");
		dtrpnAlojamientos.setFont(new Font("Tahoma", Font.PLAIN, 18));
		dtrpnAlojamientos.setBounds(365, 320, 145, 28);
		contentPane.add(dtrpnAlojamientos);
		
		JButton btnSalir = new JButton("SALIR");
		btnSalir.setBounds(390, 642, 85, 21);
		contentPane.add(btnSalir);

		btnOrdenIdAlojamiento.setActionCommand("idAlojamiento");
		btnOrdenPrecioAlojamiento.
						setActionCommand("destinoAlojamiento");
		btnOrdenIdVuelo.setActionCommand("idVuelo");
		btnOrdenPrecioVuelo.setActionCommand("precioVuelo");
		
		JScrollPane scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(23, 88, 842, 229);
		contentPane.add(scrollPane1);

		lst_Vue = new JList<>();
		scrollPane1.setViewportView(lst_Vue);

		JScrollPane scrollPane2 = new JScrollPane();
		scrollPane2.setBounds(23, 358, 842, 241);
		contentPane.add(scrollPane2);

		lst_Aloj = new JList<>();
		scrollPane2.setViewportView(lst_Aloj);
		

		dlm_1 = new DefaultListModel<>();
		lst_Vue.setModel(dlm_1);

		obj_vuelos = obj_GestorLN.getVuelos();
		dlm_1.addAll(obj_vuelos);


		dlm_2 = new DefaultListModel<>();
		lst_Aloj.setModel(dlm_2);

		obj_alojamientos = obj_GestorLN.getAlojamientos();
		dlm_2.addAll(obj_alojamientos);


		hilo.start();


		btnOrdenIdAlojamiento.addActionListener(this);
		btnOrdenIdVuelo.addActionListener(this);
		btnOrdenPrecioAlojamiento.addActionListener(this);
		btnOrdenPrecioVuelo.addActionListener(this);
		btnSalir.addActionListener(this);


		setLocationRelativeTo(null);
		setVisible( true );
	}

	/**
 	 * Maneja los eventos de accion generados por componentes de la interfaz.
	 * @param e El evento de accion.
	 */
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		switch (e.getActionCommand()) 
		{

			case "SALIR":
				System.out.println("SALIR");
				setVisible(false);
				new dlgVentanaLogin();
				break;

			case "idAlojamiento":
				System.out.println("idAlojamiento");
				ordenarAlojamientoId();
				break;

			case "destinoAlojamiento":
				System.out.println("precioAlojamiento");
				ordenarAlojamientoDestino();
				break;

			case "idVuelo":
				System.out.println("idVuelo");
				ordenarVueloId();
				break;
		
			case "precioVuelo":
				System.out.println("precioVuelo");
				ordenarVueloPrecio();
				break;

		}
	}

	/**
	 * Ordena los alojamientos por su ID y actualiza la lista de alojamientos 
	 * mostrada en la interfaz.
	 */
	private void ordenarAlojamientoId()
	{
		List<itfProperty> r = obj_GestorLN.
		getInfoAlojamientosOrdenadosDestino();

		// borrar
		dlm_2.clear();
		// actualizar
		dlm_2.addAll(r);
	}

	/**
	 * Ordena los alojamientos por su destino y actualiza la lista de 
	 * alojamientos mostrada en la interfaz.
	 */
	private void ordenarAlojamientoDestino()
	{
		List<itfProperty> r = obj_GestorLN.getAlojamientosOrdenadosPrecio();

		// borrar
		dlm_2.clear();
		// actualizar
		dlm_2.addAll(r);
	}

	/**
	 * Ordena los vuelos por su ID y actualiza la lista de vuelos mostrada en 
	 * la interfaz.
	 */
	private void ordenarVueloId()
	{
		List<itfProperty> r = obj_GestorLN.getVuelosOrdenadosId();

		// borrar
		dlm_1.clear();
		// actualizar
		dlm_1.addAll(r);
	}

	/**
	 * Ordena los vuelos por su precio y actualiza la lista de vuelos mostrada 
	 * en la interfaz.
	 */
	private void ordenarVueloPrecio()
	{
		List<itfProperty> r = obj_GestorLN.getVuelosOrdenadosPrecio();

		// borrar
		dlm_1.clear();
		// actualizar
		dlm_1.addAll(r);
	}
}