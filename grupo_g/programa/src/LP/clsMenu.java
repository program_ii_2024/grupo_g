package LP;

/**
 */

import java.time.format.DateTimeFormatter;
import java.util.*;
import COMUN.*;
import EXCEPCIONES.clsDniFormatoIncorrecto;
import EXCEPCIONES.clsUsuarioRepetido;
import LN.clsGestorLN;
import LN.enRol;

/**
 * Esta clase representa un menu en el sistema.
 * Contiene metodos y atributos relacionados con la gestion de menus en la
 * interfaz de usuario.
 * Puede ser utilizada para mostrar opciones y facilitar la interaccion.
 */
public class clsMenu 
{
    // Nexo de union entre capas.
    private clsGestorLN obj_gestor;

    /**
     * Constructor de la clase clsMenu que inicializa un objeto de la clase
     * clsGestor.
     * Este constructor puede ser utilizado para preparar la funcionalidad del
     * menu.
     */
    public clsMenu() 
    {
        obj_gestor = new clsGestorLN();
    }

    /**
     * Este metodo muestra el menu donde aparecen las opciones principales de
     * la aplicacion:
     * 
     * 1.Iniciar Sesion. // 2.Registrarse. // 3.Entrar como invitado.
     * // 4. Salir de la aplicacion.
     * 
     */

    public void vo_mostrarMenu() 
    {
        int int_op;
        int_op = -1;
        do 
        {
            System.out.println("| *            *            * |");
            System.out.println("| **          ***          ** |");
            System.out.println("| ***        *****        *** |");
            System.out.println("| ****      *******      **** |");
            System.out.println("| *************************** |");
            System.out.println("| *************************** |");
            System.out.println("| *************************** |");
            System.out.println("|  -------------------------  |");
            System.out.println("|  BIENVENIDO A DEUSTOKING    |");
            System.out.println("|  -------------------------  |");
            System.out.println("|  1. Iniciar sesion          |");
            System.out.println("|  2. Registrarse             |");
            System.out.println("|  3. Entrar como invitado    |");
            System.out.println("|  -------------------------  |");
            System.out.println("|  4. Salir                   |");
            System.out.println("|  -------------------------  |");
            System.out.print("|  Opcion: ");

            int_op = UtilidadesLP.leerEntero();

            switch (int_op) 
            {
                case 1:
                    vo_menuIniciarSesion();
                    break;
                case 2:
                    vo_menuRegistrarse();
                    break;
                case 3:
                    vo_mostrarMenuInvitado();
                    break;
            }
        } while (int_op != 4);

        System.out.println("GRACIAS POR CONFIAR EN DEUSTOKING");
        System.out.println("Agur, BenHur eta jan yoghurt!");
    }

    /**
     * 
     * Este es el metodo donde un usuario ya registrado podra iniciar sesion
     * para acceder al sistema desde su cuenta.
     * 
     */
    public void vo_menuIniciarSesion() 
    {
        String str_correo;
        String str_clave;

        System.out.println("-------------------------");
        System.out.print("Introduce tu correo: ");
        str_correo = UtilidadesLP.leerCadena();
        System.out.println();

        System.out.println("-------------------------");
        System.out.print("Introduce tu clave: ");
        str_clave = UtilidadesLP.leerCadena();
        System.out.println();

        boolean bln_resultado;
        bln_resultado = obj_gestor.bln_comprobarCredenciales(str_correo,
                str_clave);

        if (bln_resultado)
        {
            if (this.obj_gestor.obtenerRol(str_correo, str_clave) 
            == enRol.ADMINISTRADOR) 
            {
                vo_mostrarMenuAdministrador();
            } 
            else 
            {
                if (this.obj_gestor.obtenerRol(str_correo, str_clave) 
                == enRol.REGISTRADO) 
                {
                    vo_mostrarMenuReserva();
                }
            }

        } else {
            System.out.println("---------------------------");
            System.out.println("Credenciales incorrectas");
            System.out.println("---------------------------");
            vo_pausa();
            
        }
    }

    /**
     * Este es el metodo donde un usuario podra registrarse introduciento todos
     * sus datos.
     * 
     * @param str_nombre   El nombre del usuario.
     * @param str_apellido El apellido del usuario.
     * @param str_dni      El dni del usuario.
     * @param str_correo   El correo del usuario.
     * @param str_clave    La clave del usuario.
     * 
     * @return True si no existe ya un usuario igual o false si ya esta anadido
     *         en el sistema.
     */

    private void vo_menuRegistrarse() 
    {
        // Variables locales.
        String str_nombre;
        String str_apellido;
        String str_dni;
        String str_correo;
        String str_clave;

        System.out.println("-------------------------");
        System.out.print("Introduce el nombre: ");
        str_nombre = UtilidadesLP.leerCadena();
        System.out.println();

        System.out.println("-------------------------");
        System.out.print("Introduce el apellido: ");
        str_apellido = UtilidadesLP.leerCadena();
        System.out.println();

        System.out.println("-------------------------");
        System.out.print("Introduce el DNI: ");
        str_dni = UtilidadesLP.leerCadena();
        System.out.println();

        System.out.println("-------------------------");
        System.out.print("Introduce el correo: ");
        str_correo = UtilidadesLP.leerCadena();
        System.out.println();

        System.out.println("-------------------------");
        System.out.print("Introduce la clave: ");
        str_clave = UtilidadesLP.leerCadena();
        System.out.println();

        boolean bln_resultado;

        try
        {
        bln_resultado = obj_gestor.bln_insertarUsuario(str_nombre, str_apellido,
                str_dni, str_correo, str_clave);
            if (bln_resultado == true) 
            {
        
              System.out.println("*****************************************");
              System.out.println("Excelente! Usuario añadido al sistema!");
              System.out.println("*****************************************");
              vo_pausa();

            }  
        }catch(clsUsuarioRepetido e)
        {
            System.out.println("****************************");
            System.out.println(e.getMessage());
            System.out.println("****************************");

            vo_pausa();
        }
        catch (clsDniFormatoIncorrecto e) 
        {
            System.out.println("************************************");
            System.out.println(e.getMessage());
            System.out.println("************************************");
            vo_pausa();
        } 
    }

    /**
     * 
     * Este metodo te permite ver el menu del administrador una vez hayas
     * iniciado sesion como administrador:
     * 
     * 1.Anadir Usuario. // 2.Borrar Usuario. // 3.Modificar Usuario. //
     * 4.Visualizar lista de usuarios. // 5.Visualizar lista de reservas. //
     * 6.Visualizar lista de alojamientos reservados. //
     * 7.Volver al menu principal
     * 
     */
    private void vo_mostrarMenuAdministrador() 
    {
        int int_op;
        int_op = -1;
        do {
            System.out.println();
            System.out.println("----------------------");
            System.out.println("MENU DEL ADMINISTRADOR");
            System.out.println("----------------------");
            System.out.println("1. Añadir usuario");
            System.out.println("2. Borrar Usuario");
            System.out.println("3. Modificar Usuario");
            System.out.println("4. Visualizar lista de Usuarios");
            System.out.println("5. Visualizar lista de vuelos reservados");
            System.out.println(
                "6. Visualizar lista de alojamientos reservados");
            System.out.println("7. Volver al menu principal");
            System.out.println("-----------------------------------------");
            System.out.print("Opcion: ");
            int_op = UtilidadesLP.leerEntero();
            System.out.println();

            switch (int_op) 
            {
                case 1:
                    int int_op2;
                    System.out.println("********************");
                    System.out.println("QUE QUIERES AÑADIR");
                    System.out.println("********************");
                    System.out.println("1. Usuario");
                    System.out.println("2. Administrador");
                    System.out.println("********************");
                    System.out.print("OPCION: ");
                    int_op2 = UtilidadesLP.leerEntero();
                    switch (int_op2) 
                    {
                        case 1:
                            vo_anadirUsuario();
                            break;
                        case 2:
                            vo_anadirAdministrador();
                            break;
                    }
                    break;
                case 2:
                    vo_borrarUsuario();
                    break;
                case 3:
                    vo_modificarUsuario();
                    break;
                case 4:
                    vo_visualizarUsuarios();
                    break;
                case 5:
                    vo_visualizarVuelosReservadosAdministrador();
                    break;
                case 6:
                    vo_visualizarAlojamientosReservadosAdministrador();
                    break;
            }
        } while (int_op != 7);
    }

    /**
     * Este es metodo vacio.
     */
    private void vo_visualizarVuelosReservadosAdministrador() 
    {

    }

    /**
     * Este metodo sirve para que el administrador pueda visualizar los
     * alojamientos reservados.
     */
    private void vo_visualizarAlojamientosReservadosAdministrador() 
    {
        ArrayList<itfProperty> arrl_infoAlojamientosReservados;
        arrl_infoAlojamientosReservados = 
        obj_gestor.arrl_recuperarInfoReservaAlojamientos();

        if (arrl_infoAlojamientosReservados.isEmpty()) 
        {
            System.out.println("-----------------------------");
            System.out.println("NO SE HA RESERVADO NINGUN ALOJAMIENTO");
            System.out.println("-----------------------------");
            vo_pausa();
        } else 
        {
            System.out.println("---------------------------");
            System.out.println("INFORMACION DE LOS ALOJAMIENTOS RESERVADOS:");
            System.out.println("---------------------------");

        for (itfProperty itf_alojamiento : arrl_infoAlojamientosReservados) 
        {
        System.out.print("Destino: " +
        itf_alojamiento.getObjectProperty(clsConstantes.DESTINO_ALOJAMIENTO));
        System.out.println();

        System.out.print("Fecha de llegada: " +
        itf_alojamiento.getObjectProperty(clsConstantes.FECHA_INICIO));
        System.out.println();

        System.out.print("Fecha de salida: " +
        itf_alojamiento.getObjectProperty(clsConstantes.FECHA_FINAL));
        System.out.println();

        System.out.print("Precio: " +
        itf_alojamiento.getObjectProperty(clsConstantes.PRECIO_ALOJAMIENTO));
        System.out.println();

        System.out.println("---------------------------------------");
        }
        vo_pausa();
        }
    }

    /**
     * Este metodo sirve para visualizar los vuelos reservados.
     */
    private void vo_visualizarVuelosReservado() 
    {
        String str_dni;

        System.out.println("---------------------------------------");
        System.out.println("¿Cual es el DNI del representate del vuelo?");
        System.out.println("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        boolean bln_resultado1;
        bln_resultado1 = obj_gestor.bln_comprobarVueloReservado(str_dni);

        if (bln_resultado1) 
        {
            itfProperty obj_info;
            obj_info = obj_gestor.recuperarInfoReservaVuelo(str_dni);

            System.out.println("----------------------------------------");
            System.out.println("INFORMACION DE LA RESERVA DEL VUELO");
            System.out.println("----------------------------------------");

            System.out.print("DESTINO: " +
            obj_info.getObjectProperty(clsConstantes.DESTINO_VUELO));
            System.out.println();

            System.out.print("HORA DE SALIDA: " +
            obj_info.getObjectProperty(clsConstantes.HORA_SALIDA));
            System.out.println();

            System.out.print("HORA DE LLEGADA: " +
            obj_info.getObjectProperty(clsConstantes.HORA_LLEGADA));
            System.out.println();

            System.out.print("CANTIDAD DE PASAJEROS: " +
            obj_info.getObjectProperty(clsConstantes.NUMERO_PASAJEROS));
            System.out.println();

            System.out.print("CANTIDAD DE EQUIPAJE: " +
            obj_info.getObjectProperty(clsConstantes.CANTIDAD_EQUIPAJE));
            System.out.println();

            System.out.print("PRECIO: " +
            obj_info.getObjectProperty(clsConstantes.PRECIO_VUELO));
            System.out.println();

            vo_pausa();

        } else 
        {
            System.out.println("----------------------------------------");
            System.out.println("NO HAY VUELOS RESERVADOS A ESE NOMBRE");
            System.out.println("----------------------------------------");

            vo_pausa();
        }

        boolean bln_resultado2;
        bln_resultado2 =
        obj_gestor.bln_comprobarAlojamientoReservadoUsuario(str_dni);

        if (bln_resultado2) 
        {
            itfProperty obj_info;
            obj_info = 
            obj_gestor.arrl_recuperarInfoReservaAlojamientosUsuarios(str_dni);

            System.out.println("----------------------------------------");
            System.out.println("INFORMACION DE LA RESERVA DEL ALOJAMIENTO");
            System.out.println("----------------------------------------");

            System.out.print("DESTINO: " +
            obj_info.getObjectProperty(clsConstantes.DESTINO_ALOJAMIENTO));
            System.out.println();

            System.out.print("FECHA DE LLEGADA: " +
            obj_info.getObjectProperty(clsConstantes.FECHA_LLEGADA));
            System.out.println();

            System.out.print("FECHA DE SALIDA: " +
            obj_info.getObjectProperty(clsConstantes.FECHA_SALIDA));
            System.out.println();

            System.out.print("PRECIO: " +
            obj_info.getObjectProperty(clsConstantes.PRECIO_ALOJAMIENTO));
            System.out.println();

            System.out.print("PROPIETARIO: " +
            obj_info.getObjectProperty(clsConstantes.PROPIETARIO));
            System.out.println();

            vo_pausa();

        } else 
        {
            System.out.println("-------------------------------------------");
            System.out.println("NO HAY ALOJAMIENTOS RESERVADOS A ESE NOMBRE");
            System.out.println("-------------------------------------------");

            vo_pausa();
        }

    }

    /**
     * Este metodo permite al administrador anadir un nuevo usuario al sistema.
     * 
     * @param str_nombre   El nombre del usuario.
     * @param str_apellido El apellido del usuario.
     * @param str_dni      El dni del usuario.
     * @param str_correo   El correo del usuario.
     * @param str_clave    La clave del usuario.
     * 
     * @return True si no existe ya un usuario igual o false si ya esta anadido
     * en el sistema.
     */

    private void vo_anadirUsuario() 
    {
        // Variables locales.
        String str_nombre;
        String str_apellido;
        String str_dni;
        String str_correo;
        String str_clave;

        System.out.println("-------------------------");
        System.out.print("Introduce el nombre: ");
        str_nombre = UtilidadesLP.leerCadena();
        System.out.println();

        System.out.println("-------------------------");
        System.out.print("Introduce el apellido: ");
        str_apellido = UtilidadesLP.leerCadena();
        System.out.println();

        System.out.println("-------------------------");
        System.out.print("Introduce el DNI: ");
        str_dni = UtilidadesLP.leerCadena();
        System.out.println();

        System.out.println("-------------------------");
        System.out.print("Introduce el correo: ");
        str_correo = UtilidadesLP.leerCadena();
        System.out.println();

        System.out.println("-------------------------");
        System.out.print("Introduce la clave: ");
        str_clave = UtilidadesLP.leerCadena();
        System.out.println();

        boolean bln_resultado;
        try
        {
        bln_resultado = obj_gestor.bln_insertarUsuario(str_nombre,
                str_apellido, str_dni, str_correo, str_clave);

            if (bln_resultado) 
            {
             System.out.println("*****************************************");
             System.out.println("Excelente! Usuario añadido al sistema!");
             System.out.println("*****************************************");
            }

        }
        catch (clsDniFormatoIncorrecto e)
        {
            System.out.println("**************************************");
            System.out.println(e.getMessage()); 
            System.out.println("**************************************");
        }
        catch (clsUsuarioRepetido e)
        {
            System.out.println("**************************************");
            System.out.println(e.getMessage());
            System.out.println("**************************************");
        }
    }

    /**
     * Este metodo permite al administrador anadir un nuevo administrador al 
     * sistema.
     * 
     * @param str_nombre   El nombre del administrador.
     * @param str_apellido El apellido del administrador.
     * @param str_dni      El dni del administrador.
     * @param str_correo   El correo del administrador.
     * @param str_clave    La clave del administrador.
     * 
     * @return True si no existe ya un administrador igual o false si ya esta 
     * anadido en el sistema.
     */
    private void vo_anadirAdministrador() 
    {
        // Variables locales.
        String str_nombre;
        String str_apellido;
        String str_dni;
        String str_correo;
        String str_clave;

        System.out.println("-------------------------");
        System.out.print("Introduce el nombre: ");
        str_nombre = UtilidadesLP.leerCadena();
        System.out.println();

        System.out.println("-------------------------");
        System.out.print("Introduce el apellido: ");
        str_apellido = UtilidadesLP.leerCadena();
        System.out.println();

        System.out.println("-------------------------");
        System.out.print("Introduce el DNI: ");
        str_dni = UtilidadesLP.leerCadena();
        System.out.println();

        System.out.println("-------------------------");
        System.out.print("Introduce el correo: ");
        str_correo = UtilidadesLP.leerCadena();
        System.out.println();

        System.out.println("-------------------------");
        System.out.print("Introduce la clave: ");
        str_clave = UtilidadesLP.leerCadena();
        System.out.println();

        boolean bln_resultado;
        try
        {
        bln_resultado = obj_gestor.bln_insertarAdministrador(str_nombre,
                str_apellido, str_dni, str_correo, str_clave);

            if (bln_resultado) 
            {
             System.out.println("*****************************************");
             System.out.println("Excelente! Usuario añadido al sistema!");
             System.out.println("*****************************************");
            }

        }
        catch (clsDniFormatoIncorrecto e)
        {
            System.out.println("**************************************");
            System.out.println(e.getMessage()); 
            System.out.println("**************************************");
        }
        catch (clsUsuarioRepetido e)
        {
            System.out.println("**************************************");
            System.out.println(e.getMessage());
            System.out.println("**************************************");
        }
    }

    /** 
     * Este metodo permite al administrador borrar el usuario que quiera del
     * sistema.
     */
    private void vo_borrarUsuario() 
    {
        boolean bln_bln;
        String str_dni;

        System.out.println("Teclea el DNI del usuario:");
        str_dni = UtilidadesLP.leerCadena();
        bln_bln = obj_gestor.bln_borrarUsuario(str_dni);

        if (bln_bln) 
        {
            System.out.println("-----------------------------------------");
            System.out.println("Excelente! Usuario borrado del sistema!");
            System.out.println("-----------------------------------------");
            vo_pausa();
        } 
        else 
        {
            System.out.println("-------------------------------------------");
            System.out.println("Error! No existe el usuario en el sistema!");
            System.out.println("-------------------------------------------");
            vo_pausa();
        }
    }

    /** 
     * Este metodo permite al administrador modificar los datos del usuario que
     * el quiera.
     * 
     * @param str_nombre   El nombre del usuario el cual no se cambia.
     * @param str_apellido El nuevo apellido del usuario.
     * @param str_dni      El nuevo dni del usuario.
     * @param str_correo   El nuevo correo del usuario.
     * @param str_clave    La nueva clave del usuario.
     * 
     * @return True si se ha realizado correctamente o false si no existe ese
     * usuario en el sistema.
     */

    private void vo_modificarUsuario() 
    {
        String str_nombre;
        String str_apellido;
        String str_dni;
        String str_correo;
        String str_clave;

        System.out.println("Dime el DNI del usuario que deseas modificar");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();
        boolean bln_resultado;
        bln_resultado = obj_gestor.bln_comprobarUsuarioExistente(str_dni);

        if (bln_resultado) 
        {
            String str_datos;
            str_datos = obj_gestor.vo_visualizarDatosUsuario(str_dni);

            System.out.println("-------------------------------");
            System.out.println("Introduce el nombre");
            System.out.println("-------------------------------");
            System.out.print("NOMBRE: ");
            str_nombre = UtilidadesLP.leerCadena();
            System.out.println();

            System.out.println("-------------------------------");
            System.out.println("DATOS A MODIFICAR DEL USUARIO");
            System.out.println("-------------------------------");
            System.out.println(str_datos);
            System.out.println("-------------------------------");
            System.out.println();

            System.out.println("-------------------------------");
            System.out.println("Introduce el apellido");
            System.out.println("-------------------------------");
            System.out.print("APELLIDO: ");
            str_apellido = UtilidadesLP.leerCadena();
            System.out.println();

            System.out.println("-------------------------------");
            System.out.println("Introduce el correo");
            System.out.println("-------------------------------");
            System.out.print("CORREO: ");
            str_correo = UtilidadesLP.leerCadena();
            System.out.println();

            System.out.println("-------------------------------");
            System.out.println("Introduce la clave");
            System.out.println("-------------------------------");
            System.out.print("CLAVE: ");
            str_clave = UtilidadesLP.leerCadena();
            System.out.println();

            obj_gestor.bln_borrarUsuario(str_dni);
            try
            {
            obj_gestor.bln_insertarUsuario(str_nombre, str_apellido, str_dni,
                    str_correo, str_clave);
            }
            catch(clsUsuarioRepetido e)
            {
                System.out.println("Ese usuario esta repetido!");
       
            }
            catch(clsDniFormatoIncorrecto e)
            {
                System.out.println("El dni no tiene un formato correcto!"); 

            }

            System.out.println("------------------------");
            System.out.println("MODIFICACION REALIZADA");
            System.out.println("-----------------------");
            vo_pausa();

        } else 
        {
            System.out.println("------------------------------------");
            System.out.println("      NO EXISTE ESTE USUARIO        ");
            System.out.println("-- DEBE AÑADIRLO DESDE EL SUBMENU --");
            System.out.println("------ CORRESPONDIENTE -------------");
            System.out.println("------------------------------------");
            vo_pausa();
        }
    }

    /**
     * Este metodo permite al administrador ver la lista de usuarios registrados
     * en el sistema.
     */
    private void vo_visualizarUsuarios() 
    {
        List<itfProperty> arrl_infoUsuarios;
        arrl_infoUsuarios = obj_gestor.arrl_recuperarInfoUsuarios();

        if (arrl_infoUsuarios.isEmpty()) 
        {
            System.out.println("-----------------------------");
            System.out.println("No hay usuarios para mostrar");
            System.out.println("-----------------------------");
            vo_pausa();
        } else 
        {
            System.out.println("---------------------------");
            System.out.println("INFORMACION DE USUARIOS:");
            System.out.println("---------------------------");

            for (itfProperty itf_usuario : arrl_infoUsuarios) {
                System.out.print("Nombre: " +
                        itf_usuario.getObjectProperty(clsConstantes.NOMBRE));
                System.out.println();

                System.out.print("Apellido: " +
                        itf_usuario.getObjectProperty(clsConstantes.APELLIDOS));
                System.out.println();

                System.out.print("DNI: " +
                        itf_usuario.getObjectProperty(clsConstantes.DNI));
                System.out.println();

                System.out.print("Correo: " +
                        itf_usuario.getObjectProperty(clsConstantes.CORREO));
                System.out.println();

                System.out.println("---------------------------------------");
            }
            vo_pausa();
        }
    }

    /**
     * Este metodo permite realizar una pausa en el proceso cuando lo
     * indiquemos.
     */
    private void vo_pausa() 
    {
        System.out.println("Pulsa cualquier tecla para continuar...");
        UtilidadesLP.leerCaracter();
    }

    /**
     * Este metodo permite visualizar el menu si el usuario es un invitado.
     */
    private void vo_mostrarMenuInvitado() 
    {
        int int_opcion;
        int_opcion = -1;

        do 
        {
            System.out.println("---------------------------------");
            System.out.println("QUE INFORMACIÓN ESTAS BUSCANDO");
            System.out.println("---------------------------------");
            System.out.println("1. Informacion de los vuelos");
            System.out.println("2. Informacion de los alojamientos");
            System.out.println("------------------------------------");
            System.out.println("3. Volver al menu principal");
            System.out.println("------------------------------------");
            System.out.print("Opcion:");

            int_opcion = UtilidadesLP.leerEntero();

            switch (int_opcion) 
            {
                case 1:
                    vo_visualizarVuelosOrdenados();
                    break;
                case 2:
                    vo_visualizarAlojamientos();
                    break;
            }
        } while (int_opcion != 3);
    }

    /**
     * Este metodo permite mostrar el menu de reservas el cual le muestra al
     * usuario las posibles opciones: 1. Vuelo + Alojamiento. // 2. Vuelo. //
     * 3. Alojamiento. // 4. Volver al menu principal.
     * 
     */
    private void vo_mostrarMenuReserva() 
    {
        int int_opcion;
        int_opcion = -1;
        do 
        {
            System.out.println("------------------------------------");
            System.out.println("QUE TIPO DE RESERVA ESTAS BUSCANDO");
            System.out.println("------------------------------------");
            System.out.println("1. Vuelo + Alojamiento");
            System.out.println("2. Vuelo");
            System.out.println("3. Alojamiento");
            System.out.println("4. Buscar reservas realizadas");
            System.out.println("---------------------------");
            System.out.println("5. Volver al menu principal");
            System.out.println("------------------------------------");
            System.out.print("Opcion:");

            int_opcion = UtilidadesLP.leerEntero();

            switch (int_opcion) 
            {
                case 1:
                    vo_reservarVuelo();
                    vo_visualizarAlojamientos();
                    vo_reservarAlojamiento();
                    break;
                case 2:
                    vo_reservarVuelo();
                    break;
                case 3:
                    vo_visualizarAlojamientos();
                    vo_reservarAlojamiento();
                    break;
                case 4:
                    vo_visualizarVuelosReservado();
                    break;
            }
        } while (int_opcion != 5);
    }

    /**
     * Este metodo permite visualizar los alojamientos.
     */
    private void vo_visualizarAlojamientos() 
    {
        ArrayList<itfProperty> arrl_infoAlojamientos;
        arrl_infoAlojamientos = 
        obj_gestor.getInfoAlojamientosOrdenadosDestino();

        if (arrl_infoAlojamientos.isEmpty()) 
        {
            System.out.println("---------------------------------");
            System.out.println("No hay alojamientos para mostrar");
            System.out.println("---------------------------------");
            vo_pausa();
        } else 
        {
            System.out.println("-----------------------------");
            System.out.println("INFORMACION DE ALOJAMIENTOS:");
            System.out.println("-----------------------------");

            for (itfProperty obj_aloj : arrl_infoAlojamientos) {
                System.out.print("Calle: " +
                obj_aloj.getObjectProperty(clsConstantes.DESTINO_ALOJAMIENTO));
                System.out.println();

                System.out.print("Numero de habitaciones: " +
                obj_aloj.getObjectProperty(clsConstantes.NUMERO_HABITACIONES));
                System.out.println();

                System.out.print("Huespedes posibles: " +
                obj_aloj.getObjectProperty(clsConstantes.CANTIDAD));
                System.out.println();

                System.out.print("Propietario: " +
                obj_aloj.getObjectProperty(clsConstantes.PROPIETARIO));
                System.out.println();

                System.out.println("Precio por noche: " +
                obj_aloj.getObjectProperty(clsConstantes.PRECIO_ALOJAMIENTO));

                System.out.println("---------------------------------------");
            }
            vo_pausa();
        }
    }

    /**
     * Este metodo permite visualizar los vuelos ordenados por precio.
     */
    private void vo_visualizarVuelosOrdenados() 
    {
        List<itfProperty> vuelos = obj_gestor.getVuelosOrdenadosId();

        System.out.println("-----------------------------------");
        System.out.println("ORIGENES POSIBLES ORDENADOS POR ID");
        System.out.println("-----------------------------------");

        for (itfProperty r : vuelos) 
        {
            System.out.println("Origen: " +
                    r.getObjectProperty(clsConstantes.ORIGEN));

            System.out.println("ID: " +
                    r.getObjectProperty(clsConstantes.ID_VUELO));

            System.out.println("---------------------------------------");
        }
        vo_pausa();
    }

    /**
     * 
     * Este metodo permitira al administrador visualizar la lista de reservas de
     * los vuelos realizados.
     */

    private void vo_reservarVuelo() 
    {
        int int_opcion;
        int int_cantidadEquipaje;
        String str_dni;
        int int_numeroPasajeros;
        int int_precio;
        boolean bln_resultado;

        do 
        {
            System.out.println("-----------------------------");
            System.out.println("DESDE DONDE QUIERES VOLAR");
            System.out.println("-----------------------------");

            List<itfProperty> itf_vuelos = obj_gestor.getVuelosOrdenadosId();

            for (itfProperty r : itf_vuelos) 
            {
                System.out.println(r.getObjectProperty(clsConstantes.ORIGEN));
            }
            
            System.out.print("OPCION: ");

            int_opcion = UtilidadesLP.leerEntero();

        } while (int_opcion != 1 && int_opcion != 2 && int_opcion != 3 &&
                int_opcion != 4 && int_opcion != 5 && int_opcion != 6 &&
                int_opcion != 7);

        switch (int_opcion) 
        {
            // Torrevieja
            case 1:
                int int_opcionDestino1;
                int int_opcionOrden;

            do 
            {
                do
                {
                System.out.println("-------------------------------");
                System.out.println("¿COMO QUIERES ORDENAR LOS VUELOS?");
                System.out.println("-------------------------------");
                System.out.println("1. ID");
                System.out.println("2. PRECIO");
                System.out.println("-------------------------------");
                } while (int_opcion != 1 && int_opcion != 2);
                System.out.print("OPCION: ");
                int_opcionOrden = UtilidadesLP.leerEntero();

                if (int_opcionOrden == 2) 
                {
                System.out.println("-------------------------------");
                System.out.println("VUELOS DISPONIBLES ORDENADOS POR PRECIO");
                System.out.println("-------------------------------");

                List<itfProperty> itf_vuelos = obj_gestor.
                getVuelosOrdenadosPrecioTorre();
                
                for (itfProperty r : itf_vuelos) 
                {
                    System.out.println("DESTINO: " +
                    r.getObjectProperty(clsConstantes.DESTINO_VUELO));
                    System.out.println("HORA DE SALIDA: " +
                    r.getObjectProperty(clsConstantes.HORA_SALIDA));
                    System.out.println("HORA DE LLEGADA: " +
                    r.getObjectProperty(clsConstantes.HORA_LLEGADA));
                    System.out.println("PRECIO: " +
                    r.getObjectProperty(clsConstantes.PRECIO_VUELO) + " euros");
                    System.out.println("ID: " +
                    r.getObjectProperty(clsConstantes.ID_VUELO));
                    System.out.println("---------------------------");
                }
                }
                if (int_opcionOrden == 1) 
                {
                    System.out.println("-------------------------------");
                    System.out.println("VUELOS DISPONIBLES ORDENADOS POR ID");
                    System.out.println("-------------------------------");

                    List<itfProperty> itf_vuelos = obj_gestor.
                    getVuelosTorrevieja();
                    
                    for (itfProperty r : itf_vuelos) 
                    {
                        System.out.println("DESTINO: " +
                            r.getObjectProperty(clsConstantes.DESTINO_VUELO));
                        System.out.println("HORA DE SALIDA: " +
                            r.getObjectProperty(clsConstantes.HORA_SALIDA));
                        System.out.println("HORA DE LLEGADA: " +
                            r.getObjectProperty(clsConstantes.HORA_LLEGADA));
                        System.out.println("PRECIO: " +
                            r.getObjectProperty(clsConstantes.PRECIO_VUELO) +
                                " euros");
                        System.out.println("ID: " +
                            r.getObjectProperty(clsConstantes.ID_VUELO));

                        System.out.println("---------------------------");
                    }
                }
                System.out.print("OPCION: ");
                int_opcionDestino1 = UtilidadesLP.leerEntero();
            } while (int_opcionDestino1 < 1 && int_opcionDestino1 > 3);
        switch (int_opcionDestino1) 
        {
        case 1:

        System.out.println("-------------------------------");
        System.out.print(
        "Dime el DNI del pasajero representante de tu reserva");
        System.out.println("-------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("-------------------------------");
        System.out.println("-------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 35 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("-------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();

        bln_resultado = obj_gestor.bln_anadirReservaVuelo(
        "Torrevieja", 
        "Vitoria-Gasteiz",
        "1:45 pm", 
        "3 pm",
        str_dni, 
        int_numeroPasajeros, 
        int_cantidadEquipaje,
        int_precio);

        if (bln_resultado) 
        {
        System.out.println("---------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("---------------------------");
        vo_pausa();

        System.out.println("---------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------");
        System.out.println("ORIGEN: Torrevieja");
        System.out.println("DESTINO: Vitoria");
        System.out.println("HORA DE SALIDA: 1:45 pm");
        System.out.println("HORA DE SALIDA: 3 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("---------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------");
        vo_pausa();
        }
        break;

        case 2:

        System.out.println("-------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("-------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("-------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 30 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("-------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();

        bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Torrevieja", 
                "Malaga",
                "5 pm", 
                "5:30 pm",
                str_dni, 
                int_numeroPasajeros, 
                int_cantidadEquipaje,
                int_precio);

        if (bln_resultado) 
        {
        System.out.println("---------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("---------------------------");
        vo_pausa();

        System.out.println("---------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------");
        System.out.println("ORIGEN: Torrevieja");
        System.out.println("DESTINO: Malaga");
        System.out.println("HORA DE SALIDA: 5 pm");
        System.out.println("HORA DE SALIDA: 5:30 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("---------------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------");
        vo_pausa();
        }
        break;

        case 3:

        System.out.println("-------------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("-------------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("-------------------------------------------");
        System.out.println("-------------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 20 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("-------------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();

        bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Torrevieja", "Madrid",
                "7:15 pm", "7:45 pm",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje,
                int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Torrevieja");
        System.out.println("DESTINO: Madrid");
        System.out.println("HORA DE SALIDA: 7:15 pm");
        System.out.println("HORA DE SALIDA: 7:45 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("---------------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;
        }
        break;
        // MALAGA
        case 2:

        int int_opcionDestino2;
        do 
            {
                do
                {
                System.out.println("-------------------------------");
                System.out.println("¿COMO QUIERES ORDENAR LOS VUELOS?");
                System.out.println("-------------------------------");
                System.out.println("1. ID");
                System.out.println("2. PRECIO");
                System.out.println("-------------------------------");
                } while (int_opcion != 1 && int_opcion != 2);
                System.out.print("OPCION: ");
                int_opcionOrden = UtilidadesLP.leerEntero();

                if (int_opcionOrden == 2) 
                {
                System.out.println("-------------------------------");
                System.out.println("VUELOS DISPONIBLES ORDENADOS POR PRECIO");
                System.out.println("-------------------------------");

                List<itfProperty> itf_vuelos = obj_gestor.
                getVuelosOrdenadosPrecioMalaga();
                
                for (itfProperty r : itf_vuelos) 
                {
                    System.out.println("DESTINO: " +
                    r.getObjectProperty(clsConstantes.DESTINO_VUELO));
                    System.out.println("HORA DE SALIDA: " +
                    r.getObjectProperty(clsConstantes.HORA_SALIDA));
                    System.out.println("HORA DE LLEGADA: " +
                    r.getObjectProperty(clsConstantes.HORA_LLEGADA));
                    System.out.println("PRECIO: " +
                    r.getObjectProperty(clsConstantes.PRECIO_VUELO) + " euros");
                    System.out.println("ID: " +
                    r.getObjectProperty(clsConstantes.ID_VUELO));
                    System.out.println("---------------------------");
                }
                }
                if (int_opcionOrden == 1) 
                {
                    System.out.println("-------------------------------");
                    System.out.println("VUELOS DISPONIBLES ORDENADOS POR ID");
                    System.out.println("-------------------------------");

                    List<itfProperty> itf_vuelos = obj_gestor.
                    getVuelosMalaga();
                    
                    for (itfProperty r : itf_vuelos) 
                    {
                        System.out.println("DESTINO: " +
                            r.getObjectProperty(clsConstantes.DESTINO_VUELO));
                        System.out.println("HORA DE SALIDA: " +
                            r.getObjectProperty(clsConstantes.HORA_SALIDA));
                        System.out.println("HORA DE LLEGADA: " +
                            r.getObjectProperty(clsConstantes.HORA_LLEGADA));
                        System.out.println("PRECIO: " +
                            r.getObjectProperty(clsConstantes.PRECIO_VUELO) +
                                " euros");
                        System.out.println("ID: " +
                            r.getObjectProperty(clsConstantes.ID_VUELO));

                        System.out.println("---------------------------");
                    }
                }
                System.out.print("OPCION: ");
                int_opcionDestino2 = UtilidadesLP.leerEntero();
            } while (int_opcionDestino2 < 1 && int_opcionDestino2 > 3);
        switch (int_opcionDestino2) 
        {
        case 1:
        System.out.println("-------------------------------");
        System.out.print("Dime el DNI del pasajero representante");
        System.out.println("de tu reserva");
        System.out.println("-------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("-------------------------------");
        System.out.println("-------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 30 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("-------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();
        bln_resultado = obj_gestor.bln_anadirReservaVuelo(
            "Malaga", 
            "Torrevieja",
            "6 pm", 
            "6:30 pm",
            str_dni, 
            int_numeroPasajeros, 
            int_cantidadEquipaje,
            int_precio);

        if (bln_resultado)
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Malaga");
        System.out.println("DESTINO: Torrevieja");
        System.out.println("HORA DE SALIDA: 6 pm");
        System.out.println("HORA DE SALIDA: 6:30 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;

        case 2:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 50 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();

            bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Malaga", "Vitoria-Gasteiz",
                "6:30 pm", "8:30 pm",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje, int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Malaga");
        System.out.println("DESTINO: Vitoria-Gasteiz");
        System.out.println("HORA DE SALIDA: 6:30 pm");
        System.out.println("HORA DE SALIDA: 8:30 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;

        case 3:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 55 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();

        bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Torrevieja", "Malaga",
                "5:40 pm", "6:10 pm",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje,int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Torrevieja");
        System.out.println("DESTINO: Malaga");
        System.out.println("HORA DE SALIDA: 5:40 pm");
        System.out.println("HORA DE SALIDA: 6:10 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;
        }
        break;

        // ALBACETE
        case 3:

        int int_opcionDestino3;
        do 
            {
                do
                {
                System.out.println("-------------------------------");
                System.out.println("¿COMO QUIERES ORDENAR LOS VUELOS?");
                System.out.println("-------------------------------");
                System.out.println("1. ID");
                System.out.println("2. PRECIO");
                System.out.println("-------------------------------");
                } while (int_opcion != 1 && int_opcion != 2);
                System.out.print("OPCION: ");
                int_opcionOrden = UtilidadesLP.leerEntero();

                if (int_opcionOrden == 2) 
                {
                System.out.println("-------------------------------");
                System.out.println("VUELOS DISPONIBLES ORDENADOS POR PRECIO");
                System.out.println("-------------------------------");

                List<itfProperty> itf_vuelos = obj_gestor.
                getVuelosOrdenadosPrecioAlbacete();
                
                for (itfProperty r : itf_vuelos) 
                {
                    System.out.println("DESTINO: " +
                    r.getObjectProperty(clsConstantes.DESTINO_VUELO));
                    System.out.println("HORA DE SALIDA: " +
                    r.getObjectProperty(clsConstantes.HORA_SALIDA));
                    System.out.println("HORA DE LLEGADA: " +
                    r.getObjectProperty(clsConstantes.HORA_LLEGADA));
                    System.out.println("PRECIO: " +
                    r.getObjectProperty(clsConstantes.PRECIO_VUELO) + " euros");
                    System.out.println("ID: " +
                    r.getObjectProperty(clsConstantes.ID_VUELO));
                    System.out.println("---------------------------");
                }
                }
                if (int_opcionOrden == 1) 
                {
                    System.out.println("-------------------------------");
                    System.out.println("VUELOS DISPONIBLES ORDENADOS POR ID");
                    System.out.println("-------------------------------");

                    List<itfProperty> itf_vuelos = obj_gestor.
                    getVuelosAlbacete();
                    
                    for (itfProperty r : itf_vuelos) 
                    {
                        System.out.println("DESTINO: " +
                            r.getObjectProperty(clsConstantes.DESTINO_VUELO));
                        System.out.println("HORA DE SALIDA: " +
                            r.getObjectProperty(clsConstantes.HORA_SALIDA));
                        System.out.println("HORA DE LLEGADA: " +
                            r.getObjectProperty(clsConstantes.HORA_LLEGADA));
                        System.out.println("PRECIO: " +
                            r.getObjectProperty(clsConstantes.PRECIO_VUELO) +
                                " euros");
                        System.out.println("ID: " +
                            r.getObjectProperty(clsConstantes.ID_VUELO));

                        System.out.println("---------------------------");
                    }
                }
                System.out.print("OPCION: ");
                int_opcionDestino3 = UtilidadesLP.leerEntero();
            } while (int_opcionDestino3 < 1 && int_opcionDestino3 > 3);

        switch (int_opcionDestino3) 
        {
        case 1:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 15 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();

        bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Albacete", "Bilbao",
                "10:25 am", "11:20 am",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje, int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Albacete");
        System.out.println("DESTINO: Bilbao");
        System.out.println("HORA DE SALIDA: 10:25 am");
        System.out.println("HORA DE SALIDA: 11:20 am");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;
        }
        break;

        //MALDIVAS
        case 6:

        int int_opcionDestino4;
        do 
            {
                do
                {
                System.out.println("-------------------------------");
                System.out.println("¿COMO QUIERES ORDENAR LOS VUELOS?");
                System.out.println("-------------------------------");
                System.out.println("1. ID");
                System.out.println("2. PRECIO");
                System.out.println("-------------------------------");
                } while (int_opcion != 1 && int_opcion != 2);
                System.out.print("OPCION: ");
                int_opcionOrden = UtilidadesLP.leerEntero();

                if (int_opcionOrden == 2) 
                {
                System.out.println("-------------------------------");
                System.out.println("VUELOS DISPONIBLES ORDENADOS POR PRECIO");
                System.out.println("-------------------------------");

                List<itfProperty> itf_vuelos = obj_gestor.
                getVuelosOrdenadosPrecioMaldivas();
                
                for (itfProperty r : itf_vuelos) 
                {
                    System.out.println("DESTINO: " +
                    r.getObjectProperty(clsConstantes.DESTINO_VUELO));
                    System.out.println("HORA DE SALIDA: " +
                    r.getObjectProperty(clsConstantes.HORA_SALIDA));
                    System.out.println("HORA DE LLEGADA: " +
                    r.getObjectProperty(clsConstantes.HORA_LLEGADA));
                    System.out.println("PRECIO: " +
                    r.getObjectProperty(clsConstantes.PRECIO_VUELO) + " euros");
                    System.out.println("ID: " +
                    r.getObjectProperty(clsConstantes.ID_VUELO));
                    System.out.println("---------------------------");
                }
                }
                if (int_opcionOrden == 1) 
                {
                    System.out.println("-------------------------------");
                    System.out.println("VUELOS DISPONIBLES ORDENADOS POR ID");
                    System.out.println("-------------------------------");

                    List<itfProperty> itf_vuelos = obj_gestor.
                    getVuelosMaldivas();
                    
                    for (itfProperty r : itf_vuelos) 
                    {
                        System.out.println("DESTINO: " +
                            r.getObjectProperty(clsConstantes.DESTINO_VUELO));
                        System.out.println("HORA DE SALIDA: " +
                            r.getObjectProperty(clsConstantes.HORA_SALIDA));
                        System.out.println("HORA DE LLEGADA: " +
                            r.getObjectProperty(clsConstantes.HORA_LLEGADA));
                        System.out.println("PRECIO: " +
                            r.getObjectProperty(clsConstantes.PRECIO_VUELO) +
                                " euros");
                        System.out.println("ID: " +
                            r.getObjectProperty(clsConstantes.ID_VUELO));

                        System.out.println("---------------------------");
                    }
                }
                System.out.print("OPCION: ");
                int_opcionDestino4 = UtilidadesLP.leerEntero();
            } while (int_opcionDestino4 < 1 && int_opcionDestino4 > 3);

        switch (int_opcionDestino4) 
        {
        case 1:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 678 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();

            bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Maldivas", "Madrid",
                "9:40 am", "10:30 pm",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje, int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Maldivas");
        System.out.println("DESTINO: Madrid");
        System.out.println("HORA DE SALIDA: 9:40 am");
        System.out.println("HORA DE SALIDA: 10:30 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;
        }
        break;

        // VITORIA
        case 4:

        int int_opcionDestino5;
        do 
            {
                do
                {
                System.out.println("-------------------------------");
                System.out.println("¿COMO QUIERES ORDENAR LOS VUELOS?");
                System.out.println("-------------------------------");
                System.out.println("1. ID");
                System.out.println("2. PRECIO");
                System.out.println("-------------------------------");
                } while (int_opcion != 1 && int_opcion != 2);
                System.out.print("OPCION: ");
                int_opcionOrden = UtilidadesLP.leerEntero();

                if (int_opcionOrden == 2) 
                {
                System.out.println("-------------------------------");
                System.out.println("VUELOS DISPONIBLES ORDENADOS POR PRECIO");
                System.out.println("-------------------------------");

                List<itfProperty> itf_vuelos = obj_gestor.
                getVuelosOrdenadosPrecioVitoria();
                
                for (itfProperty r : itf_vuelos) 
                {
                    System.out.println("DESTINO: " +
                    r.getObjectProperty(clsConstantes.DESTINO_VUELO));
                    System.out.println("HORA DE SALIDA: " +
                    r.getObjectProperty(clsConstantes.HORA_SALIDA));
                    System.out.println("HORA DE LLEGADA: " +
                    r.getObjectProperty(clsConstantes.HORA_LLEGADA));
                    System.out.println("PRECIO: " +
                    r.getObjectProperty(clsConstantes.PRECIO_VUELO) + " euros");
                    System.out.println("ID: " +
                    r.getObjectProperty(clsConstantes.ID_VUELO));
                    System.out.println("---------------------------");
                }
                }
                if (int_opcionOrden == 1) 
                {
                    System.out.println("-------------------------------");
                    System.out.println("VUELOS DISPONIBLES ORDENADOS POR ID");
                    System.out.println("-------------------------------");

                    List<itfProperty> itf_vuelos = obj_gestor.
                    getVuelosVitoria();
                    
                    for (itfProperty r : itf_vuelos) 
                    {
                        System.out.println("DESTINO: " +
                            r.getObjectProperty(clsConstantes.DESTINO_VUELO));
                        System.out.println("HORA DE SALIDA: " +
                            r.getObjectProperty(clsConstantes.HORA_SALIDA));
                        System.out.println("HORA DE LLEGADA: " +
                            r.getObjectProperty(clsConstantes.HORA_LLEGADA));
                        System.out.println("PRECIO: " +
                            r.getObjectProperty(clsConstantes.PRECIO_VUELO) +
                                " euros");
                        System.out.println("ID: " +
                            r.getObjectProperty(clsConstantes.ID_VUELO));

                        System.out.println("---------------------------");
                    }
                }
                System.out.print("OPCION: ");
                int_opcionDestino5 = UtilidadesLP.leerEntero();
            } while (int_opcionDestino5 < 1 && int_opcionDestino5 > 3);

        switch (int_opcionDestino5) 
        {
        case 1:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 35 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();

            bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Vitoria-Gasteiz", "Torrevieja",
                "12 am", "1:15 pm",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje,int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Vitoria-Gasteiz");
        System.out.println("DESTINO: Torrevieja");
        System.out.println("HORA DE SALIDA: 12 am");
        System.out.println("HORA DE SALIDA: 1:15 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;

        case 2:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 35 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();

            bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Vitoria-Gasteiz", "Malaga",
                "4 pm", "6 pm",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje, int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Vitoria-Gasteiz");
        System.out.println("DESTINO: Malaga");
        System.out.println("HORA DE SALIDA: 4 pm");
        System.out.println("HORA DE SALIDA: 6 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;

        case 3:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 70 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();
            bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Vitoria-Gasteiz", "Madrid",
                "7:15 pm", "8 pm",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje, int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Vitoria-Gasteiz");
        System.out.println("DESTINO: Madrid");
        System.out.println("HORA DE SALIDA: 7:15 pm");
        System.out.println("HORA DE SALIDA: 8 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;
        }
        break;

        // BILBAO
        case 5:

        int int_opcionDestino6;
        do 
        {
            do
            {
            System.out.println("-------------------------------");
            System.out.println("¿COMO QUIERES ORDENAR LOS VUELOS?");
            System.out.println("-------------------------------");
            System.out.println("1. ID");
            System.out.println("2. PRECIO");
            System.out.println("-------------------------------");
            } while (int_opcion != 1 && int_opcion != 2);
            System.out.print("OPCION: ");
            int_opcionOrden = UtilidadesLP.leerEntero();

            if (int_opcionOrden == 2) 
            {
            System.out.println("-------------------------------");
            System.out.println("VUELOS DISPONIBLES ORDENADOS POR PRECIO");
            System.out.println("-------------------------------");

            List<itfProperty> itf_vuelos = obj_gestor.
            getVuelosOrdenadosPrecioBilbao();
            
            for (itfProperty r : itf_vuelos) 
            {
                System.out.println("DESTINO: " +
                r.getObjectProperty(clsConstantes.DESTINO_VUELO));
                System.out.println("HORA DE SALIDA: " +
                r.getObjectProperty(clsConstantes.HORA_SALIDA));
                System.out.println("HORA DE LLEGADA: " +
                r.getObjectProperty(clsConstantes.HORA_LLEGADA));
                System.out.println("PRECIO: " +
                r.getObjectProperty(clsConstantes.PRECIO_VUELO) + " euros");
                System.out.println("ID: " +
                r.getObjectProperty(clsConstantes.ID_VUELO));
                System.out.println("---------------------------");
            }
            }
            if (int_opcionOrden == 1) 
            {
                System.out.println("-------------------------------");
                System.out.println("VUELOS DISPONIBLES ORDENADOS POR ID");
                System.out.println("-------------------------------");

                List<itfProperty> itf_vuelos = obj_gestor.
                getVuelosBilbao();
                
                for (itfProperty r : itf_vuelos) 
                {
                    System.out.println("DESTINO: " +
                        r.getObjectProperty(clsConstantes.DESTINO_VUELO));
                    System.out.println("HORA DE SALIDA: " +
                        r.getObjectProperty(clsConstantes.HORA_SALIDA));
                    System.out.println("HORA DE LLEGADA: " +
                        r.getObjectProperty(clsConstantes.HORA_LLEGADA));
                    System.out.println("PRECIO: " +
                        r.getObjectProperty(clsConstantes.PRECIO_VUELO) +
                            " euros");
                    System.out.println("ID: " +
                        r.getObjectProperty(clsConstantes.ID_VUELO));

                    System.out.println("---------------------------");
                }
            }
            System.out.print("OPCION: ");
            int_opcionDestino6 = UtilidadesLP.leerEntero();
        } while (int_opcionDestino6 < 1 && int_opcionDestino6 > 3);

        switch (int_opcionDestino6) 
        {
        case 1:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 15 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();

            bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Bilbao", "Albacete",
                "9 am", "9:55 am",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje, int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Bilbao");
        System.out.println("DESTINO: Albacete");
        System.out.println("HORA DE SALIDA: 9 am");
        System.out.println("HORA DE SALIDA: 9:55 am");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;

        case 2:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 50 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();
            bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Bilbao", "Malaga",
                "4 pm", "6:15 pm",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje, int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Bilbao");
        System.out.println("DESTINO: Malaga");
        System.out.println("HORA DE SALIDA: 4 pm");
        System.out.println("HORA DE SALIDA: 6:15 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;

        case 3:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 75 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();
            bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Bilbao", "Madrid",
                "7 pm", "7:55 pm",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje, int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Bilbao");
        System.out.println("DESTINO: Madrid");
        System.out.println("HORA DE SALIDA: 7 pm");
        System.out.println("HORA DE SALIDA: 7:55 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;
        }
        break;

        // MADRID
        case 7:

        int int_opcionDestino7;
        do 
            {
                do
                {
                System.out.println("-------------------------------");
                System.out.println("¿COMO QUIERES ORDENAR LOS VUELOS?");
                System.out.println("-------------------------------");
                System.out.println("1. ID");
                System.out.println("2. PRECIO");
                System.out.println("-------------------------------");
                } while (int_opcion != 1 && int_opcion != 2);
                System.out.print("OPCION: ");
                int_opcionOrden = UtilidadesLP.leerEntero();

                if (int_opcionOrden == 2) 
                {
                System.out.println("-------------------------------");
                System.out.println("VUELOS DISPONIBLES ORDENADOS POR PRECIO");
                System.out.println("-------------------------------");

                List<itfProperty> itf_vuelos = obj_gestor.
                getVuelosOrdenadosPrecioMadrid();
                
                for (itfProperty r : itf_vuelos) 
                {
                    System.out.println("DESTINO: " +
                    r.getObjectProperty(clsConstantes.DESTINO_VUELO));
                    System.out.println("HORA DE SALIDA: " +
                    r.getObjectProperty(clsConstantes.HORA_SALIDA));
                    System.out.println("HORA DE LLEGADA: " +
                    r.getObjectProperty(clsConstantes.HORA_LLEGADA));
                    System.out.println("PRECIO: " +
                    r.getObjectProperty(clsConstantes.PRECIO_VUELO) + " euros");
                    System.out.println("ID: " +
                    r.getObjectProperty(clsConstantes.ID_VUELO));
                    System.out.println("---------------------------");
                }
                }
                if (int_opcionOrden == 1) 
                {
                    System.out.println("-------------------------------");
                    System.out.println("VUELOS DISPONIBLES ORDENADOS POR ID");
                    System.out.println("-------------------------------");

                    List<itfProperty> itf_vuelos = obj_gestor.
                    getVuelosMadrid();
                    
                    for (itfProperty r : itf_vuelos) 
                    {
                        System.out.println("DESTINO: " +
                            r.getObjectProperty(clsConstantes.DESTINO_VUELO));
                        System.out.println("HORA DE SALIDA: " +
                            r.getObjectProperty(clsConstantes.HORA_SALIDA));
                        System.out.println("HORA DE LLEGADA: " +
                            r.getObjectProperty(clsConstantes.HORA_LLEGADA));
                        System.out.println("PRECIO: " +
                            r.getObjectProperty(clsConstantes.PRECIO_VUELO) +
                                " euros");
                        System.out.println("ID: " +
                            r.getObjectProperty(clsConstantes.ID_VUELO));

                        System.out.println("---------------------------");
                    }
                }
                System.out.print("OPCION: ");
                int_opcionDestino7 = UtilidadesLP.leerEntero();
            } while (int_opcionDestino7 < 1 && int_opcionDestino7 > 5);
        switch (int_opcionDestino7) 
        {
        case 1:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 55 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();
            bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Madrid", "Málaga",
                "4:40 pm", "5:10 pm",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje, int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Madrid");
        System.out.println("DESTINO: Málaga");
        System.out.println("HORA DE SALIDA: 4:40 pm");
        System.out.println("HORA DE SALIDA: 5:10 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: "+int_precio+" euros");
        System.out.println("---------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.print("ERROR, ");
        System.out.print("VUELO YA RESERVADO ");
        System.out.println("PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;

        case 2:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 75 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();
            bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Madrid", "Bilbao",
                "8:25 pm", "9:10 pm",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje, int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Madrid");
        System.out.println("DESTINO: Bilbao");
        System.out.println("HORA DE SALIDA: 8:25 pm");
        System.out.println("HORA DE SALIDA: 9:10 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;

        case 3:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 70 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();
            bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Madrid", "Vitoria-Gasteiz",
                "8:30 pm", "9:15 pm",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje, int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Madrid");
        System.out.println("DESTINO: Vitoria-Gasteiz");
        System.out.println("HORA DE SALIDA: 8:30 pm");
        System.out.println("HORA DE SALIDA: 9:15 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;

        case 4:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 65 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();
            bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Madrid", "Torrevieja",
                "8:30 pm", "9:15 pm",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje, int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Madrid");
        System.out.println("DESTINO: Torrevieja");
        System.out.println("HORA DE SALIDA: 8:30 pm");
        System.out.println("HORA DE SALIDA: 9:15 pm");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;

        case 5:

        System.out.println("---------------------------------------");
        System.out.print("Dime el DNI del pasajero representante ");
        System.out.println("de tu reserva");
        System.out.println("---------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("---------------------------------------");
        System.out.println("---------------------------------------");

        System.out.println("¿Cuantos pasajeros sois?");
        System.out.print("CANTIDAD: ");
        int_numeroPasajeros = UtilidadesLP.leerEntero();
        int_precio = 678 * int_numeroPasajeros;
        System.out.println(int_precio);

        System.out.println("---------------------------------------");
        System.out.print("¿Cuantas maletas llevais?, ");
        System.out.println("cada maleta son 20€");
        System.out.print("CANTIDAD: ");
        int_cantidadEquipaje = UtilidadesLP.leerEntero();
        int_precio = int_precio + (int_cantidadEquipaje * 20);
        System.out.println();
            bln_resultado = obj_gestor.bln_anadirReservaVuelo(
                "Madrid", "Maldivas",
                "8:30 pm", "9:10 am",
                str_dni, int_numeroPasajeros, int_cantidadEquipaje, int_precio);

        if (bln_resultado) 
        {
        System.out.println("----------------------------");
        System.out.println("EXCELENTE, VUELO RESERVADO");
        System.out.println("----------------------------");
        vo_pausa();

        System.out.println("---------------------------------");
        System.out.println("RESUMEN DE LA RESERVA DEL VUELO");
        System.out.println("---------------------------------");
        System.out.println("ORIGEN: Madrid");
        System.out.println("DESTINO: Maldivas");
        System.out.println("HORA DE SALIDA: 8:30 pm");
        System.out.println("HORA DE SALIDA: 9:10 am");
        System.out.println("NUMERO DE PASAJEROS: " + int_numeroPasajeros);
        System.out.println("CANTIDAD DE MALETAS: " + int_cantidadEquipaje);
        System.out.println("PRECIO: " + int_precio + " euros");
        System.out.println("-----------------------------------");
        System.out.println();
        vo_pausa();
        } else 
        {
        System.out.println("---------------------------------------");
        System.out.println("ERROR, VUELO YA RESERVADO PREVIAMENTE");
        System.out.println("---------------------------------------");
        vo_pausa();
        }
        break;
        }
        break;
        }
    }

    /**
     * Este metodo permite reservar alojamiento.
     */
    private void vo_reservarAlojamiento() 
    {
        //Variales
        int int_opcion;
        String str_dni;
        String str_fecha;
        java.time.LocalDate fecha_llegada;
        java.time.LocalDate fecha_salida;
        int int_numNoches;
        int int_precio;
        Boolean bln_resultado;

        do 
        {
            System.out.println("---------------------------------");
            System.out.println("EN QUE DESTINO TE QUIERES ALOJAR");
            System.out.println("---------------------------------");
            System.out.println("1. Torrevieja --> 100€ por noche");
            System.out.println("2. Málaga --> 115€ por noche");
            System.out.println("3. Albacete --> 80€ por noche");
            System.out.println("4. Maldivas --> 500 euros por noche");
            System.out.println("5. Vitoria --> NO DISPONIBLE");
            System.out.println("6. Bilbao --> NO DISPONIBLE");
            System.out.println("7. Madrid --> NO DISPONIBLE");
            System.out.print("DESTINO: ");

            int_opcion = UtilidadesLP.leerEntero();
        } while (int_opcion != 1 && int_opcion != 2 && int_opcion != 3 &&
                int_opcion != 4);

        switch (int_opcion) 
        {
        case 1:

        System.out.println("-------------------------------------------");
        System.out.print(
            "Dime el DNI del huesped representante de tu reserva");
        System.out.println("-------------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("-------------------------------------------");
        System.out.println("-------------------------------------------");

        System.out.println("Introduzca la fecha de llegada (dd-mm-aaaa):");
        str_fecha = UtilidadesLP.leerCadena();
        fecha_llegada = java.time.LocalDate.parse(str_fecha,
        DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        System.out.println("Introduzca la fecha de salida (dd-mm-aaaa):");
        str_fecha = UtilidadesLP.leerCadena();
        fecha_salida = java.time.LocalDate.parse(str_fecha,
        DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        System.out.println("¿Cuantas noches son en total?");
        System.out.println("CANTIDAD DE NOCHES: ");
        int_numNoches = UtilidadesLP.leerEntero();

        int_precio = int_numNoches * 100;

        bln_resultado = obj_gestor.bln_comprobarAlojamientoReservado(
            "Torrevieja", fecha_llegada, fecha_salida);

        if (bln_resultado) 
        {
            obj_gestor.vo_añadirReservaAlojamiento("Torrevieja",
            fecha_llegada, fecha_salida, str_dni, int_numNoches, int_precio);

        System.out.println("-----------------------");
        System.out.println("ALOJAMIENTO RESERVADO");
        System.out.println("-----------------------");
        vo_pausa();

        System.out.println("-----------------------");
        System.out.println("RESUMEN DE LA RESERVA");
        System.out.println("-----------------------");
        System.out.println("DESTINO: Torrevieja");
        System.out.println("NOMBRE HUESPED: " + str_dni);
        System.out.println("FECHA LLEGADA: " + fecha_llegada);
        System.out.println("FECHA SALIDA: " + fecha_salida);
        System.out.println("NUMERO DE NOCHES: " + int_numNoches);
        System.out.println("PRECIO: " + int_precio);
        vo_pausa();
        } else 
        {
        System.out.println("--------------------------------------");
        System.out.println("ALOJAMIENTO YA RESERVADO EN ESA FECHA");
        System.out.println("--------------------------------------");
        }
        break;

        case 2:

        System.out.println("-------------------------------------------");
        System.out.print("Dime el DNI del huesped representante ");
        System.out.println("de tu reserva");
        System.out.println("-------------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("-------------------------------------------");
        System.out.println("-------------------------------------------");

        System.out.println("Introduzca la fecha de llegada (dd-mm-aaaa):");
        str_fecha = UtilidadesLP.leerCadena();
        fecha_llegada = java.time.LocalDate.parse(str_fecha,
        DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        System.out.println("Introduzca la fecha de salida (dd-mm-aaaa):");
        str_fecha = UtilidadesLP.leerCadena();
        fecha_salida = java.time.LocalDate.parse(str_fecha,
        DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        System.out.println("¿Cuantas noches son en total?");
        System.out.println("CANTIDAD DE NOCHES: ");
        int_numNoches = UtilidadesLP.leerEntero();

        int_precio = int_numNoches * 115;

        bln_resultado = obj_gestor.bln_comprobarAlojamientoReservado(
                "Malaga", fecha_llegada, fecha_salida);

        if (bln_resultado) 
        {
            obj_gestor.vo_añadirReservaAlojamiento("Malaga",
            fecha_llegada, fecha_salida, str_dni, int_numNoches, int_precio);

        System.out.println("-----------------------");
        System.out.println("ALOJAMIENTO RESERVADO");
        System.out.println("-----------------------");
        vo_pausa();

        System.out.println("-----------------------");
        System.out.println("RESUMEN DE LA RESERVA");
        System.out.println("-----------------------");
        System.out.println("DESTINO: Malaga");
        System.out.println("NOMBRE HUESPED: " + str_dni);
        System.out.println("FECHA LLEGADA: " + fecha_llegada);
        System.out.println("FECHA SALIDA: " + fecha_salida);
        System.out.println("NUMERO DE NOCHES: " + int_numNoches);
        System.out.println("PRECIO: " + int_precio);
        vo_pausa();
        } else 
        {
        System.out.println("--------------------------------------");
        System.out.println("ALOJAMIENTO YA RESERVADO EN ESA FECHA");
        System.out.println("--------------------------------------");
        }
        break;

        case 3:

        System.out.println("-------------------------------------------");
        System.out.print("Dime el DNI del huesped representante ");
        System.out.println("de tu reserva");
        System.out.println("-------------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("-------------------------------------------");
        System.out.println("-------------------------------------------");

        System.out.println("Introduzca la fecha de llegada (dd-mm-aaaa):");
        str_fecha = UtilidadesLP.leerCadena();
        fecha_llegada = java.time.LocalDate.parse(str_fecha,
        DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        System.out.println("Introduzca la fecha de salida (dd-mm-aaaa):");
        str_fecha = UtilidadesLP.leerCadena();
        fecha_salida = java.time.LocalDate.parse(str_fecha,
        DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        System.out.println("¿Cuantas noches son en total?");
        System.out.println("CANTIDAD DE NOCHES: ");
        int_numNoches = UtilidadesLP.leerEntero();

        int_precio = int_numNoches * 80;

        bln_resultado = obj_gestor.bln_comprobarAlojamientoReservado(
            "Fuentealbilla", fecha_llegada, fecha_salida);

        if (bln_resultado) 
        {
        obj_gestor.vo_añadirReservaAlojamiento("Fuentealbilla",
            fecha_llegada, fecha_salida, str_dni, int_numNoches, int_precio);

        System.out.println("-----------------------");
        System.out.println("ALOJAMIENTO RESERVADO");
        System.out.println("-----------------------");
        vo_pausa();
        System.out.println("-----------------------");
        System.out.println("RESUMEN DE LA RESERVA");
        System.out.println("-----------------------");
        System.out.println("DESTINO: Malaga");
        System.out.println("NOMBRE HUESPED: " + str_dni);
        System.out.println("FECHA LLEGADA: " + fecha_llegada);
        System.out.println("FECHA SALIDA: " + fecha_salida);
        System.out.println("NUMERO DE NOCHES: " + int_numNoches);
        System.out.println("PRECIO: " + int_precio);
        vo_pausa();
        } else 
        {
        System.out.println("--------------------------------------");
        System.out.println("ALOJAMIENTO YA RESERVADO EN ESA FECHA");
        System.out.println("--------------------------------------");
        }
        break;

        case 4:

        System.out.println("-------------------------------------------");
        System.out.print("Dime el DNI del huesped representante ");
        System.out.println("de tu reserva");
        System.out.println("-------------------------------------------");
        System.out.print("DNI: ");
        str_dni = UtilidadesLP.leerCadena();

        System.out.println("-------------------------------------------");
        System.out.println("-------------------------------------------");

        System.out.println("Introduzca la fecha de llegada (dd-mm-aaaa):");
        str_fecha = UtilidadesLP.leerCadena();
        fecha_llegada = java.time.LocalDate.parse(str_fecha,
        DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        System.out.println("Introduzca la fecha de salida (dd-mm-aaaa):");
        str_fecha = UtilidadesLP.leerCadena();
        fecha_salida = java.time.LocalDate.parse(str_fecha,
        DateTimeFormatter.ofPattern("dd-MM-yyyy"));

        System.out.println("¿Cuantas noches son en total?");
        System.out.println("CANTIDAD DE NOCHES: ");
        int_numNoches = UtilidadesLP.leerEntero();

        int_precio = int_numNoches * 500;

        bln_resultado = obj_gestor.bln_comprobarAlojamientoReservado(
                "Maldivas", fecha_llegada, fecha_salida);

        if (bln_resultado) 
        {
            obj_gestor.vo_añadirReservaAlojamiento("Maldivas",
            fecha_llegada, fecha_salida, str_dni, int_numNoches, int_precio);

        System.out.println("-----------------------");
        System.out.println("ALOJAMIENTO RESERVADO");
        System.out.println("-----------------------");
        vo_pausa();
        System.out.println("-----------------------");
        System.out.println("RESUMEN DE LA RESERVA");
        System.out.println("-----------------------");
        System.out.println("DESTINO: Maldivas");
        System.out.println("NOMBRE HUESPED: " + str_dni);
        System.out.println("FECHA LLEGADA: " + fecha_llegada);
        System.out.println("FECHA SALIDA: " + fecha_salida);
        System.out.println("NUMERO DE NOCHES: " + int_numNoches);
        System.out.println("PRECIO: " + int_precio);
        vo_pausa();
        } else 
        {
        System.out.println("--------------------------------------");
        System.out.println("ALOJAMIENTO YA RESERVADO EN ESA FECHA");
        System.out.println("--------------------------------------");
        }
        break;
        }
    }
}