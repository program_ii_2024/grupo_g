import LP.dlgVentanaLogin;

/**
 * 
 * Esta es la clase clsMain que es desde donde comienza el programa
 * 
 */
public class clsMain
{
    /**
     * Metodo main del programa.
     * @param args El main del programa
     */
    public static void main (String [] args)
    {
        new dlgVentanaLogin();
    }

    /**
     * Constructor en vacio
     */
    public clsMain()
    {
        
    }
}