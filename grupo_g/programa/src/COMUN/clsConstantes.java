package COMUN;

/**
 * Esta clase son las constantes para diferentes aspectos del programa. Contiene
 * constantes relacionadas con usuarios, reservas, vuelos, alojamientos y
 * valoraciones.
 */
public class clsConstantes 
{
    //USUARIO
    /**UBICACION DEL USUARIO */
    public static final String UBICACION = "UBICACION DEL USUARIO";

    //USUARIO REGISTRADO

    /**NOMBRE DEL USUARIO REGISTRADO*/
    public static final String NOMBRE = "NOMBRE DEL USUARIO";
    /**APELLIDO DEL USUARIO REGISTRADO*/
    public static final String APELLIDOS = "APELLIDO DEL USUARIO";
    /**DNI DEL USUARIO REGISTRADO*/
    public static final String DNI = "DNI DEL USUARIO";
    /**CORREO DEL USUARIO REGISTRADO*/
    public static final String CORREO = "CORREO DEL USUARIO";
    /**CLAVE DEL USUARIO REGISTRADO */
    public static final String CLAVE = "CLAVE DEL USUARIO";
    /**ROL DEL USUARIO REGISTRADO*/
    public static final String ROL = "EL ROL DEL USUARIO";


    //USUARIO INVITADO

    /**NUMERO DE ACCESOS DEL USUARIO INVITADO*/
    public static final String NUMACCESOS = "NUMERO DE ACCESOS DEL USUARIO";


    //RESERVAS

    /**ID DE LA RESERVA*/
    public static final String ID_RESERVA = "ID DE LA RESERVA";
    /**DIA DE COMIENZO DE LA RESERVA*/
    public static final String FECHA_INICIO = "DIA DEL COMIENZO DE LA RESERVA";
    /**DIA FINAL DE LA RESERVA*/
    public static final String FECHA_FINAL = "DIA FEL FINAL DE LA RESERVA";
    /**MES DE LA RESERVA*/
    public static final String MES = "EN QUE MES ES LA RESERVA";
    /**NOMBRE DEL REPRESENTANTE DE LA RESERVA*/
    public static final String NOMBRE_RESERVA = "NOMBRE DEL REPRESENTANTE" + 
                                                               " DE LA RESERVA";

    //VUELO

    /**HORA DE SALIDA DEL VUELO*/
    public static final String HORA_SALIDA = "HORA DE SALIDA DEL AVION";
    /**HORA DE LLEGADA DEL VUELO*/
    public static final String HORA_LLEGADA = "HORA DE LLEGADA DEL AVION";
    /**DESTINO DEL VUELO*/
    public static final String DESTINO_VUELO = "DESTINO DEL VUELO";
    /**CANTIDAD DE EQUIPAJE DEL VUELO*/
    public static final String CANTIDAD_EQUIPAJE = "CANTIDAD DE EQUIPAJE";
    /**PRECIO DEL VUELO*/
    public static final String PRECIO_VUELO = "PRECIO DEL VUELO";
    /**NOMBRE DEL PASAJERO REPRESENTANTE DEL VUELO*/
    public static final String NOMBRE_PASAJERO =
     "NOMBRE DEL PASAJERO PRINCIPAL";
    /**NUMERO DE PASAJEROS DEL VUELO*/
    public static final String NUMERO_PASAJEROS = "NUMERO DE PASAJEROS";
    /**ID DEL VUELO*/
    public static final String ID_VUELO = "ID DEL VUELO RESERVADO";
    /**ORIGEN DEL VUELO*/
    public static final String ORIGEN = "ORIGEN DEL VUELO";
    
    
    //ALOJAMIENTO

    /**UBICACION DEL ALOJAMIENTO*/
    public static final String DESTINO_ALOJAMIENTO = 
    "UBICACION EN LA QUE ESTA EL ALOJAMIENTO";
    /**NUMERO DE HABITACIONES DEL ALOJAMIENTO*/
    public static final String NUMERO_HABITACIONES = "NUMERO DE HABITACIONES";
    /**CANTIDAD DE HUESPEDES DEL ALOJAMIENTO*/
    public static final String CANTIDAD = "CANTIDAD DE PERSONAS";
    /**NOMBRE DEL PROPIETARIO DEL ALOJAMIENTO*/
    public static final String PROPIETARIO = "NOMBRE DEL PROPIETARIO";
    /**NOMBRE DEL REPRESENTANTE DE LA RESERVA DEL ALOJAMIENTO*/
    public static final String NOMBRE_HUESPED = 
    "NOMBRE DEL HUESPED REPRESENTANTE";
    /**FECHA DE LLEGADA AL ALOJAMIENTO*/
    public static final String FECHA_LLEGADA = 
    "FECHA DE LLEGADA AL ALOJAMIENTO";
    /**FECHA DE SALIDA DEL ALOJAMIENTO ALOJAMIENTO*/
    public static final String FECHA_SALIDA = "FECHA DE SALIDA DEL ALOJAMIENTO";
    /**PRECIO DEL ALOJAMIENTO*/
    public static final String PRECIO_ALOJAMIENTO = "PRECIO DEL ALOJAMIENTO";
    /**NUMERO DE NOCHES EN EL ALOJAMIENTO*/
    public static final String NUMERO_NOCHES = 
    "NUMERO DE NOCHES EN EL ALOJAMIENTO";


    //VALORACIONES   

    /**NUMERO DE ESTRELLAS DEL ALOJAMIENTOS*/
    public static final String NUMERO_ESTRELLAS = "NUMERO DE ESTRELLAS";
    /**NOTA DEL ALOJAMIENTO*/
    public static final String NOTA = "NOTA DEL ALOJAMIENTO";
    /**COMENTARIOS DEL ALOJAMIENTO*/
    public static final String COMENTARIO = "COMENTARIOS DE LOS USUARIOS";

    /**
     * Constructor vacío
     */
    public clsConstantes()
    {

    }
}