package COMUN;

/**
 * Sirve para comunicarnos entre LP y LN de manera segura.
 */
public interface itfProperty 
{
    /**
     * Sirve para obtener una propiedad.
     * 
     * @param str_propiedad La propiedad solicitada.
     * @return La propiedad que has pedido.
     * 
     */
    public Object getObjectProperty (String str_propiedad);
}