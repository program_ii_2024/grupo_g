package COMUN;
/**
 * Esta clase son las constantes de la base de datos en MySQL Workbench.
 */
public class clsConstantesLD 
{
    //USUARIOS REGISTRADOS

        /**INSERTAR USUARIO*/
        public static final String SQL_INSERT_USUARIO = 
        "INSERT INTO `deustoking`.`usuarios`"+
        " (NOMBRE, APELLIDO, DNI, CORREO, CLAVE, ROL) VALUES (?,?,?,?,?,?);"; 

        /**VISUALIZAR USUARIOS*/
        public static final String SQL_SELECT_USUARIOS = 
        "SELECT*FROM usuarios;";

        /**BORRAR USUARIOS*/
        public static final String SQL_DELETE_USUARIOS = 
        "delete from usuarios where (dni = ?);";

        /**MODIFICAR USUARIOS*/
        public static final String SQL_MODIFY_USUARIOS = 
        "update usuarios set NOMBRE = ?, APELLIDO = ?, DNI = ?, CORREO = ?,  " +
        "CLAVE = ? WHERE (DNI = ?)";


    //ALOJAMIENTOS

    /**VISUALIZAR ALOJAMIENTOS */
    public static final String SQL_SELECT_ALOJAMIENTOS = 
    "SELECT*FROM alojamientos;";

    //VUELOS REGISTRADOS

    /**VISUALIZAR VUELOS*/
    public static final String SQL_SELECT_VUELOS = 
    "SELECT * FROM aeropuertos;";
    /**VISUALIZAR VUELOS DE VITORIA */
    public static final String SQL_SELECT_VUELOS_VITORIA = 
    "select*from vuelos_vitoria;";
    /**VISUALIZAR VUELOS DE TORREVIEJA */
    public static final String SQL_SELECT_VUELOS_TORREVIEJA = 
    "select*from vuelos_torrevieja;";
    /**VISUALIZAR VUELOS DE MADRID */
    public static final String SQL_SELECT_VUELOS_MADRID = 
    "select*from vuelos_madrid;";
    /**VISUALIZAR VUELOS DE BILBAO */
    public static final String SQL_SELECT_VUELOS_BILBAO = 
    "select*from vuelos_bilbao;";
    /**VISUALIZAR VUELOS DE ALBACETE */
    public static final String SQL_SELECT_VUELOS_ALBACETE = 
    "select*from vuelos_albacete;";
    /**VISUALIZAR VUELOS DE MALAGA */
    public static final String SQL_SELECT_VUELOS_MALAGA = 
    "select*from vuelos_malaga;";
    /**VISUALIZAR VUELOS DE MALDIVAS */
    public static final String SQL_SELECT_VUELOS_MALDIVAS = 
    "select*from vuelos_maldivas;";

    /**
     * Constructor en vacio
     */
    public clsConstantesLD()
    {

    }
}