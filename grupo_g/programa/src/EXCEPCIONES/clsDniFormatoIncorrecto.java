package EXCEPCIONES;
/**
 * Esta clase es para la excepcion si el DNI introducido es de formato 
 * incorrecto.
 */
public class clsDniFormatoIncorrecto extends Exception
{   
    /**
     * Este metodo manda el mensaje de que el formato del DNI es incorrecto.
     * @param str_mensaje El mensaje que manda.
     */
    public clsDniFormatoIncorrecto(String str_mensaje)
    {
        super(str_mensaje);
    }
}