package EXCEPCIONES;

/**
 * Esta clase es para la excepcion si ya existe un usuario repetido o es igual
 * a uno ya creado anteriormente.
 */
public class clsUsuarioRepetido extends Exception
{
    /**
     * Este constructor inicializa la clase.
     */
    public clsUsuarioRepetido()
    {
        super();
    }
    /**
     * Esta clase sirve para enviar el mensaje de que ya existe un usuario igual
     * al que se intenta introducir.
     * @param str_mensaje Mensaje de la excepcion.
     */
    public clsUsuarioRepetido(String str_mensaje)
    {
        super(str_mensaje);
    }
}