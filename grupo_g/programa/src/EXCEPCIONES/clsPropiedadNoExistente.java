package EXCEPCIONES;
/**
 * Esta clase es para la excepcion si la propiedad no existe.
 */
public class clsPropiedadNoExistente extends RuntimeException
{
    /**
     * Este metodo manda el mensaje de que esa propiedad no existe.
     * @param str_mensaje El mensaje que manda.
     */
    public clsPropiedadNoExistente (String str_mensaje ) 
    {
        super(str_mensaje);
    }
}