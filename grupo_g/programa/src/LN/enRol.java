package LN;

/**
 * Esta enumerado tiene los roles posibles para los usuarios
 */
public enum enRol 
{
    /**
     * Rol de un usuario registrado en el sistema.
     */
    REGISTRADO,

    /**
     * Rol de un administrador en el sistema.
     */
    ADMINISTRADOR;
}
