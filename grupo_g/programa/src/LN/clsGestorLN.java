package LN;

import java.sql.*;
import java.time.LocalDate;
import java.util.*;

import COMUN.*;
import EXCEPCIONES.*;
import LD.*;

/**
 * Esta clase representa un gestor de usuarios y reservas en el sistema.
 * Permite la gestion de usuarios registrados y reservas realizadas.
 */
public class clsGestorLN 
{
    private clsGestorDatos obj_gld;

    /**
     * Aqui estamos declarando los conjuntos de informacion de diferentes
     * maneras
     */
    private ArrayList<clsUsuarioRegistrado> arrl_usuarioRegistrado;

    /** Mapa que mantiene una asociación entre IDs de usuarios y objetos de tipo
     *  clsUsuarioRegistrado. 
     */
    private Map<String, clsUsuarioRegistrado> map_Usuarios;

    /**
     * Arraylist de reservas realizadas por los usuarios.
     */
    private ArrayList<clsReserva> lst_reservas;

    /**
     * Arraylist de alojamientos disponibles.
     */
    private Set<clsAlojamiento> arrl_alojamientos;

    /**
     * Arraylist de alojamientos reservados.
     */
    private ArrayList<clsAlojamiento> arrl_alojamientosReservados;

    /**
     * Arraylist de vuelos reservados.
     */
    private ArrayList<clsVuelo> arrl_vuelosReservados;

    /**
     * Set de vuelos disponibles.
     */
    private Set<clsVuelo> set_vuelos;

    private Set<clsVuelo> set_vuelosVitoria;

    private Set<clsVuelo> set_vuelosMadrid;

    private Set<clsVuelo> set_vuelosBilbao;

    private Set<clsVuelo> set_vuelosMalaga;

    private Set<clsVuelo> set_vuelosAlbacete;

    private Set<clsVuelo> set_vuelosTorrevieja;

    private Set<clsVuelo> set_vuelosMaldivas;


    /** 
     * Este es el constructor que prepara los dos ArrayList para almacenar
     * usuarios y reservas respectivamente.
     * 
     */
    public clsGestorLN() 
    {
        /**
         * Inicializacion de las estructuras de datos para usuarios, reservas, 
         * alojamientos y vuelos.
         */
        map_Usuarios = new HashMap<String, clsUsuarioRegistrado>();
        lst_reservas = new ArrayList<>();
        arrl_alojamientos = new TreeSet<>();
        arrl_alojamientosReservados = new ArrayList<>();
        set_vuelos = new TreeSet<clsVuelo>();
        arrl_vuelosReservados = new ArrayList<>();
        set_vuelosVitoria = new TreeSet<clsVuelo>();
        set_vuelosMadrid = new TreeSet<clsVuelo>();
        set_vuelosMalaga = new TreeSet<clsVuelo>();
        set_vuelosMaldivas = new TreeSet<clsVuelo>();
        set_vuelosBilbao = new TreeSet<clsVuelo>();
        set_vuelosTorrevieja = new TreeSet<clsVuelo>();
        set_vuelosAlbacete = new TreeSet<clsVuelo>();
        arrl_usuarioRegistrado = new ArrayList<clsUsuarioRegistrado>();

        obj_gld = new clsGestorDatos();
        /**
         * Llamada a metodos de inicializacion adicionales.
         */
        vo_recuperarInfoUsuariosBD();
        vo_recuperarInfoVuelosVitoriaBD();
        vo_recuperarInfoVuelosTorrevieja();
        vo_recuperarInfoVuelosMalagaBD();
        vo_recuperarInfoVuelosMadridBD();
        vo_recuperarInfoVuelosBilbaoBD();
        vo_recuperarInfoVuelosAlbaceteBD();
        vo_recuperarInfoVueloMaldivasBD();
        vo_recuperarInfoVuelosBD();
        vo_recuperarInfoAlojamientosBD();
    }

   
    /**
     * Este metodo permite insertar un usuario en el sistema.
     * Controlando que no exista la repeticion del mismo.
     *
     * @param str_nombre   Nombre del usuario.
     * @param str_apellido Apellido del usuario.
     * @param str_dni      DNI del usuario.
     * @param str_correo   Correo del usuario.
     * @param str_clave    Clave del usuario.
     * @return True si el jugador se ha insertado correctamente o false si ya
     *         ha sido anadido previamente.
     * @throws clsUsuarioRepetido Si el usuario esta repetido
     * @throws clsDniFormatoIncorrecto Si el formato del dni esta mal
     */
    public boolean bln_insertarUsuario(String str_nombre, String str_apellido,
            String str_dni, String str_correo, String str_clave) 
            throws clsUsuarioRepetido, clsDniFormatoIncorrecto
    {
        clsUsuarioRegistrado obj_nuevoUsuario;
        obj_nuevoUsuario = new clsUsuarioRegistrado(str_nombre, str_apellido,
                str_dni, str_correo, str_clave,enRol.REGISTRADO);

        if(bln_validarDNI(str_dni) == false)
        {
            throw new clsDniFormatoIncorrecto
            ("El dni tiene un formato incorrecto!");
        }
        else
        {
            if (map_Usuarios.containsKey(str_dni))
            {
                throw new clsUsuarioRepetido
                ("El usuario esta repetido");
            }    
            else 
            {
                map_Usuarios.put(str_dni, obj_nuevoUsuario);
                obj_gld.vo_conectar();
                obj_gld.vo_insertarUsuarioBD(str_nombre, str_apellido, str_dni, 
                                        str_correo, str_clave,enRol.REGISTRADO);
                obj_gld.vo_desconectar();
                return true;
            }
        }    
    }

    /**
     * Metodo para insertar a un administrador
     * @param str_nombre Nombre del admin
     * @param str_apellido Apellido del admin
     * @param str_dni dni del admin
     * @param str_correo Correo del admin
     * @param str_clave Clave del admin
     * @return True si se inserta y false si no
     * @throws clsUsuarioRepetido Si el usuario esta repetido
     * @throws clsDniFormatoIncorrecto Si el formato del dni esta mal
     */
    public boolean bln_insertarAdministrador(String str_nombre, 
    String str_apellido, String str_dni, String str_correo, String str_clave) 
            throws clsUsuarioRepetido, clsDniFormatoIncorrecto
    {
        clsUsuarioRegistradoAdministrador obj_nuevoUsuario;
        obj_nuevoUsuario = new clsUsuarioRegistradoAdministrador(str_nombre, 
        str_apellido, str_dni, str_correo, str_clave,enRol.ADMINISTRADOR);

        if(bln_validarDNI(str_dni) == false)
        {
            throw new clsDniFormatoIncorrecto
            ("El dni tiene un dormato incorrecto!");
        }
        else
        {
            if (map_Usuarios.containsKey(str_dni))
            {
                throw new clsUsuarioRepetido
                ("El usuario esta repetido");
            }    
            else 
            {
                map_Usuarios.put(str_dni, obj_nuevoUsuario);
                obj_gld.vo_conectar();
                obj_gld.vo_insertarUsuarioBD(str_nombre, str_apellido, str_dni, 
                                    str_correo, str_clave, enRol.ADMINISTRADOR);
                obj_gld.vo_desconectar();
                return true;
            }
        }    
    }


    /**
     * Este metodo intenta anadir una nueva reserva de vuelo a la lista de 
     * vuelos reservados.
     * 
     * @param str_destino El destino del vuelo.
     * @param str_origen El destino del vuelo.
     * @param str_horaSalida La hora de salida del vuelo.
     * @param str_horaLlegada La hora de llegada del vuelo.
     * @param str_nombre El nombre del vuelo.
     * @param int_numeroPasajeros El numero de pasajeros del vuelo.
     * @param int_cantidadEquipaje La cantidad de equipaje.
     * @param int_precio El precio del vuelo.
     * @return True si la reserva de vuelo se anade correctamente, false si el 
     * vuelo ya esta reservado.
     */
    public boolean bln_anadirReservaVuelo(String str_origen, String str_destino,
    String str_horaSalida, String str_horaLlegada, String str_nombre, 
    int int_numeroPasajeros,int int_cantidadEquipaje, int int_precio) 
    {
        clsVuelo obj_nuevaReservaVuelo;
        obj_nuevaReservaVuelo = new clsVuelo(str_destino, str_horaSalida,
                str_horaLlegada, str_nombre, int_numeroPasajeros, 
                int_cantidadEquipaje,int_precio);

        if (arrl_vuelosReservados.contains(obj_nuevaReservaVuelo)) 
        {
            return false;
        } else 
        {
            arrl_vuelosReservados.add(obj_nuevaReservaVuelo);
            return true;
        }
    }
    /**
     * Este metodo comprueba si un usuario ha reservado algun alojamiento.
     * 
     * @param str_dni El DNI del usuario cuyas reservas de alojamiento se van a 
     * comprobar.
     * @return True si el usuario ha reservado algun alojamiento y false si no 
     * hay ninguna reserva de alojamiento en base a su DNI.
     */
    public boolean bln_comprobarAlojamientoReservadoUsuario(String str_dni)
    {
        for(clsAlojamiento obj_v : arrl_alojamientosReservados )
        {
            if(obj_v.getNombre().equals(str_dni))
                return true;
        }
        return false;
    }
    
    /**
     * Este metodo comprueba si hay algun alojamiento reservado para un destino 
     * y periodo de tiempo.
     * 
     * @param str_destino El destino del alojamiento.
     * @param dt_fechaLlegada La fecha de llegada al alojamiento.
     * @param dt_fechaSalida La fecha de salida del alojamiento.
     * @return True si no hay ningun alojamiento reservado para el destino y 
     * periodo de tiempo y false si hay algun alojamiento reservado.
     */
    public boolean bln_comprobarAlojamientoReservado(String str_destino,
            LocalDate dt_fechaLlegada, LocalDate dt_fechaSalida) 
    {
        clsAlojamiento obj_nuevoAlojamientoReservado;

        obj_nuevoAlojamientoReservado = new clsAlojamiento(str_destino, 
        dt_fechaLlegada, dt_fechaSalida, str_destino, 0,
        0);

        if (arrl_alojamientosReservados.contains(obj_nuevoAlojamientoReservado)) 
        {
            return false;
        } else 
        {
            return true;
        }
    }

    /*
     * public void vo_añadirReservaAlojamiento(String str_destino,String
     * str_nombre,)
     * {
     * clsAlojamiento obl_añadirReservaAlojamiento;
     * obl_añadirReservaAlojamiento = new clsAlojamiento()
     * }
     */

    /**
     * Este metodo recupera la informacion de los usuarios eliminados en el mapa
     * de usuarios.
     * 
     * @return Una lista que contiene la informacion de todos los usuarios.
     */
    public ArrayList<itfProperty> arrl_recuperarInfoUsuarios() 
    {
        ArrayList<itfProperty> arrl_infoUsuarios;
        arrl_infoUsuarios = new ArrayList<>();

        arrl_infoUsuarios.addAll(map_Usuarios.values());

        return arrl_infoUsuarios;
    }

    /**
     * Este metodo recupera la informacion de los usuarios eliminados en la 
     * base de datos de usuarios.
     * 
     * @return Una lista que contiene la informacion de todos los usuarios.
     */
    public ArrayList<itfProperty> arrl_recuperarInfoUsuariosBD() 
    {
        ArrayList<itfProperty> arrl_infoUsuariosBD;
        arrl_infoUsuariosBD = new ArrayList<>();

        arrl_infoUsuariosBD.addAll(arrl_usuarioRegistrado);

        return arrl_infoUsuariosBD;
    }

    /**
     * Este metodo recupera la informacion de los vuelos eliminados de el
     * arraylist de vuelos.
     * 
     * @return Una arraylist que contiene la informacion de todos los vuelos.
     */

    public ArrayList<itfProperty> arrl_recuperarInfoVuelos() 
    {
        ArrayList<itfProperty> arrl_infoVuelos;
        arrl_infoVuelos = new ArrayList<>();

        // Para recorrer cada vuelo de la lista.
        for (clsVuelo obj_vuelo : set_vuelos) 
        {
            // mete cada vuelo en el nuevo array
            arrl_infoVuelos.add(obj_vuelo);
        }
        return arrl_infoVuelos;
    }

    /**
     * Este metodo recupera la informacion de las reservas eliminadas de el
     * arraylist de reservas.
     * @param str_nombre El nombre del representante
     * @return Una arraylist que contiene la informacion de todas las reservas.
     */
    public ArrayList<itfProperty> arrl_recuperarInfoReservas(String str_nombre) 
    {
        ArrayList<itfProperty> arrl_reservas;
        arrl_reservas = new ArrayList<>();

        // Para recorrer cada jugador de la lista.
        for (clsReserva obj_reserva : lst_reservas) 
        {
            // mete cada usuario en el nuevo array
            arrl_reservas.add(obj_reserva);
        }
        return arrl_reservas;
    }

    /**
     * Este metodo comprueba si las credenciales coinciden con las credenciales
     * de algun usuario registrado.
     * 
     * @param str_correo El correo a comprobar.
     * @param str_clave La clave a comprobar.
     * @return True si las credenciales coinciden con algun usuario registrado y
     * false si no coinciden con ningun usuario.
     */
    public boolean bln_comprobarCredenciales(String str_correo,String str_clave) 
    {
        ArrayList<clsUsuarioRegistrado> arrl_infoUsuarios;
        arrl_infoUsuarios = new ArrayList<>();

        arrl_infoUsuarios.addAll(map_Usuarios.values());

        for (clsUsuarioRegistrado u : arrl_infoUsuarios) 
        {
            if (u.getCorreo().equals(str_correo) && 
                u.getClave().equals(str_clave)) 
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Este metodo borra un usuario del sistema.
     * 
     * @param str_dni Nombre del usuario a borrar.
     * @return True si se ha eliminado correctamente y false si no se encontro
     *         el usuario.
     */
    public boolean bln_borrarUsuario(String str_dni) 
    {
        clsUsuarioRegistrado obj_usuarioABorrar;
        obj_usuarioABorrar = new clsUsuarioRegistrado(null, 
        null, str_dni, null,
                null, null);

        Object obj_r;
        obj_r = map_Usuarios.remove(obj_usuarioABorrar.getDNI());
        obj_gld.vo_conectar();
        obj_gld.vo_borrarUsuarioBD(str_dni);
        obj_gld.vo_desconectar();

        if (obj_r == null)
            return false;
        else
            return true;
    }

    /** 
     * Este metodo manda al gestor los datos de todos los usuarios registrados.
     * 
     * @param str_nombre Nombre del usuario registrado cuyos datos deseas
     *                   buscar.
     * @return Los usuarios registrados.
     */
    private clsUsuarioRegistrado vo_buscarUsuarioRegistrado(String str_dni) 
    {
        for(clsUsuarioRegistrado u : map_Usuarios.values())
        {
            if(u.getDNI().equals(str_dni))
            {
                return u;
            }
        }
        return null;
    }

    /**
     * Visualiza los datos de un usuario registrado en el sistema.
     * 
     * @param str_nombre Nombre del usuario cuyos datos se quieren visualizar.
     * @return Una cadena de texto que contiene los datos del usuario.
     */
    public String vo_visualizarDatosUsuario(String str_nombre) 
    {
        clsUsuarioRegistrado obj_usuarioRegistrado;
        String str_texto;
        str_texto = "";

        obj_usuarioRegistrado = vo_buscarUsuarioRegistrado(str_nombre);

        str_texto = obj_usuarioRegistrado.getNombre() + " - " +
                obj_usuarioRegistrado.getApellidos() + " - " +
                obj_usuarioRegistrado.getDNI() + " - " +
                obj_usuarioRegistrado.getCorreo() + " - " +
                obj_usuarioRegistrado.getClave();

        return str_texto;
    }

    /**
     * Comprueba si un usuario ya esta registrado en el sistema.
     * 
     * @param str_dni Nombre del usuario a comprobar.
     * @return True si el usuario esta registrado y false si no lo esta.
     */
    public boolean bln_comprobarUsuarioExistente(String str_dni) 
    {
        clsUsuarioRegistrado obj_nuevoUsuario;
        obj_nuevoUsuario = new clsUsuarioRegistrado(str_dni);

        if (map_Usuarios.containsKey(obj_nuevoUsuario.getDNI()))
            return true;
        else 
        {
            return false;
        }
    }

    
    /**
     * Este metodo permite añadir una reserva de alojamiento.
     * 
     * @param str_destino El destino del alojamiento.
     * @param dt_fechaLlegada La hora de llegada al alojamiento.
     * @param dt_fechaSalida La hora de salida del alojamiento.
     * @param str_nombre El nombre del alojamiento.
     * @param int_numNoches El numero de noches en el alojamiento.
     * @param int_precio El precio del alojamiento.
     */
    public void vo_añadirReservaAlojamiento(String str_destino,
                                            LocalDate dt_fechaLlegada, 
                                            LocalDate dt_fechaSalida, 
                                            String str_nombre,
                                            int int_numNoches, 
                                            int int_precio) 
    {
        clsAlojamiento obj_reservarAlojamiento;
        obj_reservarAlojamiento = new clsAlojamiento(
                str_destino,
                dt_fechaLlegada,
                dt_fechaSalida, 
                str_nombre, 
                int_numNoches,
                int_precio );

        arrl_alojamientosReservados.add(obj_reservarAlojamiento);
    }


    /**
     * Obtiene una lista de vuelos para Torrevieja.
     * @return Una lista de vuelos.
     */
    public List<itfProperty> getVuelosTorrevieja()
    {
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosTorrevieja);

        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

    /**
     * Obtiene una lista de vuelos para Vitoria.
     * @return Una lista de vuelos.
     */
    public List<itfProperty> getVuelosVitoria()
    {
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosVitoria);

        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

    /**
     * Obtiene una lista de vuelos para Malaga.
     * @return Una lista de vuelos.
     */
    public List<itfProperty> getVuelosMalaga()
    {
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosMalaga);

        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

    /**
     * Obtiene una lista de vuelos para Bilbao.
     * @return Una lista de vuelos.
     */
    public List<itfProperty> getVuelosBilbao()
    {
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosBilbao);

        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

    /**
     * Obtiene una lista de vuelos para Madrid.
     * @return Una lista de vuelos.
     */
    public List<itfProperty> getVuelosMadrid()
    {
        List<clsVuelo> arr_vuelosAux;
        arr_vuelosAux = new ArrayList<>();
        arr_vuelosAux.addAll(set_vuelosMadrid);

        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arr_vuelosAux);

        return arrl_resultado;
    }

    /**
     * Obtiene una lista de vuelos para Maldivas.
     * @return Una lista de vuelos.
     */
    public List<itfProperty> getVuelosMaldivas()
    {
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosMaldivas);

        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

    /**
     * Obtiene una lista de vuelos para Albacete.
     * @return Una lista de vuelos.
     */
    public List<itfProperty> getVuelosAlbacete()
    {
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosAlbacete);

        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }   

    /**
     * Este metodo ordena los vuelos.
     * 
     * @return El resultado de los vuelos ordenados por precio.
     */
    public List<itfProperty> getVuelosOrdenadosPrecioTorre() 
    {
        // PASO 1
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosTorrevieja);

        Collections.sort(arrl_vuelosAux, new clsComparadorVuelosPrecio());

        // PASO 2
        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

    /**
     * Recupera los vuelos de malaga ordenados por preico
     * @return Los vuelos ordenador por precio
     */
    public List<itfProperty> getVuelosOrdenadosPrecioMalaga() 
    {
        // PASO 1
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosMalaga);

        Collections.sort(arrl_vuelosAux, new clsComparadorVuelosPrecio());

        // PASO 2
        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

    /**
     *Recupera los vuelos de Albacete ordenados por preico
     * @return Los vuelos ordenador por precio
     */
    public List<itfProperty> getVuelosOrdenadosPrecioAlbacete() 
    {
        // PASO 1
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosAlbacete);

        Collections.sort(arrl_vuelosAux, new clsComparadorVuelosPrecio());

        // PASO 2
        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

    /**
     * Recupera los vuelos de Vitoria ordenados por preico
     * @return Los vuelos ordenador por precio
     */
    public List<itfProperty> getVuelosOrdenadosPrecioVitoria() 
    {
        // PASO 1
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosVitoria);

        Collections.sort(arrl_vuelosAux, new clsComparadorVuelosPrecio());

        // PASO 2
        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

    /**
     * Recupera los vuelos de Bilbao ordenados por preico
     * @return Los vuelos ordenador por precio
     */
    public List<itfProperty> getVuelosOrdenadosPrecioBilbao() 
    {
        // PASO 1
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosBilbao);

        Collections.sort(arrl_vuelosAux, new clsComparadorVuelosPrecio());

        // PASO 2
        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

    /**
     * Recupera los vuelos de Madrid ordenados por preico
     * @return Los vuelos ordenador por precio
     */
    public List<itfProperty> getVuelosOrdenadosPrecioMadrid() 
    {
        // PASO 1
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosMadrid);

        Collections.sort(arrl_vuelosAux, new clsComparadorVuelosPrecio());

        // PASO 2
        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

    /**
     * Recupera los vuelos de Maldivas ordenados por preico
     * @return Los vuelos ordenador por precio
     */
    public List<itfProperty> getVuelosOrdenadosPrecioMaldivas() 
    {
        // PASO 1
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosMaldivas);

        Collections.sort(arrl_vuelosAux, new clsComparadorVuelosPrecio());

        // PASO 2
        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

    /**
     * Este metodo comprueba los vuelos reservados segun el DNI.
     * 
     * @param str_dni El DNI del usuario.
     * @return True si hay algun vuelo reservado segun el DNI y false si no hay
     * ningun vuelo relacionado al DNI.
     */
    public boolean bln_comprobarVueloReservado(String str_dni) 
    {

        for(clsVuelo obj_v : arrl_vuelosReservados)
        {
            if(obj_v.getNombre().equals(str_dni))
                return true;
        }
        return false;
    }

    /**
     * Este metodo recupera la informacion de la reserva de un vuelo.
     * 
     * @param str_dni El DNI del usuario.
     * @return True si hay algun vuelo para recuperar y false si no hay ninguno.
     */
    public itfProperty recuperarInfoReservaVuelo(String str_dni) 
    {
        for(clsVuelo obj_v : arrl_vuelosReservados)
        {
            if (obj_v.getNombre().equals(str_dni))
            {
                return obj_v;
            }
        }
        return null;
    }

    /**
     * Este metodo obtiene el arraylist de la informacion de reserva de los 
     * alojamientos.
     * 
     * @return El arraylist de la reserva de alojamientos.
     */
    public ArrayList<itfProperty> arrl_recuperarInfoReservaAlojamientos() 
    {
        ArrayList<itfProperty> arrl_resultado;

        arrl_resultado = new ArrayList<>();

        arrl_resultado.addAll(arrl_alojamientosReservados);

        return arrl_resultado;    
    }

    /**
     * Este metodo recupera la informacion de la reserva de alojamientos.
     * @param str_dni El DNI del usuario.
     * @return True si hay alguna informacion de la reserva de alojamientos 
     * asociada al DNI y false si no hay.
     */
    public itfProperty arrl_recuperarInfoReservaAlojamientosUsuarios
    (String str_dni) 
    {
        for(clsAlojamiento obj_v : arrl_alojamientosReservados)
        {
            if (obj_v.getNombre().equals(str_dni))
            {
                return obj_v;
            }
        }
        return null;
    }

    /**
     * Este metodo valida el DNI del usuario.
     * @param str_dni El DNI del usuario.
     * @return
     */
    private boolean bln_validarDNI(String str_dni) 
    {    
        String str_letras; //Letras del DNI.
        String str_numero; //Numeros del DNI.
        char letra; //Letra del DNI.
        int int_numDNI; //Numero del DNI.
        char letraCalculada; //Letra calculada del DNI.

        str_letras = "TRWAGMYFPDXBNJZSQVHLCKE";

        if (str_dni == null || str_dni.length() != 9) 
        {
            return false;
        } else 
        {
            str_numero = str_dni.substring(0, 8);
            letra = str_dni.charAt(8);
            try 
            {
                int_numDNI = Integer.parseInt(str_numero);
                int int_indice = int_numDNI % 23;
                letraCalculada = str_letras.charAt(int_indice);
                return letra == letraCalculada;

            } catch (NumberFormatException e) 
            {
                return false;
            }
        }
    }
    
    /**
     * Este metodo recupera la informacion de los usuarios de la base de datos y
     * la almacena en un mapa.
     * Dependiendo del rol de cada usuario y los almacena en un mapa utilizando 
     * un hash como clave.
     */
    private void vo_recuperarInfoUsuariosBD() 
    {
        obj_gld.vo_conectar();

        ResultSet obj_rs = obj_gld.recuperarUsuariosBD();
        try
        {
            while (obj_rs.next())
                {
                    String str_nombre = obj_rs.getString(1);
                    String str_apellido = obj_rs.getString(2);
                    String str_dni = obj_rs.getString(3);
                    String str_correo = obj_rs.getString(4);
                    String str_clave = obj_rs.getString(5);
                    enRol rol = enRol.valueOf(obj_rs.getString(6));
                    
                    if(rol == enRol.REGISTRADO)
                    {
                        clsUsuarioRegistrado valor = new clsUsuarioRegistrado
                        (str_nombre, str_apellido, str_dni, str_correo, 
                                                            str_clave, rol);

                        
                        map_Usuarios.put(str_dni, valor);
                    }
                    if (rol == enRol.ADMINISTRADOR) 
                    {
                        clsUsuarioRegistradoAdministrador valor = 
                        new clsUsuarioRegistradoAdministrador
                        (str_nombre, str_apellido, str_dni, str_correo, 
                                                            str_clave, rol);

                        
                        map_Usuarios.put(str_dni, valor);
                    }
                }
                obj_gld.vo_desconectar();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Este metodo recupera la informacion de vuelos en la base de datos.
     */
    private void vo_recuperarInfoVuelosBD() 
    {
        obj_gld.vo_conectar();

        ResultSet obj_rs = obj_gld.recuperarVuelosBD();
        try
        {
            while (obj_rs.next())
                {
                    String str_origen = obj_rs.getString(1);
                    String str_destino = obj_rs.getString(2);
                    String str_hora_salida = obj_rs.getString(3);
                    String str_hora_llegada = obj_rs.getString(4);
                    int int_precio = obj_rs.getInt(5);
                    String str_id = obj_rs.getString(6);
                    

                    clsVuelo info = new clsVuelo
                    (str_origen, str_destino, str_hora_salida, str_hora_llegada, 
                    int_precio, str_id);
                    set_vuelos.add(info);    
                }
                obj_gld.vo_desconectar();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Este metodo recupera la informacion de vuelos con origen Torrevieja
     * en la base de datos.
     */
    private void vo_recuperarInfoVuelosTorrevieja() 
    {
        obj_gld.vo_conectar();

        ResultSet obj_rs = obj_gld.recuperarVuelosTorreviejaBD();
        try
        {
            while (obj_rs.next())
                {
                    String str_origen = obj_rs.getString(1);
                    String str_destino = obj_rs.getString(2);
                    String str_hora_salida = obj_rs.getString(3);
                    String str_hora_llegada = obj_rs.getString(4);
                    int int_precio = obj_rs.getInt(5);
                    String str_id = obj_rs.getString(6);
                    

                    clsVuelo info = new clsVuelo
                    (str_origen, str_destino, str_hora_salida, str_hora_llegada, 
                    int_precio, str_id);
                    set_vuelosTorrevieja.add(info);    
                }
                obj_gld.vo_desconectar();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Este metodo recupera la informacion de vuelos con origen Malaga
     * en la base de datos.
     */
    private void vo_recuperarInfoVuelosMalagaBD() 
    {
        obj_gld.vo_conectar();

        ResultSet obj_rs = obj_gld.recuperarVuelosMalagaBD();
        try
        {
            while (obj_rs.next())
                {
                    String str_origen = obj_rs.getString(1);
                    String str_destino = obj_rs.getString(2);
                    String str_hora_salida = obj_rs.getString(3);
                    String str_hora_llegada = obj_rs.getString(4);
                    int int_precio = obj_rs.getInt(5);
                    String str_id = obj_rs.getString(6);
                    

                    clsVuelo info = new clsVuelo
                    (str_origen, str_destino, str_hora_salida, str_hora_llegada, 
                    int_precio, str_id);
                    set_vuelosMalaga.add(info);    
                }
                obj_gld.vo_desconectar();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Este metodo recupera la informacion de vuelos con origen Albacete
     * en la base de datos.
     */
    private void vo_recuperarInfoVuelosAlbaceteBD() 
    {
        obj_gld.vo_conectar();
        ResultSet obj_rs = obj_gld.recuperarVuelosAlbaceteBD();
        try
        {
            while (obj_rs.next())
                {
                    String str_origen = obj_rs.getString(1);
                    String str_destino = obj_rs.getString(2);
                    String str_hora_salida = obj_rs.getString(3);
                    String str_hora_llegada = obj_rs.getString(4);
                    int int_precio = obj_rs.getInt(5);
                    String str_id = obj_rs.getString(6);
                    

                    clsVuelo info = new clsVuelo
                    (str_origen, str_destino, str_hora_salida, str_hora_llegada, 
                    int_precio, str_id);
                    set_vuelosAlbacete.add(info);    
                }
                obj_gld.vo_desconectar();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Este metodo recupera la informacion de vuelos con origen Vitoria
     * en la base de datos.
     */
    private void vo_recuperarInfoVuelosVitoriaBD() 
    {
        obj_gld.vo_conectar();
        ResultSet obj_rs = obj_gld.recuperarVuelosVitoriaBD();
        try
        {
            while (obj_rs.next())
                {
                    String str_origen = obj_rs.getString(1);
                    String str_destino = obj_rs.getString(2);
                    String str_hora_salida = obj_rs.getString(3);
                    String str_hora_llegada = obj_rs.getString(4);
                    int int_precio = obj_rs.getInt(5);
                    String str_id = obj_rs.getString(6);
                    

                    clsVuelo info = new clsVuelo
                    (str_origen, str_destino, str_hora_salida, str_hora_llegada, 
                    int_precio, str_id);
                    set_vuelosVitoria.add(info);    
                }
                obj_gld.vo_desconectar();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Este metodo recupera la informacion de vuelos con origen Bilbao
     * en la base de datos.
     */
    private void vo_recuperarInfoVuelosBilbaoBD() 
    {
        obj_gld.vo_conectar();
        ResultSet obj_rs = obj_gld.recuperarVuelosBilbaoBD();
        try
        {
            while (obj_rs.next())
                {
                    String str_origen = obj_rs.getString(1);
                    String str_destino = obj_rs.getString(2);
                    String str_hora_salida = obj_rs.getString(3);
                    String str_hora_llegada = obj_rs.getString(4);
                    int int_precio = obj_rs.getInt(5);
                    String str_id = obj_rs.getString(6);
                    

                    clsVuelo info = new clsVuelo
                    (str_origen, str_destino, str_hora_salida, str_hora_llegada, 
                    int_precio, str_id);
                    set_vuelosBilbao.add(info);    
                }
                obj_gld.vo_desconectar();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Este metodo recupera la informacion de vuelos con origen Maldivas
     * en la base de datos.
     */
    private void vo_recuperarInfoVueloMaldivasBD() 
    {
        obj_gld.vo_conectar();
        ResultSet obj_rs = obj_gld.recuperarVuelosMaldivasBD();
        try
        {
            while (obj_rs.next())
                {
                    String str_origen = obj_rs.getString(1);
                    String str_destino = obj_rs.getString(2);
                    String str_hora_salida = obj_rs.getString(3);
                    String str_hora_llegada = obj_rs.getString(4);
                    int int_precio = obj_rs.getInt(5);
                    String str_id = obj_rs.getString(6);
                    

                    clsVuelo info = new clsVuelo
                    (str_origen, str_destino, str_hora_salida, str_hora_llegada, 
                    int_precio, str_id);
                    set_vuelosMaldivas.add(info);    
                }
                obj_gld.vo_desconectar();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Este metodo recupera la informacion de vuelos con origen Madrid
     * en la base de datos.
     */
    private void vo_recuperarInfoVuelosMadridBD() 
    {
        obj_gld.vo_conectar();
        ResultSet obj_rs = obj_gld.recuperarVuelosMadridBD();
        try
        {
            while (obj_rs.next())
                {
                    String str_origen = obj_rs.getString(1);
                    String str_destino = obj_rs.getString(2);
                    String str_hora_salida = obj_rs.getString(3);
                    String str_hora_llegada = obj_rs.getString(4);
                    int int_precio = obj_rs.getInt(5);
                    String str_id = obj_rs.getString(6);
                    

                    clsVuelo info = new clsVuelo
                    (str_origen, str_destino, str_hora_salida, str_hora_llegada, 
                    int_precio, str_id);
                    set_vuelosMaldivas.add(info);    
                }
                obj_gld.vo_desconectar();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Este metodo recupera la informacion de alojamientos de la base de datos.
     */
    private void vo_recuperarInfoAlojamientosBD() 
    {
        obj_gld.vo_conectar();
        ResultSet obj_rs = obj_gld.recuperarInfoAlojamientosBD();
        try
        {
            while (obj_rs.next())
                {   
                    
                    String str_destino = obj_rs.getString(4);
                    int int_numHabitaciones = obj_rs.getInt(5);
                    int int_numPersonas = obj_rs.getInt(6);
                    String str_propietatio = obj_rs.getString(7);
                    int int_precio = obj_rs.getInt(8);
                
                    
                    clsAlojamiento info = new clsAlojamiento(
                    str_destino, 
                    int_numHabitaciones,
                    int_numPersonas,
                    str_propietatio,  
                    int_precio);
                    arrl_alojamientos.add(info);
                 
                }
                obj_gld.vo_desconectar();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    //-----------------------ALOJAMIENTOS-----------------------//

    /**
     * Este metodo recupera los alojamientos ordenados por precio.
     * @return Los alojamientos ordenados por precio.
     */
    public List<itfProperty> getAlojamientosOrdenadosPrecio() 
    {
        // PASO 1
        List<clsAlojamiento> arrl_alojamientosAux;
        arrl_alojamientosAux = new ArrayList<>();
        arrl_alojamientosAux.addAll(arrl_alojamientos);

        Collections.sort(arrl_alojamientosAux, 
        new clsComparadorAlojamientosPrecio());

        // PASO 2
        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_alojamientosAux);

        return arrl_resultado;
    }
    
    /**
     * Este metodo recupera la informacion de los alojamientos eliminados de el
     * arraylist de alojamientos.
     * 
     * @return Una lista que contiene la informacion de todos los alojamientos.
     */
    public ArrayList<itfProperty> getInfoAlojamientosOrdenadosDestino() 
    {
        List<clsAlojamiento> arrl_alojamientosAux;
        arrl_alojamientosAux = new ArrayList<>();
        arrl_alojamientosAux.addAll(arrl_alojamientos);

        Collections.sort(arrl_alojamientosAux, 
        new clsComparadorAlojamientosDestino());

        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_alojamientosAux);

        return arrl_resultado;
    }

    //--------------------------VUELOS--------------------------------//
    
    /**
     * Este metodo recupera los vuelos ordenados por precio.
     * @return Los vuelos ordenados por precio.
     */
    public List<itfProperty> getVuelosOrdenadosPrecio() 
    {
        // PASO 1
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosVitoria);
        arrl_vuelosAux.addAll(set_vuelosAlbacete);
        arrl_vuelosAux.addAll(set_vuelosBilbao);
        arrl_vuelosAux.addAll(set_vuelosMadrid);
        arrl_vuelosAux.addAll(set_vuelosMalaga);
        arrl_vuelosAux.addAll(set_vuelosTorrevieja);
        arrl_vuelosAux.addAll(set_vuelosMaldivas);

        Collections.sort(arrl_vuelosAux, new clsComparadorVuelosPrecio());

        // PASO 2
        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

     /**
     * Este metodo obtiene los posibles destinos los cuales estan ordenados por 
     * ID.
     * 
     * @return Los vuelos ordenados por id.
     */
    public ArrayList<itfProperty> getVuelosOrdenadosId()
    {
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosVitoria);
        arrl_vuelosAux.addAll(set_vuelosAlbacete);
        arrl_vuelosAux.addAll(set_vuelosBilbao);
        arrl_vuelosAux.addAll(set_vuelosMadrid);
        arrl_vuelosAux.addAll(set_vuelosMalaga);
        arrl_vuelosAux.addAll(set_vuelosTorrevieja);
        arrl_vuelosAux.addAll(set_vuelosMaldivas);

        Collections.sort(arrl_vuelosAux, new clsComparadorVuelosID());

        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

    /**
     * Devuelve el nombre del usuario correspondiente al DNI.
     * 
     * @param str_dni El DNI del usuario.
     * @return El nombre del usuario o null si no se encuentra.
     */
    public String getNombreUsuario(String str_dni) 
    {
        for (clsUsuarioRegistrado e : map_Usuarios.values())
            if (e.getDNI().equals(str_dni))
                return e.getNombre();

        return null;
    }

    /**
     * Devuelve el apellido del usuario correspondiente al DNI.
     * 
     * @param str_dni El DNI del usuario.
     * @return El apellido del usuario o null si no se encuentra.
     */
    public String getApellidoUsuario(String str_dni) 
    {
        for (clsUsuarioRegistrado e : map_Usuarios.values())
            if (e.getDNI().equals(str_dni))
                return e.getApellidos();

        return null;
    }

    /**
     * Devuelve el correo del usuario correspondiente al DNI.
     * 
     * @param str_dni El DNI del usuario.
     * @return El correo del usuario o null si no se encuentra.
     */
    public String getCorreoUsuario(String str_dni) 
    {
        for (clsUsuarioRegistrado e : map_Usuarios.values())
            if (e.getDNI().equals(str_dni))
                return e.getCorreo();

        return null;
    }

    /**
     * Devuelve la clave del usuario correspondiente al DNI.
     * 
     * @param str_dni El DNI del usuario.
     * @return La clave del usuario o null si no se encuentra.
     */
    public String getClaveUsuario(String str_dni) 
    {
        for (clsUsuarioRegistrado e : map_Usuarios.values())
            if (e.getDNI().equals(str_dni))
                return e.getClave();

        return null;
    }

    /**
     * Devuelve una lista con los DNIs de todos los usuarios registrados.
     * 
     * @return Una lista de DNIs.
     */
    public ArrayList<String> getDniUsuarios() 
    {
        ArrayList<String> arrl_dni = new ArrayList<>();

        for (clsUsuarioRegistrado obj_u : map_Usuarios.values()) 
        {
            arrl_dni.add(obj_u.getDNI());
        }
        return arrl_dni;
    }

    /**
     * Modifica la informacion de un estudiante.
     * 
     * @param str_dni El DNI del estudiante.
     * @param str_nombre El nuevo nombre del estudiante.
     * @param str_apellido El nuevo apellido del estudiante.
     * @param str_correo El nuevo correo del estudiante.
     * @param str_clave La nueva clave del estudiante.
     */
    public void modificarEstudiante(String str_dni, String str_nombre,
    String str_apellido, String str_correo, String str_clave) 
    {
        for(clsUsuarioRegistrado e : map_Usuarios.values())
        {
            if(e.getDNI().equals(str_dni))
            {
                e.setNombre(str_nombre);
                e.setApellidos( str_apellido );
                e.setCorreo( str_correo );
                e.setClave( str_clave );
            }
        }

        // modificar en la BD
        obj_gld.vo_modificarUsuarioBD(str_nombre, str_apellido, str_dni, 
        str_correo, str_clave);
    }

    /**
     * 
     * METODO
     * 
     * @param str_nombre  El nombre del nuevo usuario
     * @param str_apellido  El apellido del nuevo usuario
     * @param str_dni  El dni del nuevo usuario
     * @param str_correo  El correo del nuevo usuario
     * @param str_clave  La clave del nuevo usuario
     * @param str_rol El rol del nuevo usuario
     * @throws clsUsuarioRepetido Si el usuario ya esta en el sistema saltara
     *                            la excepción
     * @throws clsDniFormatoIncorrecto Si el DNI esta en un formato incorrecto 
     *                                 saltara la excepción
     */
    public void vo_añadirUsuario(String str_nombre, String str_apellido, 
    String str_dni, String str_correo, String str_clave, String str_rol) 
    throws clsUsuarioRepetido, clsDniFormatoIncorrecto
        {
            if(bln_validarDNI(str_dni) == false)
            {
                throw new clsDniFormatoIncorrecto
                          ("El dni tiene un formato incorrecto!!!");
            }
            else
            {
                if (str_rol == "REGISTRADO") 
                {
                    clsUsuarioRegistrado e = new clsUsuarioRegistrado
                    (str_nombre, str_apellido, str_dni, str_correo, str_clave, 
                                                              enRol.REGISTRADO);
                    
                    if(map_Usuarios.containsKey(str_dni))
                    {
                        throw new clsUsuarioRepetido
                        ("El dni tiene un formato incorrecto!!!");
                    }
                    else
                    {
                        map_Usuarios.put(str_dni, e);
                        obj_gld.vo_conectar();
                        obj_gld.vo_insertarUsuarioBD(str_nombre, str_apellido, 
                            str_dni, str_correo, str_clave,enRol.REGISTRADO);
                        obj_gld.vo_desconectar();
                    }
                
                }
                if (str_rol == "ADMINISTRADOR") 
                {
                    clsUsuarioRegistradoAdministrador e =
                    new clsUsuarioRegistradoAdministrador (str_nombre, 
                    str_apellido, str_dni, str_correo, str_clave, 
                                                           enRol.ADMINISTRADOR);

                    if(map_Usuarios.containsKey(str_dni))
                    {
                        throw new clsUsuarioRepetido
                        ("El dni tiene un formato incorrecto!!!");
                    }
                    else
                    {
                        map_Usuarios.put(str_dni, e);
                        obj_gld.vo_conectar();
                        obj_gld.vo_insertarUsuarioBD(str_nombre, str_apellido, 
                            str_dni, str_correo, str_clave,enRol.ADMINISTRADOR);
                        obj_gld.vo_desconectar();
                    }
                }
            }
        }


    /**
     * Este metodo obtiene el rol del usuario que tenga ese
     * @param str_correo El correo del usuario  
     * @param str_clave La clave del usuario
     * @return El rol del usuario
     */
    public enRol obtenerRol(String str_correo, String str_clave) 
    {
        for (clsUsuarioRegistrado e : map_Usuarios.values()) 
        {
            if (e.getCorreo().equals(str_correo) && 
                e.getClave().equals(str_clave)) 
            {
                return e.getRol();
            }
        }
        return null;
    }

    /**
     * Devuelve una lista con los roles disponibles.
     * 
     * @return Una lista de roles.
     */
    public ArrayList<String> getRoles() 
    {
        String str_rol1 = "ADMINISTRADOR";
        String str_rol2 = "REGISTRADO";
        
        ArrayList<String> roles = new ArrayList<>();

        roles.add(str_rol1);
        roles.add(str_rol2);

        return roles;
    }


    /**
     * Este metodo borra un usuario mediante su dni
     * @param str_dni El dni del usuario a borrar
     */
    public void vo_borrarUsuario(String str_dni) 
    {
        map_Usuarios.remove(str_dni);  
        
        obj_gld.vo_conectar();
        obj_gld.vo_borrarUsuarioBD(str_dni);
        obj_gld.vo_desconectar();
    }

    /**
     * Devuelve una lista de usuarios ordenados por DNI.
     * 
     * @return Una lista de usuarios ordenados por DNI.
     */
    public List<itfProperty> getUsuariosOrdenadosDni() 
    {
        List<clsUsuarioRegistrado> arrl_usuariosAux;
        arrl_usuariosAux = new ArrayList<>();
        arrl_usuariosAux.addAll(map_Usuarios.values());

        Collections.sort(arrl_usuariosAux, new clsComparadorUsuariosDni());

        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_usuariosAux);

        return arrl_resultado;
    }

    /**
     * Devuelve una lista de usuarios ordenados por nombre.
     * 
     * @return Una lista de usuarios ordenados por nombre.
     */
    public List<itfProperty> getUsuariosOrdenadosNombre() 
    {
        List<clsUsuarioRegistrado> arrl_usuariosAux;
        arrl_usuariosAux = new ArrayList<>();
        arrl_usuariosAux.addAll(map_Usuarios.values());

        Collections.sort(arrl_usuariosAux, new clsComparadorUsuariosNombre());

        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_usuariosAux);

        return arrl_resultado;
    }

    /**
     * Devuelve una lista de todos los usuarios.
     * 
     * @return Una lista de todos los usuarios.
     */
    public ArrayList<itfProperty> getUsuarios() 
    {
        List<clsUsuarioRegistrado> arrl_usuariosAux;
        arrl_usuariosAux = new ArrayList<>();
        arrl_usuariosAux.addAll(map_Usuarios.values());

        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_usuariosAux);

        return arrl_resultado;
    }


    /**
     * Este metodo obtiene todos los vuelos del sistema
     * @return Todos los vuelos
     */
    public ArrayList<itfProperty> getVuelos() 
    {
        List<clsVuelo> arrl_vuelosAux;
        arrl_vuelosAux = new ArrayList<>();
        arrl_vuelosAux.addAll(set_vuelosVitoria);
        arrl_vuelosAux.addAll(set_vuelosAlbacete);
        arrl_vuelosAux.addAll(set_vuelosBilbao);
        arrl_vuelosAux.addAll(set_vuelosMadrid);
        arrl_vuelosAux.addAll(set_vuelosMalaga);
        arrl_vuelosAux.addAll(set_vuelosTorrevieja);
        arrl_vuelosAux.addAll(set_vuelosMaldivas);

        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_vuelosAux);

        return arrl_resultado;
    }

    /**
     * Devuelve una lista de todos los alojamientos.
     * 
     * @return Una lista de todos los alojamientos.
     */
    public ArrayList<itfProperty> getAlojamientos() 
    {
        List<clsAlojamiento> arrl_alojamientoAux;
        arrl_alojamientoAux = new ArrayList<>();
        arrl_alojamientoAux.addAll(arrl_alojamientos);

        ArrayList<itfProperty> arrl_resultado;
        arrl_resultado = new ArrayList<>();
        arrl_resultado.addAll(arrl_alojamientoAux);

        return arrl_resultado;
    }
}