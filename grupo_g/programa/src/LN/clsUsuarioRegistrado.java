package LN;

import COMUN.clsConstantes;

/**
 * Esta clase clase represeta al usuario registrado en el sistema y extiende la 
 * clase clsUsuario.
 * @see clsUsuario
 */

public class clsUsuarioRegistrado extends clsUsuario 
{
    /**
     * Estos son los atributos de la clase clsUsuarioRegistrado.
     */
    private String str_nombre; /**El nombre del usuario. */
    private String str_apellidos; /**Los apellidos del usuario. */
    private String str_dni; /**El DNI del usuario. */
    private String str_correo; /**El correo del usuario. */
    private String str_clave; /**La clave del usuario. */
    private enRol rol; /* El rol del usuario */ 

    /** 
     * Este es el constructor que se utiliza cuando un usuario se registra.
     * 
     * @param str_paramNombre   El nombre del usuario.
     * @param str_paramApellido El apellido del usuario.
     * @param str_paramDni      El DNI del usuario.
     * @param str_paramCorreo   El correo del usuario.
     * @param str_paramClave    El clave del usuario.
     * @param int_paramRol      El rol del usuario
     */

    public clsUsuarioRegistrado(String str_paramNombre, 
                                String str_paramApellido, 
                                String str_paramDni, 
                                String str_paramCorreo, 
                                String str_paramClave,
                                enRol int_paramRol) 
    {
        str_nombre = str_paramNombre;
        str_apellidos = str_paramApellido;
        str_dni = str_paramDni;
        str_correo = str_paramCorreo;
        str_clave = str_paramClave;
        rol = int_paramRol;
    }

    /**
     * Este es el constructor que se usa cuando un usuario inicia sesion.
     * 
     * @param str_paramCorreo El correo del usuario
     * @param str_paramClave La calve del usuario
     * @param int_paramRol El rol del usuairo
     */
    public clsUsuarioRegistrado(String str_paramCorreo, String str_paramClave,
                                                             enRol int_paramRol) 
    {
        str_correo = str_paramCorreo;
        str_clave = str_paramClave;
        rol = int_paramRol;
    }

    /**
     * Constructorque inicializa un usuario registrado con el correo y el rol.
     * 
     * @param str_paramCorreo El correo del usuario.
     * @param int_paramRol El rol del usuario.
     */
    public clsUsuarioRegistrado(String str_paramCorreo, enRol int_paramRol)
    {
       str_correo = str_paramCorreo;
       rol = int_paramRol;
    }

    /**
     * Constructor que inicializa un usuario con el DNI.
     * 
     * @param str_paramDni El DNI del usuario.
     */
    public clsUsuarioRegistrado(String str_paramDni)
    {
       str_dni =  str_paramDni;
    }

    /**
     * Este metodo obtiene el rol del usuario.
     * 
     * @return El rol del usuario. 
     */
    public enRol getRol() 
    {
        return rol;
    }

    /**
     * Este metodo establece el rol del usuario.
     * @param int_rol El rol del usuario.
     */
    public void setRol(enRol int_rol) 
    {
        this.rol = int_rol;
    }

    /**
     * Este metodo obtiene el nombre del usuario.
     * 
     * @return El nombre del usuario.
     */
    public String getNombre() 
    {
        return str_nombre;
    }

    /**    
     * Este metodo establece el nombre del usuario.
     * 
     * @param str_paramNombre El nombre del usuario.
     */
    public void setNombre(String str_paramNombre) 
    {
        this.str_dni = str_paramNombre;
    }

    /**     
     * Este metodo obtiene el apellido del usuario.
     * 
     * @return El apellido del usuario.
     */
    public String getApellidos() 
    {
        return str_apellidos;
    }

    /**    
     * Este metodo establece el apellido del usuario.
     * 
     * @param str_paramApellido El apellido del usuario.
     */
    public void setApellidos(String str_paramApellido) 
    {
        this.str_apellidos = str_paramApellido;
    }

    /**     
     * Este metodo obtiene el DNI del usuario.
     * 
     * @return El DNI del usuario.
     */
    public String getDNI() 
    {
        return str_dni;
    }

    /**     
     * Este metodo establece el DNI del usuario.
     * 
     * @param str_paramDni El DNI del usuario.
     */
    public void setDNI(String str_paramDni) 
    {
        this.str_dni = str_paramDni;
    }

    /**     
     * Este metodo obtiene el correo del usuario.
     * 
     * @return El correo del usuario.
     */
    public String getCorreo() 
    {
        return str_correo;
    }

    /**     
     * Este metodo establece el correo del usuario.
     * 
     * @param str_paramCorreo El correo del usuario.
     */
    public void setCorreo(String str_paramCorreo) 
    {
        str_correo = str_paramCorreo;
    }

    /**    
     * Este metodo obtiene la clave del usuario.
     * 
     * @return La clave del usuario.
     */
    public String getClave() 
    {
        return str_clave;
    }
    
    /**     
     * Este metodo establece la clave del usuario.
     * 
     * @param str_paramClave La clave del usuario.
     */
    public void setClave(String str_paramClave) 
    {
        this.str_clave = str_paramClave;
    }

    /**
    * Compara este objeto con otro objeto dado para la igualdad.
    * Dos objetos de tipo clsUsuarioRegistrado se consideran iguales si tienen
    * el mismo DNI.
    *
    * @param obj_o El objeto a comparar.
    * @return True si los objetos son iguales y false en el caso que no.
    */
    

     /**
     * Este metodo sirva para cuando el usuario nos pida que le mostremos un 
     * valor, solo se lo enseñemos si nosotros lo permitimos y si nos pide algo
     * que no hemos permitido no se lo dara.
     */
    @Override
    public Object getObjectProperty(String str_propiedad)
    {
        switch (str_propiedad) 
        {    
            case clsConstantes.NOMBRE:
                return str_nombre;
            
            case clsConstantes.APELLIDOS:
                return str_apellidos;

            case clsConstantes.DNI:
                return str_dni;

            case clsConstantes.CORREO:
                return str_correo;

            case clsConstantes.CLAVE:
                return str_clave;

            case clsConstantes.ROL:
                return rol;

            default:
                return super.getObjectProperty (str_propiedad);
        }
    }    

    /**
     * Devuelve un valor hash para el objeto actual basado en su DNI.
     * 
     * @return El valor hash del objeto actual.
     */
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((str_dni == null) ? 0 : str_dni.hashCode());
        return result;
    }

    /**
     * Compara el objeto actual con otro objeto para verificar si son iguales.
     * 
     * @param obj El objeto con el que se va a comparar.
     * @return True si el objeto actual es igual al otro objeto o false en caso 
     * contrario.
     */
    @Override
    public boolean equals(Object obj) 
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        clsUsuarioRegistrado other = (clsUsuarioRegistrado) obj;
        if (str_dni == null) 
        {
            if (other.str_dni != null)
                return false;
        } else if (!str_dni.equals(other.str_dni))
            return false;
        return true;
    }

    /**
     * Devuelve una representacion en cadena de caracteres del objeto.
     * 
     * @return Una cadena que representa el objeto, incluyendo su nombre, 
     * apellido, DNI, correo y clave.
     */
    @Override
    public String toString() 
    {
        return "NOMBRE: [" + str_nombre + "],    APELLIDO: [" + str_apellidos + 
        "],    DNI: [" + str_dni + "],    CORREO: [" + str_correo + 
        "],    CLAVE: [" + str_clave + "],    ROL: [" + rol + "]";
    }
}