package LN;

import java.time.LocalDate;

import COMUN.clsConstantes;

/**    
* Esta clase representa un alojamiento y extiende la clase clsReserva.
* 
* @see clsReserva
*/
public class clsAlojamiento extends clsReserva implements 
Comparable <clsAlojamiento>
{
     /** Destino donde se encuentra el alojamiento. */
    private String str_destino;

    /** El numero de habitaciones del alojamiento. */
    private int int_numeroHabitaciones;

    /** La cantidad de huespedes posibles. */
    private int int_cantidad;

    /** El propietario del alojamiento. */
    private String str_propietario;

    /** El El nombre del representante del alojamiento. */
    private String str_nombreHuesped;

    /** El precio del alojamiento. */
    private int int_precio;

    /** La fecha de llegada al alojamiento. */
    private LocalDate dt_fechaLlegada;

    /** La fecha de llegada al alojamiento. */
    private LocalDate dt_fechaSalida;

    /** El numero de noches que te vas a alojar en el alojamiento. */
    private int int_numNoches;

 /**
  * Constructor para crear un objeto clsAlojamiento con varios parametros.
  * 
  * @param dt_fechaParamInicio La fecha de inicio de la reserva.
  * @param dt_fechaParamFinal La fecha de fin de la reserva.
  * @param str_paramNombre El nombre de la reserva.
  * @param str_paramDestino El destino del alojamiento.
  * @param int_paramNumeroHabitaciones El numero de habitaciones del 
  * alojamiento.
  * @param int_paramCantidad La cantidad de huespedes.
  * @param str_paramPropietario El propietario del alojamiento.
  * @param int_paramPrecio El precio del alojamiento.
  * @param dt_paramFechaLlegada La fecha de llegada al alojamiento.
  * @param dt_paramFechaSalida La fecha de salida del alojamiento.
  * @param int_paramNumNoches El numero de noches de estancia en el alojamiento.
  */
 public clsAlojamiento ( LocalDate dt_fechaParamInicio , 
                        LocalDate dt_fechaParamFinal , 
                        String str_paramNombre,
                        String str_paramDestino,
                        int int_paramNumeroHabitaciones,
                        int int_paramCantidad,
                        String str_paramPropietario,
                        int int_paramPrecio,
                        LocalDate dt_paramFechaLlegada,
                        LocalDate dt_paramFechaSalida,
                        int int_paramNumNoches)
 {
    super(str_paramNombre);
    
    dt_fechaParamInicio = dt_paramFechaLlegada;
    str_destino = str_paramDestino;
    int_precio = int_paramPrecio;
    str_nombreHuesped = str_paramNombre;
    int_numNoches = int_paramNumNoches;
    int_cantidad = int_paramCantidad;
    int_numeroHabitaciones = int_paramNumeroHabitaciones;
    str_propietario = str_paramPropietario;
    dt_fechaLlegada = dt_paramFechaLlegada;
    dt_fechaSalida = dt_paramFechaSalida;
 }

 /**
  * Constructor para crear un objeto clsAlojamiento con varios parametros.
  * 
  * @param str_paramDestino El destino del alojamiento.
  * @param dt_paramFechaLlegada La fecha de llegada al alojamiento.
  * @param dt_paramFechaSalida La fecha de salida del alojamiento.
  * @param str_paramNombreHuesped El nombre del huesped.
  * @param int_paramNumNoches El numero de noches de estancia en el alojamiento.
  * @param int_paramPrecio El precio del alojamiento.
  */
 public clsAlojamiento(String str_paramDestino, 
                      LocalDate dt_paramFechaLlegada, 
                      LocalDate dt_paramFechaSalida, 
                      String str_paramNombreHuesped,
                      int int_paramNumNoches, 
                      int int_paramPrecio) 
 {
    super(str_paramDestino, dt_paramFechaLlegada, dt_paramFechaSalida, 
          str_paramNombreHuesped);

    str_destino = str_paramDestino;
    dt_fechaLlegada = dt_paramFechaLlegada;
    dt_fechaSalida = dt_paramFechaSalida;
    str_nombreHuesped = str_paramNombreHuesped;
    int_numNoches = int_paramNumNoches;
    int_precio = int_paramPrecio;
 }

 /**
  * Constructor para crear un objeto clsAlojamiento con varios parametros.
  * 
  * @param str_paramDestino El destino del alojamiento.
  * @param int_paramNumeroHabitaciones El numero de habitaciones del 
  * alojamiento.
  * @param int_paramCantidad La cantidad de huespedes.
  * @param str_paramPropietario El propietario del alojamiento.
  * @param int_paramPrecio El precio del alojamiento.
  */
 public clsAlojamiento (String str_paramDestino, 
                       int int_paramNumeroHabitaciones, 
                       int int_paramCantidad, 
                       String str_paramPropietario, 
                       int int_paramPrecio)
 {
    super(str_paramPropietario);

    str_destino = str_paramDestino;
    int_numeroHabitaciones = int_paramNumeroHabitaciones;
    int_cantidad = int_paramCantidad;
    str_propietario = str_paramPropietario;
    int_precio = int_paramPrecio;
 }

 /**  
  * Esta clase obtiene el nombre del huesped representante.
  * 
  * @return El nombre del huesped.
  */
 public String getNombreHuesped() 
 {
    return str_nombreHuesped;
 }

 /**   
  * Establece el nombre del huesped representante.
  * 
  * @param str_paramNombreHuesped El nuevo nombre del huesped representante.
  */ 
 public void setNombreHuesped(String str_paramNombreHuesped) 
 {
    this.str_nombreHuesped = str_paramNombreHuesped;
 }

 /**   
  * Esta clase obtiene el destino. 
  * 
  * @return El destino.
  */
 public String getDestino() 
 {
    return str_destino;
 }

 /**    
  * Establece la ubicacion del alojamiento.
  * 
  * @param str_paramDestino El nuevo destino.
  */
 public void setCalle(String str_paramDestino) 
 {
    str_destino = str_paramDestino;
 }

 /**      
  * Esta clase obtiene el numero de habitaciones del alojamiento.
  * 
  * @return El numero de habitaciones.
  */
 public int getNumeroHabitaciones() 
 {
    return int_numeroHabitaciones;
 }

 /**     
  * Esta clase establece el numero de habitaciones del alojamiento.
  * 
  * @param int_numeroHabitaciones El nuevo numero de habitaciones 
  * del alojamiento.
  */
 public void setNumeroHabitacion(int int_numeroHabitaciones) 
 {
    this.int_numeroHabitaciones = int_numeroHabitaciones;
 }

 /**     
  * Esta clase obtiene la cantidad de alojamientos disponibles.
  * 
  * @return La cantidad de alojamientos.
  */
 public int getCantidad() 
 {
    return int_cantidad;
 }

 /**     
  * Establece la cantidad de huespedes posibles.
  * 
  * @param int_cantidad La nueva cantidad de huespedes posibles.
  */
 public void setCantidad(int int_cantidad) 
 {
    this.int_cantidad = int_cantidad;
 }

 /**     
  * Obtiene el propietario del alojamiento.
  * 
  * @return El propietario del alojamiento.
  */
 public String getPropietario() 
 {
    return str_propietario;
 }

 /**     
  * Establece el propietario del alojamiento.
  * 
  * @param str_propietario El nuevo propietario del alojamiento.
  */
 public void setPropietario(String str_propietario) 
 {
    this.str_propietario = str_propietario;
 }

/**
 * Metodo que devuelve el precio del alojamiento
 * @return El precio del alojamiento
 */
public int getPrecio() {
    return int_precio;
 }

/**
 * Metodo que da valor al precio de un alojamiento.
 * @param int_paramPrecio el precio del alojamiento
 */
public void setPrecio(int int_paramPrecio) {
    this.int_precio = int_paramPrecio;
}

 //---------------------------------------------------------------------------//


 /**
  * Este metodo sirve para cuando el usuario nos pida que le mostremos un 
  * valor, solo se lo ensenamos si nosotros lo permitimos y si nos pide algo
  * que no hemos permitido no se lo dara.
  * 
  * @param str_propiedad La propiedad solicitada.
  */
 @Override
 public Object getObjectProperty(String str_propiedad)
 {
    switch (str_propiedad) 
    {
        case clsConstantes.DESTINO_ALOJAMIENTO:
            return str_destino;
        
        case clsConstantes.CANTIDAD:
                return int_cantidad;
            
        case clsConstantes.NUMERO_HABITACIONES:
            return int_numeroHabitaciones;

        case clsConstantes.PROPIETARIO:
            return str_propietario;
        
        case clsConstantes.PRECIO_ALOJAMIENTO:
            return int_precio;

        case clsConstantes.FECHA_LLEGADA:
            return dt_fechaLlegada;

        case clsConstantes.FECHA_SALIDA:
            return dt_fechaSalida;

        case clsConstantes.NUMERO_NOCHES:
            return int_numNoches;            
        
         default:
            return super.getObjectProperty (str_propiedad);
    }
 }

 /**
  * Compara clsAlojamiento con el objeto para el orden.
  * 
  * @param obj_o El objeto clsAlojamiento que se va a comparar.
  * @return Un valor negativo, cero o positivo segun si este objeto es menor que,
  *         igual a, o mayor que el objeto.
  */
 @Override
 public int compareTo(clsAlojamiento obj_o) 
 {
    return str_destino.compareTo(obj_o.str_destino);
 }

 /**
  * Indica si algun otro objeto es igual.
  * 
  * @param obj_o El objeto de referencia con el que comparar.
  * @return True si este objeto es el mismo que el obj y 
  *         false en caso contrario.
  */
 @Override
    public boolean equals(Object obj_o) 
    {
        if (this == obj_o)
            return true;
        if (obj_o == null)
            return false;
        if (getClass() != obj_o.getClass())
            return false;
         clsAlojamiento other = (clsAlojamiento) obj_o;
        if (str_destino == null) 
        {
            if (other.str_destino != null)
                return false;
        } else if (!str_destino.equals(other.str_destino))
            return false;
        return true;
    }

 /**
  * Devuelve una representacion en forma de cadena de este objeto clsAlojamiento.
  * 
  * @return Una cadena que contiene el destino, el numero de habitaciones, 
  *       la cantidad de personas posibles, el propietario y el precio por noche.
  */
 @Override
 public String toString() 
 {
    return "DESTINO: [" + str_destino + "], NUMERO DE HABITACIONES: [" 
    + int_numeroHabitaciones + "], CANTIDAD DE PERSONAS POSIBLES: [" 
    + int_cantidad + "], PROPIETARIO: [" + str_propietario + 
    "], PRECIO POR NOCHE: [" + int_precio + "]";
 }    
}