package LN;

import java.util.Comparator;

/**
 * Este metodo comprara los vuelos por el ID para ordenarlos
 */
public class clsComparadorVuelosID implements Comparator<clsVuelo>
{
    /**
     * Constructor
     */
    public clsComparadorVuelosID()
    {

    }
    
    @Override
    public int compare(clsVuelo obj_1, clsVuelo obj_2) 
    {
        return obj_1.getID().compareTo(obj_2.getID());
    }
}