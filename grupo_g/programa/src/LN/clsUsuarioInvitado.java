package LN;

import COMUN.clsConstantes;

/**
 * Esta clase representa a un usuario invitado en el sistema.
 * Hereda de la clase clsUsuario y puede tener caracteristicas para usuarios
 * invitados.
 * @see clsUsuario
 */
public class clsUsuarioInvitado extends clsUsuario 
{
    /**
     * Este atributo para contar el numero de accesos.
     */
    private int int_numAccesos;

    /**
     * Este es un constructor que inicializa el numero de accesos a 0.
     */
    public clsUsuarioInvitado()
    {
        int_numAccesos = 0;
    }

    /**
     * Este es el constructor que se utiliza cuando un usuario se registra.
     * 
     * @param int_paramNumAccesos El numero de accesos.
     */
    public clsUsuarioInvitado(int int_paramNumAccesos)
    {
        int_numAccesos = int_paramNumAccesos;
    }

    /**
     *     
     * Esta clase obtiene el numero de accesos.
     *  
     * @return El numero de accesos.
     */
    public int getNumAccesos() 
    {
        return int_numAccesos;
    }

    /**    
     * Establece el numero de accesos.
     * 
     * @param int_paramNumAccesos El numero de accesos.
     */
    public void setNumAccesos(int int_paramNumAccesos) 
    {
        int_numAccesos = int_paramNumAccesos;
    }
    
    /**
     * Este metodo sirve para obtener el numero de accesos.
     */
    @Override
    public Object getObjectProperty(String str_propiedad)
    {
        switch (str_propiedad) 
        {
            case clsConstantes.NUMACCESOS:
                return int_numAccesos;
            default:
                return super.getObjectProperty (str_propiedad);
        }
    }
}