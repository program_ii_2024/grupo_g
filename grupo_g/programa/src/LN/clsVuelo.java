package LN;

import COMUN.clsConstantes;
import COMUN.itfProperty;
import EXCEPCIONES.clsPropiedadNoExistente;

/**
 * Esta clase representa un vuelo en el sistema, que es un tipo de reserva.
 * Contiene atributos y metodos relacionados con la gestion de vuelos.
 * Hereda de la clase clsReserva para compartir funcionalidades comunes entre
 * reservas.
 * @see clsReserva
 */
public class clsVuelo implements itfProperty, Comparable<clsVuelo>
{

    /**
     * Estas son las variables de la clase clsVuelo.
     */
    private String str_origen; 
    private String str_id;
    private String str_nombre;
    private String str_destino;
    private String str_horaSalida;
    private String str_horaLlegada;
    private int int_cantidadEquipaje;
    private int int_numeroPasajeros;
    private int int_precio;
    
    /**
     * Este es el constructor que se utiliza cuando estas reservando un vuelo
     * con sus distintos parametros.
     * 
     * @param str_paramNombre Nombre del pasajero representante
     * @param int_paramPrecio Precio del vuelo
     * @param int_paramCantidadEquipaje Cantidad de equipaje
     * @param int_paramNumeroPasajeros Numero de pasajeros de la reserva
     * @param str_paramDestino Destino del vuelo
     * 
     */
    public clsVuelo(String str_paramDestino, String str_paramNombre,
    int int_paramNumeroPasajeros, int int_paramCantidadEquipaje, 
    int int_paramPrecio)
    {
        str_destino = str_paramDestino;
        str_nombre = str_paramNombre;
        int_numeroPasajeros = int_paramNumeroPasajeros;
        int_cantidadEquipaje = int_paramCantidadEquipaje;
        int_precio = int_paramPrecio;
    }

    /**
     * Este es el constructor que se utiliza cuando estas reservando un vuelo.
     * 
     * @param str_paramDestino El destino del vuelo.
     * @param str_paramHoraSalida La hora de salida del vuelo.
     * @param str_paramHoraLlegada La hora de llegada del vuelo.
     * @param int_paramCantidadEquipaje La cantidad de equipaje del vuelo.
     * @param int_paramPrecio El precio del vuelo.
     * @param str_paramNombre El nombre del vuelo.
     * @param int_paramNumeroPasajeros El numero de pasajeros del vuelo.
     */
    public clsVuelo(String str_paramDestino, String str_paramHoraSalida, 
    String str_paramHoraLlegada, int int_paramCantidadEquipaje,
    int int_paramPrecio, String str_paramNombre, int int_paramNumeroPasajeros)
    {
        str_destino = str_paramDestino;
        str_horaSalida = str_paramHoraSalida;
        str_horaLlegada = str_paramHoraLlegada;
        int_cantidadEquipaje = int_paramCantidadEquipaje;
        int_precio = int_paramPrecio;
        str_nombre = str_paramNombre;
        int_numeroPasajeros = int_paramNumeroPasajeros;
    }

    /**
     * Este es el constructor que se utiliza cuando estas reservando un vuelo
     * con sus respectivos parametros.
     * 
     * @param str_paramOrigen El origen del vuelo.
     * @param str_paramDestino El destino del vuelo.
     * @param str_paramHoraSalida La hora de salida del vuelo.
     * @param str_paramHoraLlegada La hora de llegada del vuelo.
     * @param int_paramPrecio El precio del vuelo.
     * @param str_paramId El ID del vuelo.
     */
    public clsVuelo(String str_paramOrigen,String str_paramDestino, 
    String str_paramHoraSalida, String str_paramHoraLlegada, 
    int int_paramPrecio, String str_paramId)
    {
        str_origen = str_paramOrigen;
        str_destino = str_paramDestino;
        str_horaSalida = str_paramHoraSalida;
        str_horaLlegada = str_paramHoraLlegada;
        int_precio = int_paramPrecio;
        str_id = str_paramId;
    }

    /**
     * Este es el constructor que se utiliza cuando estas reservando un vuelo
     * con sus diferentes parametros.
     * 
     * @param str_paramDestino El destino del vuelo.
     * @param str_paramNombre El nombre del vuelo.
     * @param int_paramNumeroPasajeros El numero de pasajeros del vuelo.
     * @param int_paramPrecio El precio del vuelo.
     */
    public clsVuelo(String str_paramDestino, String str_paramNombre, 
    int int_paramNumeroPasajeros, int int_paramPrecio)
    {
        str_destino = str_paramDestino;
        str_nombre = str_paramNombre;
        int_numeroPasajeros = int_paramNumeroPasajeros;
        int_precio = int_paramPrecio;
    }

    /**
     * Este es el constructor que se utiliza cuando estas reservando un vuelo.
     * 
     * @param str_paramDestino El destino del vuelo.
     * @param str_paramHoraSalida La hora de salida del vuelo.
     * @param str_paramHoraLlegada La hora de llegada del vuelo.
     * @param str_paramNombre El nombre del vuelo.
     * @param int_paramNumeroPasajeros El numero de pasajeros del vuelo.
     * @param int_paramCantidadEquipaje La cantidad de equipaje del vuelo.
     * @param int_paramPrecio El precio del vuelo.
     */
    public clsVuelo(String str_paramDestino, String str_paramHoraSalida, 
    String str_paramHoraLlegada, String str_paramNombre, 
    int int_paramNumeroPasajeros, int int_paramCantidadEquipaje, 
    int int_paramPrecio) 
    {
        str_destino = str_paramDestino;
        str_horaSalida = str_paramHoraSalida;
        str_horaLlegada = str_paramHoraLlegada;
        str_nombre = str_paramNombre;
        int_numeroPasajeros = int_paramNumeroPasajeros;
        int_cantidadEquipaje = int_paramCantidadEquipaje;
        int_precio = int_paramPrecio;
    }

    /**
     * Este metodo define el nombre del representante del vuelo.
     * @param str_paramNombre El nombre del representante del vuelo
     */    
    public clsVuelo(String str_paramNombre)
    {
        str_nombre = str_paramNombre;
    }

    /**
     * Este metodo obtiene el ID del vuelo.
     * @return El ID del vuelo.
     */
    public String getID() 
    {
        return str_id;
    }

    /**
     * Este metodo establece el ID del vuelo.
     * @param str_paramId El ID del vuelo.
     */
    public void setInt_ID(String str_paramId) 
    {
        str_id = str_paramId;
    }

    /**
     * Este metodo obtiene el origen del vuelo.
     * @return El origen del vuelo.
     */
    public String getOrigen() 
    {
        return str_origen;
    }

    /**
     * Este metodo establece el origen del vuelo.
     * @param str_paramOrigen El origen del vuelo.
     */
    public void setOrigen(String str_paramOrigen) 
    {
        str_origen = str_paramOrigen;
    }

    /**
     * Obtiene la hora de salida del vuelo.
     * Te devuelve la hora de salida del vuelo.
     * @return La hora de salida
     */
    public String getHoraSalida() 
    {
        return str_horaSalida;
    }

    /**
     * Establece la hora de salida del vuelo.
     * @param str_paramHoraSalida La hora de salida del vuelo.
     */
    public void setHoraSalida(String str_paramHoraSalida)
    {
        this.str_horaSalida = str_paramHoraSalida;
    }

    /**
     * Obtiene la hora de llegada del vuelo.
     * @return La hora de llegada del vuelo.
     */
    public String getHoraLlegada()
    {
        return str_horaLlegada;
    }

    /**
     * Establece la hora de llegada del vuelo.
     * @param str_paramHoraLlegada La hora de llegada del vuelo.
     */
    public void setHoraLlegada(String str_paramHoraLlegada)
    {
        this.str_horaLlegada = str_paramHoraLlegada;
    }

    /**
     * Obtiene la cantidad de equipaje del vuelo.
     * @return La cantidad de equipaje del vuelo.
     */
    public int getCantidadEquipaje()
    {
        return int_cantidadEquipaje;
    }

    /**
     * Establece la cantidad de equipaje del vuelo.
     * @param int_cantidadEquipaje La cantidad de equipaje del vuelo.
     */
    public void setCantidadEquipaje(int int_cantidadEquipaje) 
    {
        this.int_cantidadEquipaje = int_cantidadEquipaje;
    }

    /**
     * Este metodo obtiene el destino del vuelo.
     * @return El destino del vuelo.
     */
    public String getDestino()
    {
        return str_destino;
    }

    /**
     * Este metodo establece el destino del vuelo.
     * @param str_paramDestino El destino del vuelo.
     */
    public void setDestino (String str_paramDestino)
    {
        str_destino = str_paramDestino;
    }

    /**
     * Este metodo obtiene el precio del vuelo.
     * @return El precio del vuelo.
     */
    public int getPrecio()
    {
        return int_precio;
    }

    /**
     * Este metodo establece el precio del vuelo.
     * @param int_paramPrecio El precio del vuelo.
     */
    public void setInt_precio(int int_paramPrecio)
    {
        this.int_precio = int_paramPrecio;
    }

    /**
     * Este metodo sirva para cuando el usuario nos pida que le mostremos un 
     * valor, solo se lo ensenemos si nosotros lo permitimos y si nos pide algo
     * que no hemos permitido no se lo dara.
     */
    @Override
    public Object getObjectProperty(String str_propiedad)
    {
        switch (str_propiedad) 
        {
            case clsConstantes.NOMBRE_PASAJERO:
                return str_nombre;

            case clsConstantes.DESTINO_VUELO:
                return str_destino;

            case clsConstantes.HORA_LLEGADA:
                return str_horaLlegada;

            case clsConstantes.HORA_SALIDA:
                return str_horaSalida;

            case clsConstantes.CANTIDAD_EQUIPAJE:
                return int_cantidadEquipaje;

            case clsConstantes.NUMERO_PASAJEROS:
                return int_numeroPasajeros;

            case clsConstantes.PRECIO_VUELO:
                return int_precio;

            case clsConstantes.ID_VUELO:
                return str_id;
            case clsConstantes.ORIGEN:
                return str_origen;    

            default: throw new clsPropiedadNoExistente
            ("Esta propiedad no existe!");    
        }    
    }

     /**
     * Compara este vuelo con otro vuelo basado en sus ID.
     * @param obj_o El vuelo con el que se va a comparar.
     * @return Un valor negativo si este ID es menor que el vuelo dado,
     *         cero si son iguales y un valor positivo si este ID es mayor.
     */
    @Override
    public int compareTo(clsVuelo obj_o) 
    {
        return str_id.compareTo(obj_o.str_id);
    }
    /**
     * Este metodo obtiene el nombre del vuelo.
     * @return El nombre del vuelo.
     */
    public String getNombre() 
    {
        return this.str_nombre;   
    }

    /**
     * Compara el objeto actual con otro objeto para verificar si son iguales.
     * 
     * @param obj El objeto con el que se va a comparar.
     * @return True si el objeto actual es igual al otro objeto o false en caso 
     * contrario.
     */
    @Override
    public boolean equals(Object obj) 
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
         clsVuelo other = (clsVuelo) obj;
        if (str_id == null) {
            if (other.str_id != null)
                return false;
        } else if (!str_id.equals(other.str_id))
            return false;
        return true;
    }

    /**
     * Devuelve una representacion en cadena de caracteres del objeto.
     * 
     * @return Una cadena que representa el objeto, incluyendo su origen, 
     * destino, hora de salida, hora de llegada, precio e ID.
     */
    @Override
    public String toString() 
    {
        return "ORIGEN: [" + str_origen + "], -->  DESTINO: [" + str_destino
                + "],    HORA DE SALIDA: [" + str_horaSalida + 
                "],    HORA DE LLEGADA: [" + str_horaLlegada + "],    PRECIO: ["
                + int_precio + "],    ID: [" + str_id;
    }
}