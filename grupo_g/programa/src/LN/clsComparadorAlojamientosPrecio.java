package LN;

import java.util.Comparator;

/**
 * Comparador para objetos clsAlojamiento basado en el precio.
 * Implementa Comparator para comparar dos objetos clsAlojamiento
 * segun su precio.
 */
public class clsComparadorAlojamientosPrecio implements 
Comparator<clsAlojamiento> 
{

    /**
     * Compara dos objetos de tipo clsAlojameinto basado en el precio de los 
     * vuelos.
     * @param obj_1 El primer objeto clsAlojamiento a comparar.
     * @param obj_2 El segundo objeto clsAlojamiento a comparar.
     * @return Un entero negativo si obj_1 es menor que obj_2, un entero 
     * positivo si obj_1 es mayor que obj_2, o 0 si obj_1 es igual a obj_2 en 
     * terminos de precio.
     */
    @Override
    public int compare(clsAlojamiento obj_1, clsAlojamiento obj_2)
    {
        return obj_1.getPrecio() - obj_2.getPrecio();
    }

    /**
     * Constructor vacio
     */
    public clsComparadorAlojamientosPrecio()
    {

    }
}