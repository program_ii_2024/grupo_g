package LN;

import java.util.Comparator;

/**
 * Esta clase usa un comparador para comparar objetos de tipo clsVuelo basado
 * en el precio de los vuelos.
 */
public class clsComparadorVuelosPrecio implements Comparator<clsVuelo>
{
    /**
     * Compara dos objetos de tipo clsVuelo basado en el precio de los vuelos.
     * @param o1 El primer objeto clsVuelo a comparar.
     * @param o2 El segundo objeto clsVuelo a comparar.
     * @return Un entero negativo si o1 es menor que o2, un entero positivo si
     *  o1 es mayor que o2, o 0 si o1 es igual a o2 en términos de precio.
     */
    @Override
    public int compare(clsVuelo o1, clsVuelo o2)
    {
        return o1.getPrecio() - o2.getPrecio();
    }

    /**
     * Constructor vacio
     */
    public clsComparadorVuelosPrecio ()
    {

    }
}