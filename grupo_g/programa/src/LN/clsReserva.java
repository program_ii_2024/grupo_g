package LN;

import java.time.LocalDate;

import COMUN.*;
import EXCEPCIONES.clsPropiedadNoExistente;

/**
 * Esta clase representa una reserva en el sistema.
 * Las clases que heredan de esta implementar sus metodos.
 */
public class clsReserva implements itfProperty
{
    //Variables de clsReserva.
    private LocalDate dt_fechaInicio;
    private LocalDate dt_fechaFinal;
    private String str_nombreReserva;

    /**
     * Esta clase obtiene el destino, fecha de inicio y fin y el nombre.
     * 
     * @param str_destino2 El destino de la reserva.
     * @param dt_fechaParamInicio La fecha de inicio de la reserva.
     * @param dt_fechaParamFinal La fecha final de la reserva.
     * @param str_paramNombre El nombre final de la reserva.
     */
    public clsReserva(String str_destino2, LocalDate dt_fechaParamInicio, 
        LocalDate dt_fechaParamFinal, String str_paramNombre )
    {
        dt_fechaInicio = dt_fechaParamInicio;
        dt_fechaFinal = dt_fechaParamFinal;
        str_nombreReserva = str_paramNombre;
    }

    /**
     * Esta clase obtiene el nombre de la reserva.
     * 
     * @param str_nombre El nombre de la reserva.
     */
    public clsReserva(String str_nombre)
    {
        str_nombreReserva = str_nombre;
    }

    /**    
     * Esta clase obtiene el dia de inicio de la reserva.
     * 
     * @return La fecha de inicio de la reserva.
     */
    public LocalDate getDiaInicio() 
    {
        return dt_fechaInicio;
    }

    /**    
     * Establece el dia de inicio de la reserva.
     * 
     * @param dt_fechaDiaInicio El dia de inicio de la reserva.
     */
    public void setDiaInicio(LocalDate dt_fechaDiaInicio) 
    {
        dt_fechaInicio = dt_fechaDiaInicio;
    }

    /**   
     * Esta clase obtiene el dia final de la reserva.
     * 
     * @return El dia final de la reserva.
     */
    public LocalDate getDiaFinal() 
    {
        return dt_fechaFinal;
    }

    /**    
     * Establece el dia final de la reserva.
     * 
     * @param dt_fechaDiaFinal El dia final de la reserva.
     */
    public void setDiaFinal(LocalDate dt_fechaDiaFinal) 
    {
        this.dt_fechaFinal = dt_fechaDiaFinal;
    }

    /**
     * Esta clase obtiene el nombre del representante de la reserva.
     * 
     * @return El nombre del representante de la reserva.
     */
    public String getNombre() 
    {
        return str_nombreReserva;
    }

    /**
     * Establece el nombre del representante de la reserva.
     * 
     * @param str_paramNombre El nombre del representante de la reserva.
     */
    public void setEstado (String str_paramNombre) 
    {
        str_nombreReserva = str_paramNombre;
    }

//------------------------------------------------------------------------------

    /**
     * Este metodo sirva para cuando el usuario nos pida que le mostremos un 
     * valor, solo se lo enseñemos si nosotros lo permitimos y si nos pide algo
     * que no hemos permitido no se lo dara.
     */
    @Override
    public Object getObjectProperty(String str_propiedad)
    {
        switch (str_propiedad) 
        {
            case clsConstantes.FECHA_INICIO: return dt_fechaInicio;
            case clsConstantes.FECHA_FINAL : return dt_fechaFinal;          
            case clsConstantes.NOMBRE_RESERVA : return str_nombreReserva;
            default: throw new clsPropiedadNoExistente
            ("Esta propiedad no existe!");
        }
    }
}