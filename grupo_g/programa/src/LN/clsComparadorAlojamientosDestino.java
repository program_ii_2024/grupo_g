package LN;

import java.util.Comparator;

/**
 * Metodo que compara los alojamientos por destino para ordenarlos 
 * alfabeticamente
 */
public class clsComparadorAlojamientosDestino implements 
                                                      Comparator<clsAlojamiento>
{

    @Override
    public int compare(clsAlojamiento obj_1, clsAlojamiento obj_2) 
    {
        return obj_1.getDestino().compareTo(obj_2.getDestino());    
    }

    /**
     * Constructor
     */
    public clsComparadorAlojamientosDestino()
    {
        
    }
    
}