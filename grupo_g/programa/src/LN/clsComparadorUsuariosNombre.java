package LN;

import java.util.Comparator;

/**
 * Comparador para objetos clsUsuarioRegistrado basado en el nombre.
 * Implementa Comparator para comparar dos objetos clsUsuarioRegistrado
 * segun su nombre.
 */
public class clsComparadorUsuariosNombre implements 
Comparator<clsUsuarioRegistrado>
{
    /**
     * Compara dos objetos de tipo clsUsuarioRegistrado basado en el nombre.
     * 
     * @param obj_1 El primer objeto clsUsuarioRegistrado a comparar.
     * @param obj_2 El segundo objeto clsUsuarioRegistrado a comparar.
     * @return Un entero negativo si el nombre de obj_1 es menor que el de 
     * obj_2, un entero positivo si el nombre de obj_1 es mayor que el de obj_2, 
     * o 0 si ambos nombres son iguales.
     */
    @Override
    public int compare(clsUsuarioRegistrado obj_1, clsUsuarioRegistrado obj_2)
    {
        return obj_1.getNombre().compareTo(obj_2.getNombre());
    }
}
