package LN;

/**
 * Esta clase representa el usuario administrador y extiende la clase 
 * clsUsuarioRegistrado.
 * 
 * @see clsUsuarioRegistrado
 */
public class clsUsuarioRegistradoAdministrador extends clsUsuarioRegistrado
{
    /**
     * Este constructor es para el administrador.
     * 
     * @param str_paramNombre El nombre del administrador.
     * @param str_paramApellido El apellido del administrador.
     * @param str_paramDni El DNI del administrador.
     * @param str_paramCorreo El correo del administrador.
     * @param str_paramClave La clave del administrador.
     * @param int_paramRol El rol del administrador
     */

    public clsUsuarioRegistradoAdministrador(
                String str_paramNombre, 
                String str_paramApellido, 
                String str_paramDni, 
                String str_paramCorreo, 
                String str_paramClave,
                enRol int_paramRol)
    {
        super(str_paramNombre, 
                str_paramApellido, 
                str_paramDni, 
                str_paramCorreo, 
                str_paramClave,
                int_paramRol);
    }
}