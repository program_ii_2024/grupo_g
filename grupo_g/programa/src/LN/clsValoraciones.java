package LN;

/**
 * Esta clase representa un conjunto de valoraciones en el sistema.
 * Contiene metodos y atributos relacionados con la gestion de las valoraciones
 * de los usuarios.
 */
public class clsValoraciones 
{
    /**Atributo de numero de estrellas de la valoracion. */
    private int int_numEstrellas;
    /**Atributo de nota de la valoracion. */
    private int int_nota;
    /**Atributo del comentario de la valoracion. */
    private String str_comentario;


    /** 
     * Este es un constructor vacio.
     * 
     */
    public clsValoraciones()
    {

    }

     /** 
     * Este es el constructor que registra las valoraciones que hacen los 
     * usuarios sobre los alojamientos.
     * 
     * @param int_NumEstrellas El numero de estrellas.
     * @param int_nota         La nota del alojamiento.
     * @param str_comentario   Los comentarios del alojamiento.
     */
    public clsValoraciones(int int_NumEstrellas, int int_nota, 
    String str_comentario)
    {

    }
    /**     
     * Este metodo obtiene el numero de estrellas del alojamiento
     * 
     * @return El numero de estrellas.
     */
    public int getNumEstrellas() 
    {
        return int_numEstrellas;
    }

    /**    
     * Este metodo establece el numero de estrellas del alojamiento.
     * 
     * @param int_numEstrellas El numero de estrellas.
     */
    public void setNumEstrellas(int int_numEstrellas) 
    {
        this.int_numEstrellas = int_numEstrellas;
    }

    /**
     *     
     * Este metodo obtiene la nota del alojamiento.
     * 
     * @return La nota del alojamiento.
     */
    public int getNota() 
    {
        return int_nota;
    }

    /**     
     * Este metodo establece la nota del alojamiento.
     * 
     * @param int_nota La nota del alojamiento.
     */
    public void setNota(int int_nota) 
    {
        this.int_nota = int_nota;
    }

    /**    
     * Este metodo obtiene los comentarios del alojamiento.
     * 
     * @return El comentario del alojamiento.
     */
    public String getComentario() 
    {
        return str_comentario;
    }

    /**     
     * Este metodo establece el comentario del alojamiento.
     * 
     * @param str_comentario El comentario del alojamiento.
     */
    public void setComentario(String str_comentario) 
    {
        this.str_comentario = str_comentario;
    }
}