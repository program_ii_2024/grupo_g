package LN;

import COMUN.itfProperty;
import EXCEPCIONES.clsPropiedadNoExistente;
import COMUN.clsConstantes;

/**
 * Esta clase representa un usuario generico en el sistema.
 * Contiene atributos y metodos relacionados con la gestion de usuarios.
 */
public abstract class clsUsuario implements itfProperty, Comparable<clsUsuario>
{
    /**
     * Este es el atributo de la ubicacion.
     */
    private String str_ubicacion;

    /**
     * Este es un constructor por defecto.
     * 
     */
    public clsUsuario()
    {
        str_ubicacion = null;
    }

    /**
     * 
     * Este es el constructor que se utiliza para guardar la ubicacion del 
     * usuario.
     * 
     * @param str_paramUbicacion La ubicacion del usuario.
     */
    public clsUsuario(String str_paramUbicacion)
    {
        this.str_ubicacion = str_paramUbicacion;
    }

    /**
     * Recibe la ubicacion desde donde esta navegando el usuario.
     * @return La ubicacion desde donde esta navegando el usuario.
     */
    public String getUbicacion() 
    {
        return str_ubicacion;
    }

    /**
     * Establece la ubicacion desde donde esta navegando el usuario.
     * @param str_paramUbicacion La ubicacion del usuario.
     */
    public void setUbicacion(String str_paramUbicacion)
    {
        str_ubicacion = str_paramUbicacion;
    }

    /**
     * Este metodo sirve para obtener la ubicacion.
     */
    @Override
    public Object getObjectProperty(String str_propiedad)
    {
        switch(str_propiedad)
        {
            case clsConstantes.UBICACION : 
                    return str_ubicacion;            
            
            default: throw new clsPropiedadNoExistente
            ("Esta propiedad no existe!");
        }
    }

    /**
     * Este metodo sirve para comparar la ubicacion del usuario. 
     */
    @Override
    public int compareTo(clsUsuario obj_o)
    {
        return str_ubicacion.compareTo(obj_o.str_ubicacion);
    }
}