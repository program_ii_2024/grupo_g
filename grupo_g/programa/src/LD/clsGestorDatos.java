package LD;

import java.sql.*;
import COMUN.*;
import LN.enRol;

/**Este es el gestor que se comunica con la base de datos */
public class clsGestorDatos 
{
	//Variables constantes
    private static final String URL = "jdbc:mysql://localhost:3306/";
	private static final String SCHEMA = "deustoking";
	private static final String PARAMS = "?useUnicode=true&useJDBCCompliantTime"
										+"zoneShift=true&useLegacyDatetimeCode="
									   +"false&serverTimezone=UTC&useSSL=false";
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String USER = "root";
	private static final String PASS = "root";

	/**
	 * Objeto para crear la conexion a la base de datos.
	 */
	Connection obj_conexion = null;

	/**
	 * Objeto para crear la consulta a base de datos.
	 */
	PreparedStatement obj_ps = null;

	/**
	 * Objeto para devolver el resultado de la consulta.
	 */
	ResultSet obj_rs = null;

	/**
	 * Constructor sin parametros de la clase.
	 */
	public clsGestorDatos() 
	{

	}

	/**
	 * Metodo para la conexion a la base de datos. 
	 */
	public void vo_conectar() 
    {
		try 
        {
			//Se carga el driver de acceso a datos.
			Class.forName(DRIVER);
			obj_conexion = DriverManager.getConnection
			(URL + SCHEMA + PARAMS, USER, PASS);
        
			System.out.println("Debug: CONECTED TO DATABASE 'DEUSTOKING'");
		} 
        catch (Exception e) 
        {
			System.out.println("DEBUG: NO CONNECTION");
		}
	}

	/**
	 * Metodo para desconectar de la base de datos.
	 */
	public void vo_desconectar() 
	{
		try 
		{
			obj_conexion.close();
			obj_ps.close(); //Cerrar el statement tambien cierra el resultset.
		} catch (SQLException e)
		{

		} finally 
		{
			try 
			{
				obj_conexion.close();
			} catch (Exception e) 
			{
				/* no hago nada */
			}
			try 
			{
				obj_ps.close();
			} catch (Exception e) 
			{
				/* no hago nada */
			}
		}
	}

	/**
	 * Metodo para insertar un usuario en la base de datos.
	 * 
	 * @param str_nombre El nombre del usuario.
	 * @param str_apellido El apellido del usuario.
	 * @param str_dni El DNI del usuario.
	 * @param str_correo El correo del usuario.
	 * @param str_clave La clave del usuario.
	 * @param rol El rol del usuario
	 */
    public void vo_insertarUsuarioBD(String str_nombre, String str_apellido, 
    String str_dni, String str_correo, String str_clave, enRol rol) 
    {
		try 
		{
			obj_ps = obj_conexion.prepareStatement
			 (clsConstantesLD.SQL_INSERT_USUARIO);

            obj_ps.setString(1, str_nombre);
            obj_ps.setString(2, str_apellido);
            obj_ps.setString(3, str_dni);
			obj_ps.setString(4, str_correo);
			obj_ps.setString(5, str_clave);
			obj_ps.setString(6, rol.name());

			System.out.println(obj_ps);

			obj_ps.executeUpdate();

		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}
	}

	/**
	 * Metodo para recuperar usuarios de la base de datos.
	 * 
	 * @return Un resultado que contiene los datos de los usuarios recuperados 
	 * de la base de datos o null si ocurre un error durante la ejecucion de la 
	 * consulta.
	 */
    public ResultSet recuperarUsuariosBD() 
	{
		try 
		{
			obj_ps = obj_conexion.prepareStatement
										  (clsConstantesLD.SQL_SELECT_USUARIOS);
			obj_rs = obj_ps.executeQuery();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}
		return obj_rs;
	}

	/**
	 * Recupera los vuelos de la base de datos y devuelve un objeto ResultSet 
	 * que tiene los resultados.
	 * 
	 * @return Un objeto ResultSet que tiene los vuelos recuperados de la base 
	 * de datos.
	 */
	public ResultSet recuperarVuelosBD()
	{
		try 
		{
			obj_ps = obj_conexion.prepareStatement
									(clsConstantesLD.SQL_SELECT_VUELOS);
			obj_rs = obj_ps.executeQuery();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}
		return obj_rs;
	}

	/**
 	 * Recupera un conjunto de resultados que contiene informacion de vuelos con 
	 * origen en Vitoria desde la base de datos.
 	 *
 	 * @return Un resultado que contiene los datos de los vuelos con origen en 
	 * Vitoria recuperados de la base de datos o null si ocurre un error durante
	 * la consulta.
	 */
	public ResultSet recuperarVuelosVitoriaBD() 
	{
		try 
		{
			obj_ps = obj_conexion.prepareStatement
									(clsConstantesLD.SQL_SELECT_VUELOS_VITORIA);
			obj_rs = obj_ps.executeQuery();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}
		return obj_rs;
	}

	/**
 	 * Recupera un conjunto de resultados que contiene informacion de vuelos con 
	 * origen en Bilbao desde la base de datos.
 	 *
 	 * @return Un resultado que contiene los datos de los vuelos con origen en 
	 * Bilbao recuperados de la base de datos o null si ocurre un error durante
	 * la consulta.
	 */
	public ResultSet recuperarVuelosBilbaoBD() 
	{
		try 
		{
			obj_ps = obj_conexion.prepareStatement
									 (clsConstantesLD.SQL_SELECT_VUELOS_BILBAO);
			obj_rs = obj_ps.executeQuery();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}
		return obj_rs;
	}

	/**
 	 * Recupera un conjunto de resultados que contiene informacion de vuelos con 
	 * origen en Torrevieja desde la base de datos.
 	 *
 	 * @return Un resultado que contiene los datos de los vuelos con origen en 
	 * Torrevieja recuperados de la base de datos o null si ocurre un error 
	 * durante la consulta.
	 */
	public ResultSet recuperarVuelosTorreviejaBD() 
	{
		try 
		{
			obj_ps = obj_conexion.prepareStatement
								 (clsConstantesLD.SQL_SELECT_VUELOS_TORREVIEJA);
			obj_rs = obj_ps.executeQuery();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}
		return obj_rs;
	}

	/**
 	 * Recupera un conjunto de resultados que contiene informacion de vuelos con 
	 * origen en Albacete desde la base de datos.
 	 *
 	 * @return Un resultado que contiene los datos de los vuelos con origen en 
	 * Albacete recuperados de la base de datos o null si ocurre un error 
	 * durante la consulta.
	 */
	public ResultSet recuperarVuelosAlbaceteBD() 
	{
		try 
		{
			obj_ps = obj_conexion.prepareStatement
								   (clsConstantesLD.SQL_SELECT_VUELOS_ALBACETE);
			obj_rs = obj_ps.executeQuery();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}
		return obj_rs;
	}

	/**
 	 * Recupera un conjunto de resultados que contiene informacion de vuelos con 
	 * origen en Madrid desde la base de datos.
 	 *
 	 * @return Un resultado que contiene los datos de los vuelos con origen en 
	 * Madrid recuperados de la base de datos o null si ocurre un error durante
	 * la consulta.
	 */
	public ResultSet recuperarVuelosMadridBD() 
	{

		try 
		{
			obj_ps = obj_conexion.prepareStatement
								     (clsConstantesLD.SQL_SELECT_VUELOS_MADRID);
			obj_rs = obj_ps.executeQuery();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}
		return obj_rs;
	}

	/**
 	 * Recupera un conjunto de resultados que contiene informacion de vuelos con 
	 * origen en Maldivas desde la base de datos.
 	 *
 	 * @return Un resultado que contiene los datos de los vuelos con origen en 
	 * Maldivas recuperados de la base de datos o null si ocurre un error 
	 * durante la consulta.
	 */
	public ResultSet recuperarVuelosMaldivasBD() 
	{
		try 
		{
			obj_ps = obj_conexion.prepareStatement
								   (clsConstantesLD.SQL_SELECT_VUELOS_MALDIVAS);
			obj_rs = obj_ps.executeQuery();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}
		return obj_rs;
	}

	/**
 	 * Recupera un conjunto de resultados que contiene informacion de vuelos con 
	 * origen en Malaga desde la base de datos.
 	 *
 	 * @return Un resultado que contiene los datos de los vuelos con origen en 
	 * Malaga recuperados de la base de datos o null si ocurre un error durante
	 * la consulta.
	 */
	public ResultSet recuperarVuelosMalagaBD() 
	{
		try 
		{
			obj_ps = obj_conexion.prepareStatement
								 	 (clsConstantesLD.SQL_SELECT_VUELOS_MALAGA);
			obj_rs = obj_ps.executeQuery();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}
		return obj_rs;
	}

	/**
	 * Recupera la informacion de alojamientos de la base de datos y devuelve un
	 * objeto ResultSet que tiene los resultados.
	 * 
 	 * @return Un objeto ResultSet que tiene la informacion de los alojamientos
	 * recuperada de la base de datos.
	 */
	public ResultSet recuperarInfoAlojamientosBD()
	{	
		try 
		{
			obj_ps = obj_conexion.prepareStatement
								 	 (clsConstantesLD.SQL_SELECT_ALOJAMIENTOS);
			obj_rs = obj_ps.executeQuery();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}
		return obj_rs;
	}

	/**
	 * Borra un usuario de la base de datos utilizando su DNI.
	 * 
	 * @param str_dni El DNI del usuario que se va a borrar.
	 */
	public void vo_borrarUsuarioBD(String str_dni) 
	{
		try 
		{			
			obj_ps = obj_conexion.prepareStatement
										  (clsConstantesLD.SQL_DELETE_USUARIOS);
			obj_ps.setString(1, str_dni);
			obj_ps.executeUpdate();
		} catch (SQLException e) 
		{
			e.printStackTrace();
		} finally 
		{
		}
	}

	/**
	 * Este metodo envia los datos a modificar a la base de datos
	 * @param str_nombre El nombre del usuario
	 * @param str_apellido El apellido del usuario
	 * @param str_dni El dni del usuario 
	 * @param str_correo El correo del usuario
	 * @param str_clave La clave del usuario
	 */
	public void vo_modificarUsuarioBD(String str_nombre, 
									  String str_apellido, 
									  String str_dni, 
									  String str_correo,
									  String str_clave)
	{
		vo_conectar();
		try
		{
			obj_ps = obj_conexion.prepareStatement
										  (clsConstantesLD.SQL_MODIFY_USUARIOS);

			obj_ps.setString(1, str_nombre);
			obj_ps.setString(2, str_apellido);
			obj_ps.setString(3, str_dni);
			obj_ps.setString(4, str_correo);
			obj_ps.setString(5, str_clave);
			obj_ps.setString(6, str_dni);
			obj_ps.executeUpdate();
				
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}finally
		{
		}
		vo_desconectar();
	}
}