CREATE DATABASE  IF NOT EXISTS `deustoking` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `deustoking`;
-- MySQL dump 10.13  Distrib 8.0.36, for Win64 (x86_64)
--
-- Host: localhost    Database: deustoking
-- ------------------------------------------------------
-- Server version	8.3.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aeropuertos`
--

DROP TABLE IF EXISTS `aeropuertos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aeropuertos` (
  `ORIGEN` varchar(160) NOT NULL,
  `DESTINO` varchar(160) DEFAULT NULL,
  `HORA_SALIDA` varchar(60) DEFAULT NULL,
  `HORA_LLEGADA` varchar(60) DEFAULT NULL,
  `PRECIO` int DEFAULT NULL,
  `ID` varchar(160) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aeropuertos`
--

LOCK TABLES `aeropuertos` WRITE;
/*!40000 ALTER TABLE `aeropuertos` DISABLE KEYS */;
INSERT INTO `aeropuertos` VALUES ('Torrevieja',NULL,NULL,NULL,NULL,'V001'),('Malaga',NULL,NULL,NULL,NULL,'V002'),('Albacete',NULL,NULL,NULL,NULL,'V003'),('Vitoria-Gasteiz',NULL,NULL,NULL,NULL,'V004'),('Bilbao',NULL,NULL,NULL,NULL,'V005'),('Maldivas',NULL,NULL,NULL,NULL,'V006'),('Madrid',NULL,NULL,NULL,NULL,'V007');
/*!40000 ALTER TABLE `aeropuertos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alojamientos`
--

DROP TABLE IF EXISTS `alojamientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alojamientos` (
  `fechaInicio` varchar(50) DEFAULT NULL,
  `fechaFinal` varchar(50) DEFAULT NULL,
  `Nombre` varchar(255) DEFAULT NULL,
  `Destino` varchar(255) DEFAULT NULL,
  `NumeroHabitaciones` int DEFAULT NULL,
  `Cantidad` int DEFAULT NULL,
  `Propietario` varchar(255) DEFAULT NULL,
  `Precio` int DEFAULT NULL,
  `FechaLlegada` varchar(50) DEFAULT NULL,
  `FechaSalida` varchar(50) DEFAULT NULL,
  `NumNoches` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alojamientos`
--

LOCK TABLES `alojamientos` WRITE;
/*!40000 ALTER TABLE `alojamientos` DISABLE KEYS */;
INSERT INTO `alojamientos` VALUES (NULL,NULL,NULL,'C. Pablo Mercader Torregrosa, Torrevieja, Alicante',5,10,'Cristiano Ronaldo',100,NULL,NULL,NULL),(NULL,NULL,NULL,'Av. de Julio Iglesias, Andalucía, Marbella, Málaga',2,3,'Isabel Pantoja',115,NULL,NULL,NULL),(NULL,NULL,NULL,'C. Zaragoza, 9, 02260 Fuentealbilla, Albacete',4,8,'Andres Iniesta',80,NULL,NULL,NULL),(NULL,NULL,NULL,'Male, Maldivas',1,2,'Luis Rubiales',500,NULL,NULL,NULL),(NULL,NULL,NULL,'C. Dato, 31, 01005 Vitoria, Álava',1,2,'Oier Martínez',110,NULL,NULL,NULL),(NULL,NULL,NULL,'Luis Kalea, 6, Deusto, 48014 Bilbao',1,2,'Nico Williams',97,NULL,NULL,NULL),(NULL,NULL,NULL,'C. Gran Vía, 1, 28013 Madrid, España',1,3,'Belen Esteban',114,NULL,NULL,NULL);
/*!40000 ALTER TABLE `alojamientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `NOMBRE` varchar(60) NOT NULL,
  `APELLIDO` varchar(160) NOT NULL,
  `DNI` varchar(60) NOT NULL,
  `CORREO` varchar(160) DEFAULT NULL,
  `CLAVE` varchar(60) NOT NULL,
  `ROL` int NOT NULL,
  PRIMARY KEY (`DNI`),
  UNIQUE KEY `CORREO` (`CORREO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES ('Asier','Sanchez','00000000A','asier','asier',1),('Javier','Cerro','00000000C','javier','javier',0),('Ekaitz','Martínez','00000000E','ekaitz','ekaitz',1),('Oier','Martínez','72851803P','oier','oier',1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vuelos_albacete`
--

DROP TABLE IF EXISTS `vuelos_albacete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vuelos_albacete` (
  `ORIGEN` varchar(160) NOT NULL,
  `DESTINO` varchar(160) NOT NULL,
  `HORA_SALIDA` varchar(60) NOT NULL,
  `HORA_LLEGADA` varchar(60) DEFAULT NULL,
  `PRECIO` int NOT NULL,
  `ID` varchar(160) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vuelos_albacete`
--

LOCK TABLES `vuelos_albacete` WRITE;
/*!40000 ALTER TABLE `vuelos_albacete` DISABLE KEYS */;
INSERT INTO `vuelos_albacete` VALUES ('Albacete','Bilbao','10:25 am','11:20 am',15,'VA001');
/*!40000 ALTER TABLE `vuelos_albacete` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vuelos_bilbao`
--

DROP TABLE IF EXISTS `vuelos_bilbao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vuelos_bilbao` (
  `ORIGEN` varchar(160) NOT NULL,
  `DESTINO` varchar(160) NOT NULL,
  `HORA_SALIDA` varchar(60) NOT NULL,
  `HORA_LLEGADA` varchar(60) DEFAULT NULL,
  `PRECIO` int NOT NULL,
  `ID` varchar(160) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vuelos_bilbao`
--

LOCK TABLES `vuelos_bilbao` WRITE;
/*!40000 ALTER TABLE `vuelos_bilbao` DISABLE KEYS */;
INSERT INTO `vuelos_bilbao` VALUES ('Bilbao','Albacete','9:00 am','9:55 am',15,'VB001'),('Bilbao','Malaga','4:00 pm','6:15 pm',50,'VB002'),('Bilbao','Madrid','7:00 pm','7:55 pm',75,'VB003');
/*!40000 ALTER TABLE `vuelos_bilbao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vuelos_madrid`
--

DROP TABLE IF EXISTS `vuelos_madrid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vuelos_madrid` (
  `ORIGEN` varchar(160) NOT NULL,
  `DESTINO` varchar(160) NOT NULL,
  `HORA_SALIDA` varchar(60) NOT NULL,
  `HORA_LLEGADA` varchar(60) DEFAULT NULL,
  `PRECIO` int NOT NULL,
  `ID` varchar(160) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vuelos_madrid`
--

LOCK TABLES `vuelos_madrid` WRITE;
/*!40000 ALTER TABLE `vuelos_madrid` DISABLE KEYS */;
INSERT INTO `vuelos_madrid` VALUES ('Madrid','Malaga','4:40 pm','5:10 pm',55,'VM001'),('Madrid','Bilbao','8:25 pm','9:10 pm',75,'VM002'),('Madrid','Vitoria','8:30 pm','9:15 pm',70,'VM003'),('Madrid','Torrevieja','8:30 pm','9:15 pm',65,'VM004'),('Madrid','Maldivas','8:30 pm','9:10 am',678,'VM005');
/*!40000 ALTER TABLE `vuelos_madrid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vuelos_malaga`
--

DROP TABLE IF EXISTS `vuelos_malaga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vuelos_malaga` (
  `ORIGEN` varchar(160) NOT NULL,
  `DESTINO` varchar(160) NOT NULL,
  `HORA_SALIDA` varchar(60) NOT NULL,
  `HORA_LLEGADA` varchar(60) DEFAULT NULL,
  `PRECIO` int NOT NULL,
  `ID` varchar(160) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vuelos_malaga`
--

LOCK TABLES `vuelos_malaga` WRITE;
/*!40000 ALTER TABLE `vuelos_malaga` DISABLE KEYS */;
INSERT INTO `vuelos_malaga` VALUES ('Malaga','Torrevieja','6:00 pm','6:30 pm',30,'VMA001'),('Malaga','Vitoria','6:30 pm','8:30 pm',50,'VMA002'),('Malaga','Madrid','5:40 pm','6:10 pm',55,'VMA003');
/*!40000 ALTER TABLE `vuelos_malaga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vuelos_maldivas`
--

DROP TABLE IF EXISTS `vuelos_maldivas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vuelos_maldivas` (
  `ORIGEN` varchar(160) NOT NULL,
  `DESTINO` varchar(160) NOT NULL,
  `HORA_SALIDA` varchar(60) NOT NULL,
  `HORA_LLEGADA` varchar(60) DEFAULT NULL,
  `PRECIO` int NOT NULL,
  `ID` varchar(160) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vuelos_maldivas`
--

LOCK TABLES `vuelos_maldivas` WRITE;
/*!40000 ALTER TABLE `vuelos_maldivas` DISABLE KEYS */;
INSERT INTO `vuelos_maldivas` VALUES ('Maldivas','Madrid','9:40 pm','10:30 pm',678,'VMAL001');
/*!40000 ALTER TABLE `vuelos_maldivas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vuelos_torrevieja`
--

DROP TABLE IF EXISTS `vuelos_torrevieja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vuelos_torrevieja` (
  `ORIGEN` varchar(160) NOT NULL,
  `DESTINO` varchar(160) NOT NULL,
  `HORA_SALIDA` varchar(60) NOT NULL,
  `HORA_LLEGADA` varchar(60) DEFAULT NULL,
  `PRECIO` int NOT NULL,
  `ID` varchar(160) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vuelos_torrevieja`
--

LOCK TABLES `vuelos_torrevieja` WRITE;
/*!40000 ALTER TABLE `vuelos_torrevieja` DISABLE KEYS */;
INSERT INTO `vuelos_torrevieja` VALUES ('Torrevieja','Vitoria','1:45 pm','3 pm',35,'VT001'),('Torrevieja','Malaga','5 Pm','5:30 pm',30,'VT002'),('Torrevieja','Madrid','7:15 pm','7:45 pm',20,'VT003');
/*!40000 ALTER TABLE `vuelos_torrevieja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vuelos_vitoria`
--

DROP TABLE IF EXISTS `vuelos_vitoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vuelos_vitoria` (
  `ORIGEN` varchar(160) NOT NULL,
  `DESTINO` varchar(160) NOT NULL,
  `HORA_SALIDA` varchar(60) NOT NULL,
  `HORA_LLEGADA` varchar(60) DEFAULT NULL,
  `PRECIO` int NOT NULL,
  `ID` varchar(160) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vuelos_vitoria`
--

LOCK TABLES `vuelos_vitoria` WRITE;
/*!40000 ALTER TABLE `vuelos_vitoria` DISABLE KEYS */;
INSERT INTO `vuelos_vitoria` VALUES ('Vitoria','Torrevieja','12:00 am','1:15 pm',35,'VVI001'),('Vitoria','Malaga','4:00 pm','6:00 pm',50,'VVI002'),('Vitoria','Madrid','7:15 pm','8:00 pm',70,'VVI003');
/*!40000 ALTER TABLE `vuelos_vitoria` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-04-26  9:18:14
