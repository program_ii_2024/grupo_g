
package components;

import javax.swing.*;
import java.util.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.ActionEvent;
import javax.swing.text.BadLocationException;
import javax.swing.GroupLayout.*;

public class TextAreaDemo extends JFrame
        implements DocumentListener {

    private JLabel jLabel1;
    private JScrollPane jScrollPane1;
    private JTextArea textArea;

    private static final String COMMIT_ACTION = "commit";

    private static enum Mode {
        INSERT, COMPLETION
    };

    private final List<String> words;
    private Mode mode = Mode.INSERT;
    // Constructor
    public TextAreaDemo() {
        super("TextAreaDemo");
        initComponents(); // Inicializa los componentes de la interfaz

        textArea.getDocument().addDocumentListener(this); // Agrega un DocumentListener al JTextArea
        // Configura la acción de presionar Enter para autocompletar palabras
        InputMap im = textArea.getInputMap();
        ActionMap am = textArea.getActionMap();
        im.put(KeyStroke.getKeyStroke("ENTER"), COMMIT_ACTION);
        am.put(COMMIT_ACTION, new CommitAction());

        // Lista de palabras para autocompletar
        words = new ArrayList<String>(8);
        words.add("spark");
        words.add("special");
        words.add("spectacles");
        words.add("spectacular");
        words.add("swing");
        words.add("ekaitz");
        words.add("julen");
        words.add(" sublime");

        Collections.sort(words);

    }

    // Método para inicializar los componentes de la interfaz
    private void initComponents() {
        jLabel1 = new JLabel("Try typing 'spectacular' or 'Swing'...");

        textArea = new JTextArea();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        textArea.setColumns(20);
        textArea.setLineWrap(true);
        textArea.setRows(10);
        textArea.setWrapStyleWord(true);

        jScrollPane1 = new JScrollPane(textArea);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);

        // Configuración del diseño del GroupLayout
        
        // Crear un grupo paralelo para el eje horizontal.
        ParallelGroup hGroup = layout.createParallelGroup(GroupLayout.Alignment.LEADING);
        // Crear grupos secuenciales y paralelos.
        SequentialGroup h1 = layout.createSequentialGroup();
        ParallelGroup h2 = layout.createParallelGroup(GroupLayout.Alignment.TRAILING);
        // Agregue un panel de desplazamiento y una etiqueta al grupo paralelo h2
        h2.addComponent(jScrollPane1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE);
        h2.addComponent(jLabel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE);

        // Agregue un espacio de contenedor al grupo secuencial h1
        h1.addContainerGap();
        // Suma el grupo h2 al grupo h1
        h1.addGroup(h2);
        h1.addContainerGap();
        // Agrega el grupo h1 a hGroup
        hGroup.addGroup(Alignment.TRAILING, h1);
        // Crear el grupo horizontal
        layout.setHorizontalGroup(hGroup);

        // Crear un grupo paralelo para el eje vertical.
        ParallelGroup vGroup = layout.createParallelGroup(GroupLayout.Alignment.LEADING);
        // Crear un grupo secuencial
        SequentialGroup v1 = layout.createSequentialGroup();
        // Agregue un espacio de contenedor al grupo secuencial v1
        v1.addContainerGap();
        // Agregar una etiqueta al grupo secuencial v1
        v1.addComponent(jLabel1);
        v1.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED);
        // Agregar panel de desplazamiento al grupo secuencial v1
        v1.addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE);
        v1.addContainerGap();
        // Agregue el grupo v1 a vGroup
        vGroup.addGroup(v1);
        // Crear el grupo vertical
        layout.setVerticalGroup(vGroup);
        pack();

    }
    // Métodos de escucha

    public void changedUpdate(DocumentEvent ev) {
    }

    public void removeUpdate(DocumentEvent ev) {
    }

    // Método que se ejecuta cuando se inserta texto en el JTextArea
    public void insertUpdate(DocumentEvent ev) {
        if (ev.getLength() != 1) {
            return;
        }

        int pos = ev.getOffset();
        String content = null;
        try {
            content = textArea.getText(0, pos + 1);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }

        // Encuentra la posición donde comienza la palabra
        int w;
        for (w = pos; w >= 0; w--) {
            if (!Character.isLetter(content.charAt(w))) {
                break;
            }
        }
        if (pos - w < 1) {
            // Demasiado pocos caracteres
            return;
        }

        String prefix = content.substring(w + 1).toLowerCase();
        int n = Collections.binarySearch(words, prefix);
        if (n < 0 && -n <= words.size()) {
            String match = words.get(-n - 1);
            if (match.startsWith(prefix)) {
                // Se encontró una palabra para autocompletar
                String completion = match.substring(pos - w);
                // No se puede modificar el Document desde dentro de la notificación,
                // así que se envía una tarea que realice el cambio más tarde
                SwingUtilities.invokeLater(
                        new CompletionTask(completion, pos + 1));
            }
        } else {
            // No se encontró nada
            mode = Mode.INSERT;
        }
    }

    private class CompletionTask implements Runnable {
        String completion;
        int position;

        CompletionTask(String completion, int position) {
            this.completion = completion;
            this.position = position;
        }

        public void run() {
            textArea.insert(completion, position);
            textArea.setCaretPosition(position + completion.length());
            textArea.moveCaretPosition(position);
            mode = Mode.COMPLETION;
        }
    }

    private class CommitAction extends AbstractAction {
        public void actionPerformed(ActionEvent ev) {
            if (mode == Mode.COMPLETION) {
                int pos = textArea.getSelectionEnd();
                textArea.insert(" ", pos);
                textArea.setCaretPosition(pos + 1);
                mode = Mode.INSERT;
            } else {
                textArea.replaceSelection("\n");
            }
        }
    }

    public static void main(String args[]) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                // Turn off metal's use of bold fonts
                UIManager.put("swing.boldMetal", Boolean.FALSE);
                new TextAreaDemo().setVisible(true);
            }
        });
    }
}