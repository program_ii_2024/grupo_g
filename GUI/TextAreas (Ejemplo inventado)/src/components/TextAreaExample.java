package components;

import javax.swing.*;
import java.awt.event.*;
import java.awt.Font;

public class TextAreaExample extends JFrame {
    private JTextArea textArea; // Área de texto donde el usuario puede ingresar texto
    private String textoGuardado = ""; // Variable para almacenar el texto guardado

    public TextAreaExample() 
    {
        setTitle("TextArea"); // Título de la ventana
        setSize(800, 600); // Tamaño de la ventana
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cerrar la aplicación cuando se cierre la ventana

        textArea = new JTextArea(); // Crear un nuevo JTextArea
        JScrollPane scrollPane = new JScrollPane(textArea); // Agregar el JTextArea a un JScrollPane para permitir el desplazamiento

        // Botón para poner la letra en cursiva
        JButton cursivaButton = new JButton("Cursiva");
        cursivaButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
                Font fuenteActual = textArea.getFont(); // Obtener la fuente actual del JTextArea
                Font nuevaFuente = new Font(fuenteActual.getName(), Font.ITALIC, fuenteActual.getSize()); // Crear una nueva fuente cursiva
                textArea.setFont(nuevaFuente); // Establecer la nueva fuente en el JTextArea
            }
        });

        // Botón para poner la letra en negrita
        JButton negritaButton = new JButton("Negrita");
        negritaButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
                Font fuenteActual = textArea.getFont(); // Obtener la fuente actual del JTextArea
                Font nuevaFuente = new Font(fuenteActual.getName(), Font.BOLD, fuenteActual.getSize()); // Crear una nueva fuente en negrita
                textArea.setFont(nuevaFuente); // Establecer la nueva fuente en el JTextArea
            }
        });

        // Botón para quitar la letra en negrita
        JButton quitarNegritaButton = new JButton("Quitar Negrita");
        quitarNegritaButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
                Font fuenteActual = textArea.getFont(); // Obtener la fuente actual del JTextArea
                Font nuevaFuente = new Font(fuenteActual.getName(), Font.PLAIN, fuenteActual.getSize()); // Crear una nueva fuente sin negrita
                textArea.setFont(nuevaFuente); // Establecer la nueva fuente en el JTextArea
            }
        });

        // Botón para quitar la letra en cursiva
        JButton quitarCursivaButton = new JButton("Quitar Cursiva");
        quitarCursivaButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) 
            {
                Font fuenteActual = textArea.getFont(); // Obtener la fuente actual del JTextArea
                Font nuevaFuente = new Font(fuenteActual.getName(), Font.PLAIN, fuenteActual.getSize()); // Crear una nueva fuente sin cursiva
                textArea.setFont(nuevaFuente); // Establecer la nueva fuente en el JTextArea
            }
        });

        // Botón para borrar el texto
        JButton borrarButton = new JButton("Borrar");
        borrarButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
                textArea.setText(""); // Borrar el texto del JTextArea
            }
        });

        // Botón para guardar el texto
        JButton guardarButton = new JButton("Guardar");
        guardarButton.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent e) 
            {
                textoGuardado = textArea.getText(); // Obtener el texto del JTextArea y guardarlo en la variable textoGuardado
                JOptionPane.showMessageDialog(TextAreaExample.this, "Texto guardado:\n"); // Mostrar un mensaje con el texto guardado
                System.out.println(textoGuardado);
            }
        });

        // Agregar los componentes al panel de contenido
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(cursivaButton);
        buttonPanel.add(negritaButton);
        buttonPanel.add(quitarNegritaButton);
        buttonPanel.add(quitarCursivaButton);
        buttonPanel.add(borrarButton);
        buttonPanel.add(guardarButton);

        getContentPane().add(scrollPane, "Center"); // Agregar el JScrollPane al centro de la ventana
        getContentPane().add(buttonPanel, "North"); // Agregar el panel de botones al sur de la ventana

        setVisible(true); // Hacer visible la ventana
    }

    public static void main(String[] args) 
    {
        new TextAreaExample(); // Crear una instancia de la clase TextAreaExample
    }
}